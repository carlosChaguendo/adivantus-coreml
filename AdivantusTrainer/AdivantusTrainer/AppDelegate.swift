//
//  AppDelegate.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 4/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

