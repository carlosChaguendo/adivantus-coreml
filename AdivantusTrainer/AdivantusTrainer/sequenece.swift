//
//  SequenceType.swift
//  AdivantusCore
//
//  Created by carlos chaguendo on 1/06/16.
//  Copyright © 2016 Mayorgafirm. All rights reserved.
//



public extension Sequence {
    
    public func groupBy<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U: [Iterator.Element]] {
        var dict: [U: [Iterator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) {
                dict[key] = [el]
                
            }
        }
        return dict
    }
    
    
    public func countBy<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U: Int] {
        var dict: [U: Int] = [:]
        for el in self {
            let key = keyFunc(el)
            if dict[key] == nil {
                dict[key] = 1
            } else {
                dict[key] = dict[key]! + 1
            }
            
            //if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
    
    
    
}


public extension Sequence {
    
    /// Este método es como `.uniq` excepto que acepta una funcion que se invoca para cada elemento en el array para generar el criterio por el cual se calcula la unicidad.
    /// El orden de los valores de los resultados se determina por el orden en que ocurren en el array. La iteración se invoca con un argumento:
    /// - Parameters:
    ///   - getIdentifier: La funcion invocada por elemento
    /// - Returns: Devuelve el nuevo array libre de duplicados.
    public func uniq<Id: Hashable >(by getIdentifier: (Iterator.Element) -> Id) -> [Iterator.Element] {
        var ids = Set<Id>()
        return self.reduce([]) { uniqueElements, element in
            if ids.insert(getIdentifier(element)).inserted {
                return uniqueElements + [element]
            }
            return uniqueElements
        }
    }
}


public extension Sequence where Iterator.Element: Hashable {
    
    /// Crea una versión libre de duplicados de una Array, usando comparaciones de igualdad, en la que sólo se mantiene la primera ocurrencia de cada elemento.
    /// El orden de los valores de los resultados se determina por el orden en que ocurren en el array.
    var uniq: [Iterator.Element] {
        return self.uniq(by: { (element) -> Iterator.Element in
            return element
        })
    }
    
}


public extension  RangeReplaceableCollection where Element : Equatable {
    
    /// Elimina el elemento del listado que seha igual a:
    ///
    /// - Returns: Elemento eliminado
    mutating func  remove(_ element: Element) {
        if let index = self.index(of: element) {
            remove(at: index)
        }
    }
    
}

public extension Array {
    
    public var nonEmpty:Bool{
        return !isEmpty
    }
    
    public subscript (safe index: Int) -> Element? {
        return index >= 0 && index < count ? self[index] : nil
    }
    
    /// Removes all elements from an array that the callback returns true.
    ///
    /// - Returns: Array with removed elements.
    @discardableResult
    public mutating func remove( callback: (Iterator.Element) -> Bool) -> [Iterator.Element] {
        
        var index = 0
        var removed: [Iterator.Element] = []
        
        for el in self {
            
            if callback(el) == true {
                let rm = self[index]
                removed.append(rm)
                self.remove(at: index)
                index -= 1
            }
            index += 1
        }
        
        
        
        
        return removed
    }
    
    
    /// Replace all elements from an array that the callback returns true.
    ///
    /// :return Array with replaced elements.
    public mutating func replace(by newElement: Iterator.Element, when: (Iterator.Element) -> Bool) -> [Iterator.Element]? {
        
        var index = 0
        var removed: [Iterator.Element] = []
        
        for el in self {
            if when(el) == true {
                removed.append(self[index])
                self.remove(at: index)
                self.insert(newElement, at: index)
            }
            index += 1
        }
        
        return removed.count == 0 ? nil : removed
    }
    
    public mutating func replace(by newElement: Iterator.Element, at index: Index) {
        remove(at: index)
        insert(newElement, at: index)
    }
    
    
}
