//// Example puzzle from http://en.wikipedia.org/wiki/Sudoku
//let example: Sudoku = [
//    [5, 3, 0,  0, 7, 0,  0, 0, 0],
//    [6, 0, 0,  1, 9, 5,  0, 0, 0],
//    [0, 9, 8,  0, 0, 0,  0, 6, 0],
//
//    [8, 0, 0,  0, 6, 0,  0, 0, 3],
//    [4, 0, 0,  8, 0, 3,  0, 0, 1],
//    [7, 0, 0,  0, 2, 0,  0, 0, 6],
//
//    [0, 6, 0,  0, 0, 0,  2, 8, 0],
//    [0, 0, 0,  4, 1, 9,  0, 0, 5],
//    [0, 0, 0,  0, 8, 0,  0, 7, 0],
//]
//
//println("\nPuzzle:")
//printSudoku(example)
//if let solutionForExample = solveSudoku(example) {
//    println("\nSolution:")
//    printSudoku(solutionForExample)
//}
//else {
//    println("No solution")
//}
//
//
//// Find all solutions to this puzzle (there are 20)
//
//let diagonals: Sudoku = [
//    [9, 0, 0,  0, 0, 0,  6, 0, 0],
//    [0, 8, 0,  0, 0, 0,  0, 5, 0],
//    [0, 0, 7,  0, 0, 0,  0, 0, 4],
//
//    [3, 0, 0,  6, 0, 0,  9, 0, 0],
//    [0, 2, 0,  0, 5, 0,  0, 8, 0],
//    [0, 0, 1,  0, 0, 4,  0, 0, 7],
//
//    [6, 0, 0,  9, 0, 0,  3, 0, 0],
//    [0, 5, 0,  0, 8, 0,  0, 2, 0],
//    [0, 0, 4,  0, 0, 7,  0, 0, 1],
//]
//
//println("\nPuzzle:")
//printSudoku(diagonals)
//var solutionCount: Int = 0
//findAllSolutions(diagonals) { solution in
//    ++solutionCount
//
//    println("\nSolution \(solutionCount):")
//    printSudoku(solution)
//
//    // Return true to continue
//    return true
//}
//if solutionCount == 0 {
//    println("No solutions")
//}
