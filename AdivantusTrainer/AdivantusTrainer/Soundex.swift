//
//  Soundex.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 9/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa


public class Soundex {
    
    private static let en_mapping_string = Array("01230120022455012623010202")
    private static let en_alphabet = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    private let mapping: [Character:Character] = Soundex.buildMapping(en_alphabet,alphabet:en_mapping_string)
    
    private static func buildMapping(_ codes: Array<Character>, alphabet: Array<Character>) -> [Character:Character] {
        var retval: [Character:Character] = [:]
        for (index,code) in codes.enumerated() {
            retval[code] = alphabet[index]
        }
        return retval
    }
    
    private var soundexMapping: Array<Character> =   Array(repeating: " ", count: 4)// Array(repeating:" ",count:4)
    
    private func getMappingCode(_ s: String, index:Int) -> Character {
        
        
        
        let i = s.index(s.startIndex, offsetBy: index)
        
        let mappedChar = mapChar(s[i])
        
        if (index>1 && !(mappedChar=="0"))
        {
            let j = s.index(s.startIndex,offsetBy:index-1)
            //let j = s.startIndex.advancedBy(index-1)
            
            let hwChar = s[j]
            
            if (hwChar=="H" || hwChar=="W")
            {
                let k = s.index(s.startIndex,offsetBy:index-2)
                //let k = s.startIndex.advancedBy(index-2)
                let prehwChar = s[k]
                let firstCode = mapChar(prehwChar)
                if (firstCode==mappedChar || "H"==prehwChar || "W"==prehwChar) {
                    return "0"
                }
            }
        }
        
        return mappedChar
    }
    
    private func mapChar(_ c: Character) -> Character {
        if let val = mapping[c] {
            return val
        }
        return "0" // not specified in original Soundex specification, if character is not found, code is 0
    }
    
    public func soundex(of: String) -> String {
        
        guard (of.count>0) else {
            return ""
        }
        
        let str=of.uppercased()
        
        var out: Array<Character> = Array("    ")
        var last: Character = " "
        var mapped: Character = " "
        var incount=1
        var count = 1
        
        out[0]=str[str.startIndex]
        last = getMappingCode(str, index: 0)
        while (incount < str.count && count < out.count) {
            mapped = getMappingCode(str, index: incount)
            incount += 1
            if (mapped != "0") {
                if (mapped != "0" && mapped != last) {
                    out[count]=mapped
                    count += 1
                }
            }
        }
        return String(out)
    }
}
