//
//  Extensions.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 5/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa


extension Optional {
    
    public func `or`(else value : Wrapped?) -> Optional {
        return self ?? value
    }
    
    public func `or`(else value: Wrapped) -> Wrapped {
        return self ?? value
    }
    
}

extension String {
    /// Referencia  unica a una cadena vacia
    public static let empty = ""
}

extension Optional where Wrapped == String {
    
    public var orEmpty: String {
        return self.or(else: String.empty)
    }
    
}

/// Referencia  unica a un array vacio
fileprivate let ArrayEmpty: Array<Any> = []

extension Optional where Wrapped: Sequence {
    
    public var orEmpty:Wrapped {
        if self == nil {
            // Evita estar creando arrays vacios en memoria
            return ArrayEmpty as! Wrapped
        }
        return self!
    }
    
}


extension Optional where Wrapped == Bool {
    
    public var orNot: Bool {
        return self.or(else: false)
    }
    
}

