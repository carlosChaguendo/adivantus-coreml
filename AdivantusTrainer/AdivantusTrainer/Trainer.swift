#!/usr/bin/swift
//
//  Trainer.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 4/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa
import Foundation
import CreateML
import NaturalLanguage

public  func messageWhenChangeMedication() -> [[String:String]]{
//    let bundle = Bundle.main
//    guard let url = bundle.url(forResource: "medicamentos", withExtension: "json") else {
//        fatalError("medicationsChangesMessage.json not found")
//    }
    let fileURL = URL(fileURLWithPath: "/Users/leonardo/workspaces/swift/AdivantusTrainer/AdivantusTrainer/medicamentos.json")
    
    print("URL \(fileURL)")
    
    guard let inData = try? Data(contentsOf: fileURL) else {
        fatalError(" \(fileURL) file not found ")
    }
    
    do {
        return try JSONSerialization.jsonObject(with: inData, options: []) as! [[String: String]]
    } catch {
        print(error.localizedDescription)
    }
    
    return []
    
}


do {
    
   
    
    
    let dictionary = messageWhenChangeMedication()
    print("dictionary \(dictionary.count)")
    
    let (texts, labels): ([String], [String]) = dictionary.reduce(into: ([], [])) {
        $0.0.append($1["token"]!)
        $0.1.append($1["label"]!)
    }
    
    print("texts \(texts.count)")
    print("labels \(labels.count)")
    
    
  
    
    let metadata = MLModelMetadata(
        author: "Carlos Chaguendo",
        shortDescription: "Un modelo entrenado para detectar medicamnetos,dosage, nombre generico",
        version: "1.0",
        additional:["Larry": "Capija"])
    
//    if false {
//        let dataTable = try MLDataTable(dictionary: ["token": texts, "label": labels])
//        let (trainingData, testingData) = dataTable.randomSplit(by: 0.8, seed: 0)
//
//        let classifierText = try MLTextClassifier(trainingData: trainingData, textColumn: "token", labelColumn: "label")
//        let evaluation = classifierText.evaluation(on: testingData)
//        print("Evaluation")
//        print(evaluation)
//        
//
//        let destinationPath1 = "/Users/leonardo/workspaces/swift/AdivantusTrainer/AdivantusTrainer/MedicationTextClassifier.mlmodel"
//        try classifierText.write(to: URL(fileURLWithPath: destinationPath1), metadata: metadata)
//
//    }else{
    
    
        let data = try MLDataTable(contentsOf: URL(fileURLWithPath: "/Users/leonardo/workspaces/swift/AdivantusTrainer/AdivantusTrainer/medicamentos.json"))
        let (trainingData,testingData) = data.randomSplit(by: 0.8, seed: 8)
    
//        let sentimentClassifier = try MLTextClassifier(trainingData: trainingData, textColumn: "token", labelColumn: "label")
//        let evaluation = sentimentClassifier.evaluation(on: testingData)
//        print("sentimentClassifier \(evaluation)")
    

    
        let classifierWord = try MLWordTagger(trainingData: trainingData, tokenColumn: "token", labelColumn: "label")

        
        let destinationPath2 = "/Users/leonardo/workspaces/swift/AdivantusTrainer/AdivantusTrainer/MedicationWordClassifier.mlmodel"
        try classifierWord.write(to: URL(fileURLWithPath: destinationPath2), metadata: metadata)
//    }

    
}catch {
    print(error)
}
