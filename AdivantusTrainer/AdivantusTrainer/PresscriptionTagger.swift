//
//  PresscriptionTagger.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 9/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa
import CoreML
import NaturalLanguage

class PresscriptionTagger {

    private let medicationWordTagger = MedicationWordTagger()
     private let wordTaggerModel: NLModel = try! NLModel(mlModel:  MedicationWordTagger().model)
    
    
    func filter(string: String) -> String {
        var str = string.uppercased()
        if let range = str.range(of: "RX#") {
            str = str.replacingCharacters(in: ..<range.lowerBound, with: "")
        }
        return str
    }
    
    /**
     
     */
    func decode(string: String) -> Void {
        print("\n\(string)\n")
        do {
        var labels: [String: String] = [:]
        try string.split(separator: " ").forEach { (str) in
            

            let predictions = try medicationWordTagger.prediction(text: String(str))
          //  print("\(str)  ===== ")
            
            for i in 0..<predictions.labels.count {
                let tag = predictions.labels[i]
                let txt = predictions.tokens[i]
                
//                print("\t\t \(tag) = \t \(txt)")
                
                if labels[tag] == nil {
                    labels[tag] = txt
                } else {
                    labels[tag] = labels[tag]! + " " + txt
                }
                
            }
        }
        
        print("\n lini a por line map")
        labels.forEach { (key: String, value: String) in
            print(" \(key)\t\t  \(value)")
        }
        }catch {
            print("Error \(error)")
        }
    }
    
    func tokens(text: String) {
        
        let scheme = NLTagScheme.init("cusrom")
        
        if scheme == nil {
            preconditionFailure()
        }
        
        
        let model = try! NLModel(mlModel: medicationWordTagger.model)
        
        
        
        let tagger = NLTagger(tagSchemes: [scheme])
        tagger.setModels([model], forTagScheme: scheme)
        
        let range = text.startIndex..<text.endIndex
        
        tagger.setLanguage(.english, range: range)
        let tags = tagger.tags(in: range, unit: .word, scheme: scheme, options: [])
        
        for (tag, range) in tags {
//            switch tag {
//            case .personalName?:
//                detectedName = String(text[range])
//            case .placeName?:
//                detectedPlace = String(text[range])
//            default:
//                break
//            }
            let part =  text[range]
            print("Tag \(tag)  \(part)")
        }
      
    }
    
    
}


