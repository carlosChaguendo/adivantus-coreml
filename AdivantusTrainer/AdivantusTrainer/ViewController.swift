//
//  ViewController.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 4/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa
import Foundation
import CreateML
import Vision
import NaturalLanguage








class ViewController: NSViewController {
    

    let rosePharmacy = """
    ROSE PHARMACY
    (714) 662-0548
    R
    1220 Hemlock Way, Suite 110
    AL LAW PRUHIBITS TRANSFER OF THI DRUG IO ANY PERSON OTHER THAN PAIIENT FOR WHOM PRE CIHtE
    Santa Ana, CA 92707
    *
    RX# 962971
    WACKER, JANYCE
    ATORVASTATIN 40MG
    Dr.REXINGER, ELWYN
    U R
    12/01/2016 CG
    Generic for: LIPITOR
    TAKE 1 TABLET BY MOUTH DAILY
    FOR CHOLESTEROLY
    REFILLS:7 LF: 11/01/20
    Oval:APO;ATV40;White:White
    Cty# 30 Exp Date: 12/01/2017
    """
    
    
    let encino = """
    (818) 789-9200
    16001 VENTURA BLVD. #135
    ENCINO, CA 91436
    COENCINO CARE
    PHA RMAC
    7-09-16
    ORIG
    FILL: 12-28-16
    RX#805387
    CARTER,ALICE
    VITAMIN D3 10001U
    IND
    DR.CHOW,PRISCILLA
    TAKE ONE CAPSULE BY
    MOUTH EVERY DAY
    CARTER,ALICE
    12-28-16
    RX# 805387
    ,*
    VITAMIN D3 1000IU
    L
    .
    REF CINO CA 01298-0445-02
    #30
    ESCR
    EXPIRBS

    """
    
    
    let asp = """
    ASAP PHARM
    818-543-1800
    www.asappharmacy.com
    1340 E. Wilson Ave.
    7842597
    MATHER,SANDRA
    RANITIDINE 150 MG TABLET
    GENERIC FOR: ZANTAC 150MG TABLE 30TAB
    Glendale, CA 91206
    FILL DATE: 08/07/18
    STRID
    TAKE 1 TABLET BY MOUTH
    EVERY DAY FOR GI
    PREVENTION
    This medicine is a brown, round
    shaped, tablet imprinted with S4
    on one side.
    EXP
    64380-0803-08
    rson other than the patient for whom pre
    10UBEGUI
    sOr
    """
    
    
    let hospice = """
    GILBERT
    DRUGS
    714)6388230
    9240 GARDEN GROVE BLD SUTTE #20. GARDEN GROVE. CA 92844
    RX#6279019
    CL T DANG
    NNEDY, GLORIA
    08/10/18
    DOCUSATE SODIUM 250MG
    TAKE ONE CAPSULE BY MOUTH 2
    IMES A DAY FOR CONSTIPATION
    HOLD FOR LOOSE STOOL
    CASH PRESCAIPnO
    
    UBSTITUTED FOR COLACE 2SONG CAP
    QTY#:60  EXP-08/10/19 02 REFILLS
    """
    
    
    let encino2 = """
    0010 PG
    WED 6:55 AM Q
    MMIM INHNHIMIMAANHH MIMIMAHIMAM
    EN
    D
    SCH
    GILBER7
    C DRUGS
    RIRIN
    (714) 638-8230
    9240 GARDEN GROVE BLD., SUITE #20, GARDEN GROVE, CA 92844
    81
    32
    M. SAMPANG
    CL
    RX#6277134
    WALTERS, LINDA (ES)
    MIRTAZAPINE 15 MG TABLET
    07/27/18
    TEVAU
    RU
    AT
    35
    0-
    3-
    87
    TAKE ONE-HALF TABLET BY MOUTH
    EVERY DAY AT BEDTIME FOR SLEEP
    WG
    ETUH
    4
    SCAN/ES
    SUBSTITUTED FOR REMERON 15MG TAB
    QTY: 15 TAB
    00093-7206-56
    EMBER
    02 REFILLS
    NA
    EXP 07/27/19
    NG
    DE

    """
    
    
    let rons = """
    DR.NEYSSAN.TEBYANI
    DISPENSED DATE
    C6/19/2018
    OPTUNRX
    NO REFILLS, SUBMIT NEW PRE SCRIPTIO.
    RX: 200187042
    DISCARD AFTER:
    1.877.389.6358
    SEE PACKAAGE
    CANNOT FILL AFTER:
    03/27/2019
    2858 LOKERAVE EAST STE 100/ CADSBAD, CA 9 010
    ITEM: (3 OF 3)
    JAYNE,BAILEY
    PREMARIN VAG CRE 0.625MG
    MFG: WYETH
    APPLY 1 INCH OF CREAM TO
    FINGER AND APPLY TO OUTER
    URETHRA AND VAGINAL AREA
    TWICE WEEKLY
    QTY:30
    RPH: Q NGUYENN
    REFILLS: 0
    CALL YOUR DOCTOR TOR MEDICAL ADVICE ABOUT SIDE EFFECTS. YOU MAY REPORT ELFECTS TO FOA AT 1-800-FDA-1088
    ILH
    CAUTION:FEDERAL LAW PROHIBITS THE LRANSTER OF THIS DUG TO ANY, PERSON OTHER THAN THE PATIENT FOR WHOM A WAS PRESCRIBED. DO NOTUSE IUT
    DISPENSED 8Y: NPI #1497704431 /0DEA # BP9744524/CATIC # PHY47482

    """
    
    /*
     rx_number              RX # 965486
     resident_name              NICHOLS , JUDE
     refill              REFILLS : 9
     qty              CTY # 90
     physician_name              DR.HARRISON , VERONICA
     phone              ( 714 ) 662-0548
     pharmacy_name              ROSE RPHARMACY
     medication_name              GC QUETIAPINE FUMARATE.25MG TAE
     last_fill_date              LF : 12 / 01 / 20
     ins              R U
     generic              GENERIC FOR : SEROQUEL
     expedition_date              EXP DATE : 01 / 02 / 201
     dosage              TAKE 1 TABLET BY MOUTHTHREE TIMES DAILY FOR AGITATION " ATYFE
     date              01|/|02|/|2017
     caution              CAUTION : FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBED
     address              1220 HEMLOCK WAY , SUITE 110 SANTA ANA , CA 92707
     _trash_              ROUND : APO QUE / 25 ; PEACH PEACH
*/
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// entrenar otro tagger para sacar el
        let txt = rons.uppercased()
        //clasifiBylines(text: txt)
        //clasifiBy(text: txt)
        
        let result = try! MedicationStrength().prediction(text: "LEVOTHYROXYNE SODIUM 0.112MG TAB")
        
        
        
        if false {
            let a = "TAKE 1 EVERY 4 HOURS AS NEEDED"
            let b = "TAKE 1 TABLET BY MOUNTH AT BDTIME CHOLESTEROY"
            let c = "TAKE 1 TABLET BY MOUNTH AT BEDTIME CHOLESTEROY"
            let d = "1 TAB EVERY 6 HOURS"
            clasifyCron(text: d)
            return
        }
        
        
        let schemeTagger = NLTagScheme(rawValue: "schemeTagger")

        
         let tagger = NLTagger(tagSchemes: [schemeTagger])
        
        let taggador = MedicationWordTagger().model
        

        let nlModelTagger = try! NLModel(mlModel: taggador)
        
        
        tagger.setModels([nlModelTagger], forTagScheme: schemeTagger)
        
        
        tagger.string = txt
        let range = txt.startIndex..<txt.endIndex
        
        /// Bueno para sacr el nombre del medicamento, malo para el dosage,qty
        /// + medication name
       /// createTags(tagger: tagger, scheme: schemeClasification, unit: .sentence, range: range, options: options)
        
        ///
        /// + Dosaje
        /// + Rx
        /// + Qty
        /// + resident name
        //  + ph name
 

   
        
         createTags(tagger: tagger, scheme: schemeTagger, unit: .word, range: range, options: [])
        
        /// +- Medication Name
        /// +  Generic
        /// +- resident_name  si entre las comas no hay espacio
        /// +- physician_name  Si no hay espaciosn entre los nombres
       
        
   
        
        
       
        
    }
    
    


    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    
    func wordCounts(text: String) -> [String: Double] {
        // 1
        var bagOfWords: [String: Double] = [:]
        // 2
        let tagger = NSLinguisticTagger(tagSchemes: [.tokenType], options: 0)
        // 3
        let range = NSRange(text.startIndex..., in: text)
        // 4
        let options: NSLinguisticTagger.Options = [.omitPunctuation, .omitWhitespace]
        
        
        // 5
        tagger.string = text
        // 6
        tagger.enumerateTags(in: range, unit: .word, scheme: .tokenType, options: options) { _, tokenRange, _ in
            let word = (text as NSString).substring(with: tokenRange)
            bagOfWords[word, default: 0] += 1
        }
        
        return bagOfWords
    }
    
    
    func clasifyCron(text: String)  {
        let cronClas = CronsTextClassifier()
        
        let prn = CronsPRNTextClassifier()
        
        let result = try! cronClas.prediction(text: text)
        let result2 = try! prn.prediction(text: text)
        
        print("result \(result.label)  \t\(result2.label)")
        
        let counts = wordCounts(text: text)

 
        
 
        //prediction.label
        
    }
    
    
    func clasifiBylines(text: String) {
        let mTagger = MedicationWordTagger()
        let textClasifier = MedicationTextClassifier()
        
        let tagerMl =  try! NLModel.init(mlModel: mTagger.model)
        

    
        
        let lines: [String] = text.split(separator: "\n").map(String.init)
        var groups:[String: [String]] = [:]
        var category:[String: [String]] = [:]
        var joined:[String: [String]] = [:]

        
        for line in lines where line.count > 1 {
            
         

            let predictions = try! mTagger.prediction(text: line)
            let predictionsClass = try! textClasifier.prediction(text: line)
            
           // let token = wordCounts(text: line)
            // 16001 VENTURA BLVD. #135
           let labelMax = predictions.labels.countBy{ $0 }.map{ ($0.key, $0.value) }.sorted { $0.1 < $1.1 }.last?.0
           let tag = tagerMl.predictedLabel(for: line)

//            print( " \(predictionsClass.label == labelMax.orEmpty) Line # \(line) \t\t tagger:\(labelMax.orEmpty) == classifier:\(predictionsClass.label)")
            
            if predictionsClass.label == tag {
                joined[predictionsClass.label, default:[]].append(line)
            }
     
            if case nil = category[predictionsClass.label]?.append(line) {
                category[predictionsClass.label] = [line]
            }
            
            for (i,token) in predictions.tokens.enumerated() {
                let key = predictions.labels[i]
                //print("\t \(token) -> \(key)")
                if case nil = groups[key]?.append(token) {
                    groups[key] = [token]
                }
            }
        }
        
//        print("\n\n\nPor Grupo")
//        groups.forEach { (key: String, value: [String]) in
//            print(" \(key)\t\t  \(value.joined(separator: " "))")
//        }
//
//        print("\n\n\nPor categoria")
//        category.forEach { (key: String, value: [String]) in
//            print(" \(key)\t\t  \(value.joined(separator: " "))")
//        }
        
        print("\n\n\nPor Conjuncion")
        joined.forEach { (key: String, value: [String]) in
            print(" \(key)\t\t  \(value.joined(separator: " "))")
        }

        
    }
    
    
    func clasifiBy(text input: String) {
        let text = input.replacingOccurrences(of: "\n", with: " ")
         print("\n")
        print(text)
        print("\n")
        
        let mTagger = MedicationWordTagger()
        
        var groups:[String: [String]] = [:]
            //print("line: \(line)")
            let predictions = try! mTagger.prediction(text: text)
        
            for (i,token) in predictions.tokens.enumerated() {
                let key = predictions.labels[i]
                groups[key, default: []].append(token)
////                print("\t \(token) -> \(key)")
//                if case nil = groups[key]?.append(token) {
//                    groups[key] = [token]
//                }
            }
    
        groups.map{ ($0.key, $0.value )}.sorted(by: { $0.0 > $1.0 }).forEach { (key: String, value: [String]) in
            
            let separats = key == "date" ? "|": " "
            
            print(" \(key)\t\t\t  \(value.joined(separator: separats))")
        }
        
    }
    
    
    
    
    


    
    func createTags(tagger: NLTagger, scheme:NLTagScheme, unit: NLTokenUnit, range: Range<String.Index>, options:NLTagger.Options = [.omitWhitespace] ) {
        var labels: [String: [String]] = [:]
        
        let testString = tagger.string!
        //let model = tagger.models(forTagScheme: scheme).first
        
        print("\n\nscheme ++++++++++++++++ \(scheme.rawValue) \(unit.rawValue)")
        
        tagger.enumerateTags(in: range, unit: unit, scheme: scheme, options: options) { (tag, range) -> Bool in
            
            if tag == .whitespace {
                
            } else {
                let txt = String(testString[range]).replacingOccurrences(of: "\n", with: " ")
                let tag = tag?.rawValue ?? "n.a"
                
                labels[tag, default: []].append(txt)
                
                //let prediction = model?.predictedLabel(for: txt).orEmpty
            
                
                //let other = (prediction == tag ) ? "" : prediction
                //print("[NLTagger] TAG: \(tag) - \(other.orEmpty) \t\(txt)  )")
            }
            return true
        }
        labels.map{ ($0.key, $0.value )}.sorted(by: { $0.0 > $1.0 }).forEach { (key: String, value: [String]) in
            print(" \(key)\t\t\t  \(value.joined(separator: " "))")
        }
        
    }
    
    /**
     Detector de lenguaje
     */
    func lenguageP() {
        let code = """
        struct Plane: Codable {
            var manufacturer: String
            var model: String
            var seats: Int
        }


        var access = function( elems, fn ) {
            return 1
        };
        """
        print("Detectando lenguage de \(code)")
        let url = Bundle.main.url(forResource: "ProgrammingLanguageClassifier",
                                  withExtension: "mlmodelc")!
        let model = try! NLModel(contentsOf: url)
        print(model.predictedLabel(for: code)) // Swift
        
        
        let scheme = NLTagScheme("scheme")
        let tagger = NLTagger(tagSchemes: [scheme, .tokenType, .language, .lexicalClass, .nameType, .lemma])
        let options:NLTagger.Options = [.omitWhitespace]//[.omitPunctuation, .omitWhitespace, .joinNames]
        do {
            
            tagger.setModels([model], forTagScheme: scheme)
            tagger.string = code
            let range =  code.startIndex..<code.endIndex
            tagger.enumerateTags(in: range, unit: .word, scheme: scheme, options: options) { (tag, range) -> Bool in
                
                    print("TAG: \(tag?.rawValue ?? "n.a") \t    \(String(code[range]))  ")
                    //results.append((tag?.rawValue ?? "", ))
                
                return true
            }
            print("lenguaje \( tagger.dominantLanguage?.rawValue)")
            
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    

}

