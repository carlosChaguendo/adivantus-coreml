//
//  ProgrammingLanguage.swift
//  AdivantusTrainer
//
//  Created by JOSE LEONARDO DIAZ on 4/07/18.
//  Copyright © 2018 MayorgaFirm Inc. All rights reserved.
//

import Cocoa

enum ProgrammingLanguage: String {
    case c = "C"
    case cPlusPlus = "C++"
    case go = "Go"
    case java = "Java"
    case javaScript = "JavaScript"
    case objectiveC = "Objective-C"
    case php = "PHP"
    case ruby = "Ruby"
    case rust = "Rust"
    case swift = "Swift"
    
    init?(for fileURL: URL, at level: Int) {
        let pathComponents = fileURL.pathComponents
        let directory = pathComponents[pathComponents.count - level]
        switch (directory, fileURL.pathExtension) {
        case ("c", "h"), (_, "c"): self = .c
        case ("cc", "h"), (_, "cc"), (_, "cpp"): self = .cPlusPlus
        case (_, "go"): self = .go
        case (_, "java"): self = .java
        case (_, "js"): self = .javaScript
        case ("objective-c", "h"), (_, "m"): self = .objectiveC
        case (_, "php"): self = .php
        case (_, "rb"): self = .ruby
        case (_, "rs"): self = .rust
        case (_, "swift"): self = .swift
        default:
            return nil
        }
    }
}
