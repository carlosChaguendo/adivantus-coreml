import turicreate as tc
import coremltools

#https://github.com/apple/coremltools/blob/master/docs/APIExamples.md
mlmodel = coremltools.models.MLModel('painting.mlmodel')
model = tc.load_model('painting.model')



data = tc.SFrame('training.sframe')

train_data, test_data = data.random_split(0.8)



scores = model.evaluate(train_data)


# Obtener segun oprediccion
#annotations = tc.SArray([{'label':'rose-pharmacy','type':'rectangle','coordinates':{'x':151.3649580919369,'y':45.57859608641785,'width':302.7299161838738,'height':91.1571921728357},'confidence':0.9662834616904112},{'label':'rose-pharmacy','type':'rectangle','coordinates':{'x':96.53737105823905,'y':25.36278793620423,'width':193.0747421164781,'height':50.72557587240846},'confidence':0.9730876957024531},{'label':'rose-pharmacy','type':'rectangle','coordinates':{'x':172.70339481157924,'y':47.318130494624825,'width':345.4067896231585,'height':94.63626098924965},'confidence':0.9634374952130588}])



test = tc.SFrame({'image': data['image']})
test['predictions'] = model.predict(test)

predictions_stacked = tc.object_detector.util.stack_annotations(test['predictions'])
print(predictions_stacked)

#test_data['image_with_ground_truth'] =  tc.object_detector.util.draw_bounding_boxes(test_data['image'], test_data['annotations'])

test['image_with_predictions'] =  tc.object_detector.util.draw_bounding_boxes(test['image'], test['predictions'])



test[['image', 'image_with_predictions']].explore()



im_zoom = tc.SArray([
tc.Image('test-images/zoom2/img_0501.jpg'),
tc.Image('test-images/zoom2/img_2055.jpg'),
tc.Image('test-images/zoom2/img_2540.jpg'),
tc.Image('test-images/zoom2/img_5384.jpg'),
tc.Image('test-images/zoom2/img_5644.jpg'),
tc.Image('test-images/zoom2/img_6513.jpg'),
tc.Image('test-images/zoom2/img_6926.jpg'),
tc.Image('test-images/zoom2/img_8241.jpg'),
tc.Image('test-images/zoom2/img_8343.jpg'),
tc.Image('test-images/zoom2/img_9850.jpg')
])



im1 = tc.SArray([
tc.Image('test-images/IMG_1754.JPG'),
tc.Image('test-images/IMG_5322.JPG'),
tc.Image('test-images/IMG_1780.JPG'),
tc.Image('test-images/IMG_7442.JPG'),
tc.Image('test-images/IMG_6248.JPG'),
tc.Image('test-images/IMG_7588.JPG'),
tc.Image('test-images/IMG_0622.JPG'),
tc.Image('test-images/IMG_4586.JPG'),
tc.Image('test-images/IMG_2662.JPG')
])
test = tc.SFrame({'image': im1})

test['predictions'] = model.predict(test)

predictions_stacked = tc.object_detector.util.stack_annotations(test['predictions'])

# El valor deberia estar entre 65 & 90
predictions_stacked.apply(lambda x: (100 * x['height']) / x['width'])



test['image_with_predictions'] =  tc.object_detector.util.draw_bounding_boxes(test['image'], test['predictions'])
test[['image', 'image_with_predictions']].explore()
