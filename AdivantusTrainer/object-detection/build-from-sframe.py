import turicreate as tc

data = tc.SFrame('training.sframe')
data['image_with_ground_truth'] =  tc.object_detector.util.draw_bounding_boxes(data['image'], data['annotations'])
data.explore()



# Make a train-test split
train_data, test_data = data.random_split(0.8)

# Use CPU
tc.config.set_num_gpus(0)

# Create a model using Turi Create’s object detector API
model = tc.object_detector.create(train_data,  feature='image', annotations='annotations', max_iterations=4000)




# Save the predictions to an SArray
predictions = model.predict(test_data)

# Evaluate the model and save the results into a dictionary
metrics = model.evaluate(test_data)

# Save the model for later use in Turi Create
# Important to save in case something after breaks the script
# Save the model for later use in Turi Create
model.save('painting.model')

# Export for use in Core ML file to the current directory
model.export_coreml('Painting.mlmodel',include_non_maximum_suppression=False)

