import Foundation
import PlaygroundSupport
import Cocoa
import Vision

guard #available(OSX 10.14, *) else {
    fatalError()
}

public extension FloatingPoint {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Self {
        let divisor = Self(Int(pow(10.0, Double(places))))
        return (self * divisor).rounded() / divisor
    }
}



import CoreML


Bundle.main.url(forResource: "Painting", withExtension: "mlmodelc")


let modelUrl = Bundle.main.url(forResource: "Painting", withExtension: "mlmodelc")!

let model = try MLModel(contentsOf: modelUrl)
print(model.description)
//let model = try! NLModel(contentsOf: url)

let visionModel = try! VNCoreMLModel.init(for: model)






let h = 1000

let container = NSView(frame:  NSRect(x: 0, y: 0, width: 1000, height: h))
//container.wantsLayer = true
//container.layer?.backgroundColor = NSColor.green.cgColor

let imageView = NSImageView(frame: NSRect(x: 0, y: 140, width: 1000, height: h - 140))
imageView.imageScaling = NSImageScaling.scaleAxesIndependently
//imageView.wantsLayer = true
//imageView.layer?.backgroundColor = NSColor.red.cgColor

let dragView = DragAndDropView()

container.addSubview(dragView)
container.addSubview(imageView)



func highlightArea(observation:VNDetectedObjectObservation, color: NSColor = .red) {
   let  boundingRect = observation.boundingBox
    let source = imageView.frame
    
    let rectWidth = source.size.width * boundingRect.size.width
    let rectHeight = source.size.height * boundingRect.size.height
    
    let outline = NSView()
    outline.frame = NSRect(x: boundingRect.origin.x * source.size.width, y:boundingRect.origin.y * source.size.height, width: rectWidth, height: rectHeight)
    
    
    let label = NSTextField()
    let confidence = (observation.confidence.rounded(toPlaces: 1) * 100 ).description
    label.stringValue = "Confidence \(confidence) %"
    label.sizeToFit()
    label.wantsLayer = true
    label.layer?.backgroundColor = color.withAlphaComponent(0.5).cgColor
    outline.addSubview(label)
    
    outline.wantsLayer = true
    outline.layer?.backgroundColor = color.withAlphaComponent(0.2).cgColor
    //    outline.layer.frame = NSRect(x: boundingRect.origin.x * source.size.width, y:boundingRect.origin.y * source.size.height, width: rectWidth, height: rectHeight)
    //    outline.layer.borderWidth = 2.0
    //    outline.layer.borderColor =color.cgColor
    imageView.addSubview(outline)
}





let horizontRequest = VNDetectTextRectanglesRequest.init { (rq, error) in
    guard let results = rq.results as? [VNTextObservation] else {
        return
    }
    
        print(results)
 
    
    for object in results {
        highlightArea(observation: object, color: NSColor.green)
    }

   
 
    
}

horizontRequest.preferBackgroundProcessing = true


let rectangleRequest = VNDetectRectanglesRequest.init { (rq, error) in

    guard let results = rq.results as? [VNRectangleObservation] else {
        return
    }
    
    for object in results {
        highlightArea(observation: object, color: NSColor.green)
    }
    print(results)
}

rectangleRequest.preferBackgroundProcessing = true




let visionRequest = VNCoreMLRequest.init(model: visionModel) { (reuest, error) in
    
    guard let results = reuest.results as? [VNDetectedObjectObservation] else {
        return
    }
    
    for object in results {
        highlightArea(observation: object)
    }
    dragView.stringValue = "Resultados \(results.count) "
    print(results)
}

visionRequest.imageCropAndScaleOption = .scaleFill
visionRequest.preferBackgroundProcessing = true



dragView.dragOperation = { (image:NSImage) -> Void in
    
    

    imageView.image = image
    
    while imageView.subviews.count > 1 {
        imageView.subviews.removeLast()
    }
    

    
    if let data = image.tiffRepresentation {
       
        do {
            let handler =  VNImageRequestHandler.init(data: data, options: [:])
            try handler.perform([ horizontRequest])
        } catch {
            print(error)
        }
    }
    
    
    //view.stringValue = model.predictedLabel(for: string) ?? ""
}

PlaygroundPage.current.liveView = container
PlaygroundPage.current.needsIndefiniteExecution = true
 
 

