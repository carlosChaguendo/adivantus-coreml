//: [Previous](@previous)

import Cocoa
import Foundation
import CreateML
import NaturalLanguage



enum Source: String {
    case medications =  "medicamentos"
    case crons = "crons-sin-prn"
    case cronsPrn = "crons"
    
    var modelName: String {
        switch self {
        case .medications: return "MedicationTextClassifier.mlmodel" //104.984 elements
        case .crons: return "CronsTextClassifier.mlmodel"
        case .cronsPrn: return "CronsPRNTextClassifier.mlmodel"
        }
    }
}



do {
    
    let source = Source.cronsPrn
    
    
    
    let url: URL = Bundle.main.url(forResource: source.rawValue, withExtension: "json")!
    
    
    
    let metadata = MLModelMetadata(
        author: "Carlos Chaguendo",
        shortDescription: "Un modelo entrenado para detectar \(source.rawValue) \(Date())",
        version: "1.0",
        additional:["Larry": "Capija"])
    
    
    let dataTable = try MLDataTable(contentsOf: url)
    
    
    print(dataTable.columnTypes)
    print(dataTable.size)
    
    let (trainingData, testingData) = dataTable.randomSplit(by: 0.8, seed: 5)
    
    let classifierText = try MLTextClassifier(trainingData: trainingData, textColumn: "token", labelColumn: "label")
    let evaluation = classifierText.evaluation(on: testingData)
    print("Evaluation")
    print(evaluation)
    print("==============Evaluation")
    
    let destinationPath1 = "/Users/carlos.chaguendo/Documents/ios4/adivantus-coreml/AdivantusTrainer/AdivantusTrainer/models/\(source.modelName)"
    try classifierText.write(to: URL(fileURLWithPath: destinationPath1), metadata: metadata)
    
}catch {
    print(error)
}



//: [Next](@next)
