//: [Previous](@previous)

import Foundation
import CreateML
import NaturalLanguage


extension Date {
    
    public func formatISO(timeZone: TimeZone = NSTimeZone.default) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter.string(from: self as Date)
    }
    
}


enum Source: String {
    case all =  "taggerV3.0"
    case url = "http-filldate"
    case medicationStrength = "MedicationStrength"
    
    
    var modelName: String {
        switch self {
        case .all: return "MedicationWordTagger.mlmodel" //104.984 elements
        case .url: return "UrlFillDate.mlmodel"
        case .medicationStrength: return "MedicationStrengthWordTagger.mlmodel"
            
        }
    }
}

guard
    let dataInfo =  Bundle.main.url(forResource: "dataInfoV3.0", withExtension: "json"),
    let dataSourceInfo = try? Data(contentsOf: dataInfo),
    let json = try? JSONSerialization.jsonObject(with: dataSourceInfo, options: []) as? [String: Any]
    else {
    preconditionFailure("NO existe")
}


var additionals = ["date": Date().formatISO()]

for (k,v) in json ?? [:] {
    additionals[k] = String(describing: v)
}

print(additionals)

let source = Source.all

var url: URL = Bundle.main.url(forResource: source.rawValue, withExtension: "json")! //2738 elements
let processStart = Date().timeIntervalSince1970

//: Se obtiene los datos del `.csv`
//: Se obtienen los datos de entrenamiento y de validacion
let dataTable = try MLDataTable(contentsOf: url)

print(dataTable.columnTypes)
print(dataTable.size)

let (trainingData, testingData) = dataTable.randomSplit(by: 0.8, seed: 5)

//NLLanguage.english
//maxEnt(revision: nil)

print("trainingData")
print(trainingData.size)


print("testingData")
print(testingData.size)



let tagger = try MLWordTagger.init(trainingData: trainingData, tokenColumn: "token", labelColumn: "label")



let evaluation = tagger.evaluation(on: testingData)
print("Evaluation")
print(evaluation)
print("==============Evaluation")




let end = Date().timeIntervalSince1970
let duration = end - processStart
//: > Metadatos para generar el modelo
//: >
//: > Se deberia agregar la fecha y la version
additionals["train-duration"] = String(describing: duration)

let metadata = MLModelMetadata(
    author: "Carlos Chaguendo Trasnh",
    shortDescription: "Un modelo entrenado para detectar partes de una prescipcion medica ",
    version: "3.1",
    additional: additionals)

let destinationPath1 = "/Users/carlos.chaguendo/Documents/datos/ios5.0/adivantus-coreml/AdivantusTrainer/AdivantusTrainer/models/\(source.modelName)"

let destinationPath2 = "/Users/carlos.chaguendo/Documents/datos/veratous-full-app/veratous/src/main/webapp/tesseract-traineddata/\(source.modelName)"





try tagger.write(to: URL(fileURLWithPath: destinationPath1), metadata: metadata)


try tagger.write(to: URL(fileURLWithPath: destinationPath2), metadata: metadata)












//: [Next](@next)





