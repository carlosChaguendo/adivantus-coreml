# tomado de
# https://www.raywenderlich.com/5213-natural-language-processing-on-ios-with-turi-create
# https://apple.github.io/turicreate/docs/userguide/text_classifier/


import turicreate as tc

data = tc.SFrame.read_json('medicamentos.json', orient='records')
data
model = tc.text_classifier.create(data, 'label', features=['token'], max_iterations=25)

#model.save('MedicationsTextClassifierTuri.model')
model.export_coreml('MedicationsTextClassifierTuri.mlmodel')
