# tomado de
# https://www.raywenderlich.com/5213-natural-language-processing-on-ios-with-turi-create
# https://apple.github.io/turicreate/docs/userguide/text_classifier/


import turicreate as tc
import winmltools
import coremltools


data = tc.SFrame.read_json('crons.json', orient='records')
data
model = tc.text_classifier.create(data, 'label', features=['token'])

model.save('CronsTextClassifierTuri.model')
model.export_coreml('CronsTextClassifierTuri.mlmodel')




model_coreml = coremltools.utils.load_spec("CronsTextClassifierTuri.mlmodel")
model_onnx = winmltools.convert.convert_coreml(model_coreml, "CronsTextClassifierTuriONNX")

# Save as text
winmltools.utils.save_text(model_onnx, "CronsTextClassifierTuriONNX.json")

# Save as protobuf
winmltools.utils.save_model(model_onnx, "CronsTextClassifierTuriONNX.onnx")

