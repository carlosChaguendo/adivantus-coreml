# adivantus-coreml

Estructura del repositorio

* __adivantus-coreml-exporter__ - Aplicacion java encargada de preparar los datos necesarios para entrenar un red neuronal
* __AdivantusTrainer__ - Applicacion macOS con los play grounds necesarios para entrenar un modelo usando CreateMl con los datos generados en `adivantus-coreml-exporter`
* __adivantus-ocr__ - Aplicacion de prueba con el tesseract y procesamiento de la imagenes
