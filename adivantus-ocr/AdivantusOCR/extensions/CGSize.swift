//
//  CGSize.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 20/02/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

extension CGSize {

	func scaleRatio(_ from: CGSize, to: CGSize) -> CGSize {

		let aspectWidth: CGFloat = to.width / from.width;
		let aspectHeight: CGFloat = to.height / from.height;
		let ratio = min(aspectWidth, aspectHeight)

		return CGSize(width: width * ratio, height: height * ratio)

	}

}
