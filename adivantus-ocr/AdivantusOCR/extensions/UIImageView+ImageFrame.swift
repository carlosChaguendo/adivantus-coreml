//
//  UIImageView+ImageFrame.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 23/02/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import Material

public extension UIImageView {

	public var imageScale: CGRect {

		let imgViewSize: CGSize = frame.size; // Size of UIImageView
		let imgSize = image!.size; // Size of the image, currently displayed

		// Calculate the aspect, assuming imgView.contentMode==UIViewContentModeScaleAspectFit

		let scaleW: CGFloat = imgViewSize.width / imgSize.width;
		let scaleH: CGFloat = imgViewSize.height / imgSize.height;
		let aspect: CGFloat = fmin(scaleW, scaleH);

		var imageRect = CGRect(x: 0, y: 0, width: imgSize.width * aspect, height: imgSize.height * aspect)

		// Note: the above is the same as :
		// CGRect imageRect=CGRectMake(0,0,imgSize.width*=aspect,imgSize.height*=aspect) I just like this notation better

		// Center image

		imageRect.origin.x = (imgViewSize.width - imageRect.size.width) / 2;
		imageRect.origin.y = (imgViewSize.height - imageRect.size.height) / 2;

		// Add imageView offset

		imageRect.origin.x += frame.origin.x;
		imageRect.origin.y += frame.origin.y;

		return imageRect

	}




}



public extension UIImage {
    
    
 
        func image(withRotation radians: CGFloat) -> UIImage {
            let cgImage = self.cgImage!
            let LARGEST_SIZE = CGFloat(max(self.size.width, self.size.height))
            let context = CGContext.init(data: nil, width:Int(LARGEST_SIZE), height:Int(LARGEST_SIZE), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: cgImage.bitmapInfo.rawValue)!
            
            var drawRect = CGRect.zero
            drawRect.size = self.size
            let drawOrigin = CGPoint(x: (LARGEST_SIZE - self.size.width) * 0.5,y: (LARGEST_SIZE - self.size.height) * 0.5)
            drawRect.origin = drawOrigin
            var tf = CGAffineTransform.identity
            tf = tf.translatedBy(x: LARGEST_SIZE * 0.5, y: LARGEST_SIZE * 0.5)
            tf = tf.rotated(by: CGFloat(radians))
            tf = tf.translatedBy(x: LARGEST_SIZE * -0.5, y: LARGEST_SIZE * -0.5)
            context.concatenate(tf)
            context.draw(cgImage, in: drawRect)
            var rotatedImage = context.makeImage()!
            
            drawRect = drawRect.applying(tf)
            
            rotatedImage = rotatedImage.cropping(to: drawRect)!
            let resultImage = UIImage(cgImage: rotatedImage)
            return resultImage
            
            
        }
    
    
    
    
    
    public func imageRotatedByDegrees(_ degrees: CGFloat, flip: Bool) -> UIImage {
        //        let radiansToDegrees: (CGFloat) -> CGFloat = {
        //            return $0 * (180.0 / CGFloat(Double.pi))
        //        }
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(Double.pi)
        }
        
        let size: CGSize
        if width > height {
            size = CGSize(width: height, height: width)
        } else {
            size = CGSize(width: width, height: height)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap!.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
        
        // // Rotate the image context
        bitmap!.rotate(by: degreesToRadians(degrees));
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if (flip) {
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap!.scaleBy(x: yFlip, y: -1.0)
        
        bitmap?.draw(cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}

