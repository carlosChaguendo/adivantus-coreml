//
//  CGPoint.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 27/01/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

enum CGPointMoveDirection {
    
    case tl, tr, bl, br
    
}

extension CGPoint {
    
    func plusx(_ v: CGFloat) -> CGPoint {
        return CGPoint(x: x + v, y: y)
    }
    
    func plusy(_ v: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: y + v)
    }
    
    func reverseY() -> CGPoint {
        return CGPoint(x: x, y: -y)
    }
    
    func move(_ dir: CGPointMoveDirection, value: CGFloat) -> CGPoint {
        
        var x = self.x
        var y = self.y
        
        switch dir {
        case .tl:
            x -= value
            y -= value
        case .tr:
            x += value
            y -= value
        case .bl:
            x -= value
            y += value
        case .br:
            x += value
            y += value
        }
        
        return CGPoint(x: x, y: y)
        
    }
    
//     mutating func toFixed(_ numberOfPlaces: CGFloat = 2.0) -> CGPoint {
//        return CGPoint(x: x.toFixed(numberOfPlaces), y: y.toFixed(numberOfPlaces))
//        
//    }
    
}
