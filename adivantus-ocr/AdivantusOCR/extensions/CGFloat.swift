//
//  CGFloat.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 27/01/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

extension CGFloat{
    

}


public extension FloatingPoint {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Self {
        let divisor = Self(Int(pow(10.0, Double(places))))
        return (self * divisor).rounded() / divisor
    }
}



public extension Sequence {
    
    public func groupBy<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U: [Iterator.Element]] {
        var dict: [U: [Iterator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) {
                dict[key] = [el]
                
            }
        }
        return dict
    }
    
    
    public func countBy<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U: Int] {
        var dict: [U: Int] = [:]
        for el in self {
            let key = keyFunc(el)
            if dict[key] == nil {
                dict[key] = 1
            } else {
                dict[key] = dict[key]! + 1
            }
            
            //if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
    
    
    
}
