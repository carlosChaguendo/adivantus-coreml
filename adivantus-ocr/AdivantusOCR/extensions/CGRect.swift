//
//  CGRect.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 27/01/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

extension CGRect {
    
    func scale(_ from: CGSize, to: CGSize) -> CGRect {
        
        ///
        /// Re-maps a number from one range to another. That is, a value of fromLow would get mapped to toLow,
        /// a value of fromHigh to toHigh, values in-between to values in-between, etc.
        ///
        func mapValue(_ x: CGFloat, in_min: CGFloat = 0, in_max: CGFloat, out_min: CGFloat = 0, out_max: CGFloat) -> CGFloat {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
        
        let x = mapValue(self.origin.x, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let y = mapValue(self.origin.y, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)
        let w = mapValue(self.width, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let h = mapValue(self.height, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)
        
        return CGRect(x: x, y: y, width: w, height: h)
        
    }
    
    func compress(_ v: CGFloat) -> CGRect {
        return CGRect(x: origin.x + v, y: origin.y + v, width: (width - v), height: (height - v))
    }
    
    func expand(_ v: CGFloat) -> CGRect {
        return CGRect(x: origin.x - v, y: origin.y - v, width: (width + v), height: (height + v))
        
    }
    
    var topLeft:CGPoint{
        get{
            return origin
        }
    }
    
    var topRight:CGPoint{
        get{
            return origin.plusx(width)
        }
    }
    
    var bottomLeft:CGPoint{
        get{
            return origin.plusy(height)
        }
    }
    
    var bottomRight:CGPoint{
        get{
            return origin.plusy(height).plusx(width)
        }
    }
    
}

