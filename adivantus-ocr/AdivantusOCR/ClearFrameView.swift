//
//  ClearFrameView.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 16/08/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

fileprivate extension UIBezierPath{
    
    func addLine(horizontalFrom point: CGPoint,size: CGFloat) -> UIBezierPath{
        self.move(to: point)
        self.addLine(to: point.plusx(size))
        return self
    }
    
    func addLine(verticalFrom point: CGPoint,size: CGFloat) -> UIBezierPath{
        self.move(to: point)
        self.addLine(to: point.plusy(size))
        return self
    }
}

@IBDesignable
class ClearFrameView: UIView {
    
    public var cropRect: CGRect = CGRect.zero
    public var baseSize = CGSize(width: 320, height: 500) {
        didSet{
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        let cameraButtonsSize: CGFloat = 100
        let availableFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: frame.width, height: frame.height - cameraButtonsSize))
        
       
        self.cropRect = CGRect(origin: CGPoint.zero, size: baseSize)//.compress(50)//.scale(baseSize, to: availableFrame.size)
        
        cropRect.origin.x = availableFrame.midX - cropRect.midX
        cropRect.origin.y = availableFrame.midY - cropRect.midY
        
        
        
        let path = UIBezierPath()
        path.lineWidth = 2
        
        let lw: CGFloat = path.lineWidth/2
        let lineSize: CGFloat = 30
        
        path
            .addLine(horizontalFrom: cropRect.topLeft.plusy(lw), size: lineSize)
            .addLine(horizontalFrom: cropRect.topRight.plusy(lw), size: -lineSize)
            .addLine(horizontalFrom: cropRect.bottomLeft.plusy(-lw), size: lineSize)
            .addLine(horizontalFrom: cropRect.bottomRight.plusy(-lw), size: -lineSize)
            // lineas verticales
            .addLine(verticalFrom: cropRect.topLeft.plusx(lw), size: lineSize)
            .addLine(verticalFrom: cropRect.topRight.plusx(-lw), size: lineSize)
            .addLine(verticalFrom: cropRect.bottomLeft.plusx(lw), size: -lineSize)
            .addLine(verticalFrom: cropRect.bottomRight.plusx(-lw), size: -lineSize)
            .close()
        
        
        
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext()!
        overlayViewDrawBg(rect)
        context.saveGState()
        
        // Draw crop area
        context.clear(cropRect)
        context.saveGState();
        
        UIColor.uicolorFromHex(0xFBC700).set()
        path.stroke()
        path.fill()
        
    }
    
    func overlayViewDrawBg(_ rect: CGRect) {
        
        // Background Color
        UIColor.black.withAlphaComponent(0.5).setFill()
        let cornerOffset: CGFloat = 0
        // Draw
        let path = UIBezierPath(rect: rect.insetBy(dx: cornerOffset, dy: cornerOffset))
        path.fill()
        
    }
    
    
    
    
    
}

