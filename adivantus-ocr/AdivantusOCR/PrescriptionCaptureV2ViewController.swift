//
//  PrescriptionCaptureV2ViewController.swift
//  AdivantusIphone
//
//  Created by carlos chaguendo on 16/08/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit
import AVFoundation
import CoreImage
import PKHUD

public protocol PrescriptionCaptureViewDelegate: NSObjectProtocol {
    
    func prescriptionCaptureViewController(viewController: UIViewController, didContinueActionWith croppedImage: UIImage)
    
}

public class PrescriptionCaptureV2ViewController: UIViewController {
    
    public weak var delegate: PrescriptionCaptureViewDelegate?
    
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var preview: UIView!
    @IBOutlet weak var livePreview: UIView!
    @IBOutlet weak var cropView: ClearFrameView!
    
    private var session: AVCaptureSession?
    private var stillImageOutput: AVCaptureStillImageOutput?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    @IBOutlet var buttons: [UIButton]!
    
    override public func loadView() {
        Bundle.init(for: PrescriptionCaptureV2ViewController.self).loadNibNamed("PrescriptionCaptureV2ViewController", owner: self, options: [:])
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.cropView.baseSize = CGSize(width: 320 * 1.2, height: 500 * 1.2)
        }
        
        buttons.forEach({
            $0.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
        })
        
        
        //        imgPreview.isHidden = true
        preview.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.async(execute: {
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindowLevelStatusBar + 1
            }
        })
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        DispatchQueue.main.async(execute: {
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindowLevelNormal
            }
        })
        
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if session == nil {
            addVideoLayerToLivePreview()
        } else {
            session!.startRunning()
        }
    }
    
    
    private func addVideoLayerToLivePreview() {
        session = AVCaptureSession()
        
        if session!.canSetSessionPreset(AVCaptureSessionPresetPhoto) {
            session!.sessionPreset = AVCaptureSessionPresetPhoto
        }
        
        do {
            
            let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            let input = try AVCaptureDeviceInput(device: backCamera)
            
            
            try backCamera?.lockForConfiguration()
            
            if backCamera?.isFocusModeSupported(.continuousAutoFocus) == true {
                 backCamera?.focusMode = .continuousAutoFocus
            }
            
            if backCamera?.isSmoothAutoFocusSupported == true {
               backCamera?.isSmoothAutoFocusEnabled = true
            }
           
            if backCamera?.activeFormat.isVideoHDRSupported == true {
                backCamera?.automaticallyAdjustsVideoHDREnabled = false
                backCamera?.isVideoHDREnabled = true
            }
            
            if backCamera?.isExposureModeSupported(.continuousAutoExposure)  == true{
                backCamera?.exposureMode = .continuousAutoExposure
            }
            backCamera?.unlockForConfiguration()
            
            
            
            if session!.canAddInput(input) {
                session!.addInput(input)
                
                
//                / se depretco
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                stillImageOutput?.isHighResolutionStillImageOutputEnabled = true
                

                if stillImageOutput?.isStillImageStabilizationSupported == true {
                    stillImageOutput?.automaticallyEnablesStillImageStabilizationWhenAvailable = true
                }
                
                
                let deviceOutput = AVCaptureVideoDataOutput()
                deviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
                deviceOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.default))
               deviceOutput.alwaysDiscardsLateVideoFrames = true
                session?.addOutput(deviceOutput)
           
                
                
                //                session?.addOutput(deviceOutput)
                
                if session!.canAddOutput(stillImageOutput) {
                    session!.addOutput(stillImageOutput)
                    
                    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                    videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait

                    videoPreviewLayer!.connection?.preferredVideoStabilizationMode = .cinematic
                    
                    livePreview.layer.insertSublayer(videoPreviewLayer!, at: 0)
                    let rec = CGRect(origin: CGPoint(x: 0, y: 0), size: UIScreen.main.bounds.size)
                    videoPreviewLayer!.frame = rec
                    
                    session!.startRunning()
                    
                }
                
            }
            
        } catch let error as NSError {

        }
        
    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.landscapeLeft]
    }
    
    override public var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeLeft
    }
    
    
    class func fixImageOrientation(_ src: UIImage?) -> UIImage? {
        
        guard let src = src else {
            return nil
        }
        
        
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat((Double.pi/2)))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-(Double.pi/2)))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg: CGImage = ctx.makeImage()!
        let img: UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    
    
    

    /**
     
     */
    private func captureImage(videoConnection: AVCaptureConnection, completion: @escaping (UIImage) -> Void) {
        
        stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (sampleBuffer, error) -> Void in
            
            if let error = error {
        
                
            }else if let sampleBuffer = sampleBuffer {
                
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                let image = UIImage(data: imageData!, scale: UIScreen.main.scale)!
                
                let maxResolution = [self.view.frame.width,self.view.frame.height].max()!
                let imageFixOrientation = PrescriptionCaptureV2ViewController.fixImageOrientation(image)!
                
                print("Original image size \(image.size) maxResolution:\(maxResolution)")
                
                print("UIScreen.main.scale \(UIScreen.main.scale)")
                
                let cameraButtonsSize: CGFloat = 100
                var cropRect = self.cropView.cropRect
                /// Nota: sitema de cordenadas de core images es contrario al uikit
                cropRect.origin = cropRect.origin.plusy(cameraButtonsSize)
                
                
   
                self.imgPreview.image = imageFixOrientation
                
                let sizeOfImage = imageFixOrientation.size
                let sizeOfView = self.imgPreview.imageScale.size
                
                
                print("cropRect \(cropRect) \(self.cropView.cropRect)")
                cropRect = cropRect.scale(sizeOfView , to: sizeOfImage)
                print("cropRect \(cropRect)")
                
                var ciImage = self.convertCGImageToCIImage(imageFixOrientation.cgImage!)!
                // El sistema de coordenadas de CoreImage es contrario al de UIKit
                ciImage = self.cropOverlayForPoints(ciImage, topLeft: cropRect.bottomLeft, topRight: cropRect.bottomRight, bottomLeft: cropRect.topLeft, bottomRight: cropRect.topRight)
                
                let toImage = UIImage(ciImage: ciImage)
                
                let i  = self.preventCGImageNil(image: toImage)
                let out = i.imageRotatedByDegrees(-90, flip: false)
                
                completion(out)
                
            }else {
                preconditionFailure()
            }
        })
    }
    
    open func cropOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        
        var businessCard: CIImage
        businessCard = image.applyingFilter("CIPerspectiveTransformWithExtent",
                                            withInputParameters: [
                                                "inputExtent": CIVector(cgRect: image.extent),
                                                "inputTopLeft": CIVector(cgPoint: topLeft),
                                                "inputTopRight": CIVector(cgPoint: topRight),
                                                "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                                "inputBottomRight": CIVector(cgPoint: bottomRight)
            ])
        businessCard = image.cropping(to: businessCard.extent)
        
        return perspectiveCorrection(businessCard, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
    }
    
    open func perspectiveCorrection(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        
        return image.applyingFilter("CIPerspectiveCorrection", withInputParameters: [
            
            "inputTopLeft": CIVector(cgPoint: topLeft),
            "inputTopRight": CIVector(cgPoint: topRight),
            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
            "inputBottomRight": CIVector(cgPoint: bottomRight)
            
            ])
        
    }
    
    open  func convertCGImageToCIImage(_ inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage)
        return ciImage
    }
    
    fileprivate func applyGlobalFilters(image: UIImage) -> CIImage {
        

        return image.ciImage!
        
        // return ImageStudio.applyNoir(originOverlay)//.g8_blackAndWhite()
        
    }
    
    // MARK: - Camera Actions
    
    @IBAction func closeAction(_ sender: Any) {
        if navigationController?.viewControllers.first == self {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func reTakeAction(_ sender: Any) {
        
        self.preview.isHidden = true
        self.imgPreview.image = nil
        
        session!.startRunning()
        livePreview.isHidden = false
    }
    
    @IBAction func takePhotoAction(_ sender: Any) {
        
        if stillImageOutput == nil {

            // postAlert("Camera inaccessable", message: "Application cannot access the camera.")
            return
        }
        
        if let videoConnection = stillImageOutput!.connection(withMediaType: AVMediaTypeVideo) {
            stillImageOutput?.isHighResolutionStillImageOutputEnabled = true
            
            captureImage(videoConnection: videoConnection) { (img) in
                // detiene la gravacion de vieo
                self.session!.stopRunning()
                self.livePreview.isHidden = true
                self.preview.isHidden = false
                self.imgPreview.image = img
            }
        }
    }
    
    @IBAction func saveInImage(_ sender: Any) {
        guard let img = self.imgPreview.image else {
            return
        }
        
        PKHUD.sharedHUD.show()
        HUD.show(.progress)
        
        let i  = preventCGImageNil(image: img)
        UIImageWriteToSavedPhotosAlbum(i, self, #selector(saveSuccesfull(_:didFinishSavingWithError:contextInfo:)), nil);
    }
    
    
     func preventCGImageNil(image: UIImage) -> UIImage {
        if image.cgImage == nil {
            return UIImage(cgImage: convertCIImageToCGImage(image.ciImage!))
        }
        return image
    }
    
     func convertCIImageToCGImage(_ inputImage: CIImage) -> CGImage! {
        let context = CIContext(options: nil)
        return context.createCGImage(inputImage, from: inputImage.extent)
        
    }
    
    internal func saveSuccesfull(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        HUD.hide()
        
        if let error = error {
            HUD.flash(.labeledError(title: "Error", subtitle: error.localizedDescription))
        } else {
            HUD.flash(.success, delay: 1.0)
            reTakeAction(self)
        }
    }
    
    @IBAction func useCurrentImage() {
        guard let img = self.imgPreview.image else {
            return
        }
       
        if delegate == nil {

        } else {
            delegate?.prescriptionCaptureViewController(viewController: self, didContinueActionWith: img)
        }
    }
    
    
    
}


extension PrescriptionCaptureV2ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutputSampleBuffer sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        //
        //        let context = CIContext()
        //        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { preconditionFailure() }
        //CIComicEffect, CIHeightFieldFromMask

        
        
        DispatchQueue.main.async { [unowned self] in
            //              self.livePreview.isHidden = true
            //                    self.preview.isHidden = false
            //             self.imgPreview.image = image
        }
        
    }
}

