//
//  EvilChineseIdCard.swift
//  Emboss
//
//  Created by Carlos Chaguendo on 24/07/18.
//  Copyright © 2018 Jayway. All rights reserved.
//

import UIKit

public struct Configuration {
    
    public static var `default`: Configuration { return Configuration() }
    
    public var colorMonochromeFilterInputColor: CIColor? // CIColorMonochrome kCIInputColorKey 参数
    public var colorControls: (CGFloat, CGFloat, CGFloat) // CIColorControls Saturation, Brightness, Contrast
    public var exposureAdjustEV: CGFloat // CIExposureAdjust IInputEVKey
    
    public var gaussianBlurSigma: Double
    
    public var smoothThresholdFilter: (CGFloat, CGFloat)? // inputEdgeO, inputEdge1
    
    public var unsharpMask: (CGFloat, CGFloat) // Radius, Intensity
    
    init() {
        colorMonochromeFilterInputColor = CIColor(red: 0.0, green: 0.0, blue: 0.75, alpha: 0.6)//CIColor(red: 0.75, green: 0.75, blue: 0.75)
        colorControls = (0.4, 0.2, 1.1)
        exposureAdjustEV = 0.7
        gaussianBlurSigma = 0.4
        smoothThresholdFilter = (0.35, 0.85)
        unsharpMask = (2.5, 0.5)
    }
    
    
    public func process(conf: Configuration = Configuration.`default`, inputImage2: CIImage) -> CIImage  {
        
        var inputImage: CIImage = inputImage2
          CustomPhotoAlbum.createDirectory(named: "evil")
             CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "input")
        // 只有在主动设置的时候才丢弃颜色信息
        if let color = conf.colorMonochromeFilterInputColor {
            // 0x00. 灰度图 --> 主要用来做文字识别所以直接去掉色彩信息
            inputImage = inputImage.applyingFilter("CIColorMonochrome", withInputParameters: [kCIInputColorKey: CIColor(red: 0.75, green: 0.75, blue: 0.75)])
            
               CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "CIColorMonochrome")
       
  
        }
        
        // 0x01. 提升亮度 --> 会损失一部分背景纹理 饱和度不能太高
//        inputImage = inputImage.applyingFilter("CIColorControls", withInputParameters: [
//            kCIInputSaturationKey: conf.colorControls.0,
//            kCIInputBrightnessKey: conf.colorControls.1,
//            kCIInputContrastKey: conf.colorControls.2])
//          CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "CIColorControls")

        
        // 0x02 曝光调节
        inputImage = inputImage.applyingFilter("CIExposureAdjust", withInputParameters: [kCIInputEVKey: conf.exposureAdjustEV])
CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "CIExposureAdjust")
        
        // 0x03 高斯模糊
        if #available(iOS 10.0, *) {
            inputImage = inputImage.applyingGaussianBlur(withSigma: conf.gaussianBlurSigma)
            CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "applyingGaussianBlur")
        } else {
            // Fallback on earlier versions
        }
    
        
        if let sf = conf.smoothThresholdFilter {
            // 0x04. 去燥
            inputImage = SmoothThresholdFilter(inputImage,
                                               inputEdgeO: sf.0,
                                               inputEdge1: sf.1).outputImage ?? inputImage
            
             CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "smoothThresholdFilter")
        
        }
        
        // 0x05 增强文字轮廓
        inputImage = inputImage.applyingFilter("CIUnsharpMask",
                                               withInputParameters: [kCIInputRadiusKey: conf.unsharpMask.0, kCIInputIntensityKey: conf.unsharpMask.1])

        
        CustomPhotoAlbum.createDirectory(named: "evil")
        CustomPhotoAlbum.save(image: UIImage(ciImage: inputImage), inDirectory: "evil", named: "input")
        
        return inputImage
    }
    
}
