//
//  ViewController.swift
//  Emboss
//
//  Created by Paolo Longato on 12/03/2017.
//  Copyright © 2017 Jayway. All rights reserved.
//




import UIKit
import Vision
import CoreImage
import AdivantusOCR
import TesseractOCR


public extension UIImage {
    
    
    public func fixOrientation() -> UIImage {
        return self
    }
}


extension CGPoint {
    
    func plusx(_ v: CGFloat) -> CGPoint {
        return CGPoint(x: x + v, y: y)
    }
    
    func plusy(_ v: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: y + v)
    }
}

extension CIDetector {
    
    open func features<T : CIFeature>(in image: CIImage, type: T.Type ) -> [T] {
        if let fectu = self.features(in: image) as? [T] {
            return fectu
        }
        
        return []
    }
    
}

class ViewController: UIViewController {

    // Play around with these parameters and build for device.
    private let inputText = "a"
    private let embossTexture = UIImage(named: "texture2")!
    private let fontSize: CGFloat = 28
    private var totalImages = 0

    @IBOutlet weak var imageView: UIImageView!


    let pstyle = NSMutableParagraphStyle()
    var fonts: [UIFont] = []
    var styles: [[String: Any]] = []

    var panagrams: [String] = []


    override func viewDidLayoutSubviews() {
        pstyle.alignment = NSTextAlignment.left

        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)

        print(path)
        loadFontnames()


//        panagrams.append("José compró una vieja zampoña en Perú. Excusándose, Sofía tiró su whisky al desagüe de la banqueta.")
//        panagrams.append("El cadáver de Wamba, rey godo de España, fue exhumado y trasladado en una caja de zinc que pesó un kilo")
//        panagrams.append("El veloz murciélago hindú comía feliz cardillo y kiwi. La cigüeña tocaba el saxofón detrás del palenque de paja")
//        panagrams.append("El viejo Señor Gómez pedía queso, kiwi y habas, pero le ha tocado un saxofón.")
//        panagrams.append("Jovencillo emponzoñado de whisky: ¡qué figurota exhibe!")
//        panagrams.append("Whisky bueno: ¡excitad mi frágil pequeña vejez!")
//        panagrams.append("Quiere la boca exhausta vid, kiwi, piña y fugaz jamón")
//        panagrams.append("Cien kilogramos pesó el extranjero, que vive y se exhibe fumando.")
//        panagrams.append("Jackdaws love my big sphinx of quartz")
//        panagrams.append("The quick brown fox jumps over the lazy dog1")
//        panagrams.append("Grumpy wizards make toxic brew for the evil queen and Jack")
//        panagrams.append("Necesitamos unha tipografía chuliña de cor kiwi, que lle zorregue unha labazada visual á xente")

//        panagrams.append("ROSE PHARMACY")
//        panagrams.append("Chirstiano, Joseph")
//        panagrams.append("MIRTAZAINE 1%MG")
//        panagrams.append("TAKE 1.5 TABLETS BY MOUNTH")
//        panagrams.append("BEDTIME")
//          panagrams.append("Chirstiano, Josephine")
//        panagrams.append("VITAMIN D3")
//        panagrams.append("REFILLS: 2")
//        panagrams.append("Rx# 965443")
//         panagrams.append("Chirstiano, Joseph\nVITAMIN D3")


//        panagrams.append("TAKE 1 CAPSULE BY MOUNTH DAYLY\nBEDTIME (FOR GERD)")

        panagrams.append("TAKE 1.5 TABLETS BY")
        panagrams.append("Dr. HUERTA, ALBERT")
        panagrams.append("Rx# 966717")
        panagrams.append("17 CG")
        panagrams.append("MIRTAZAPINE 15MG")
        panagrams.append("Christiano, Joseph")
        panagrams.append("Generic for : REMERON")
        panagrams.append("TAKE 1.5 TABLETS")
        panagrams.append("BEDTIME")
        panagrams.append("REFILLS:3 LF: 12/02/20 czgty#45 Exp Date: 01/02/201")





//         styles.append([NSFontAttributeName: UIFont.systemFont(ofSize: fontSize ), NSForegroundColorAttributeName: UIColor.black,  NSParagraphStyleAttributeName: pstyle])

        let font = UIFont.boldSystemFont(ofSize: fontSize)
        let stringAttributes = [NSFontAttributeName: font,
                                NSForegroundColorAttributeName: UIColor.white]
        let size = imageView.frame.size
        imageView.image = UIImage
            .image(from: inputText, attributes: stringAttributes, size: size)?
            .applyEmboss(shadingImage: embossTexture)

    }
    
    
    /**
     Instancia de teseract con la configuracion mas optima
     
     * [Disabling the dictionaries](https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality#dictionaries-word-lists-and-patterns)
     * [Page Segmentation Mode](https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality#page-segmentation-method)
     
     */
    lazy var tesseract: G8Tesseract = {
        guard let tesseract = G8Tesseract(
            language: "eng",
            configDictionary: nil,
            configFileNames: nil,
            cachesRelatedDataPath: "adivantus-ocr/cache2",
            // Opcion de escaneo mas precisa pero lenta
            engineMode: .tesseractCubeCombined
            ) else{
                preconditionFailure()
        }
        tesseract.pageSegmentationMode = .singleBlock
        tesseract.maximumRecognitionTime = 60.0
//        tesseract.delegate = self.G8delegate
        
        // se usa `NSString(string: "1").boolValue` por lo que:
        // 0   || F = false
        // [1-9] || T = true
        
        tesseract.setVariablesFrom([
            kG8ParamLoadFreqDawg: "F",
            kG8ParamLoadSystemDawg: "F",
            kG8ParamTesseditWriteImages: "T"
            ])
        
        
        
        return tesseract
    }()
    
    func testtesseract(image:UIImage) {
        
        
//        tesseract.image = image
//        tesseract.charWhitelist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,./-+()&"
//
//        tesseract.recognize()
//        print("Textx \n")
//       print(tesseract.recognizedText)
//
//
//
//        if let blocks = ( tesseract.recognizedBlocks(by: G8PageIteratorLevel.word) as? [G8RecognizedBlock] ) {
//            for block in blocks {
//
//                if block.confidence < 40 { continue }
//                print(block)
//            }
//
//        }
        print("\n")
        let ciImage = CIImage(image: image)!
        
        /// Se calcula el angulo mas commun en los textos de la imagen
        /// para rotar la imagen
        let maxAlfa = textDetector!.features(in: ciImage, type: CITextFeature.self)
            .map { atan2($0.topRight.y - $0.topLeft.y, $0.topRight.x - $0.topLeft.x).rounded(toPlaces: 2) }
            .countBy{ $0 }
            .reduce((0,0)) { (prev, next) -> (CGFloat, Int) in
                return next.value >  prev.1 ? next : prev
            }
        
        var rotate =  image.image(withRotation: -maxAlfa.0)
        
        save(image: rotate, inDirectory: "cidetectorimages", named: "withRotation max"  )


        
        
        
    }

    func loadFontnames() {

        styles.removeAll()

        for family in UIFont.familyNames {
            let familyString = family as NSString
            let familyl = family.lowercased()
            print("Family:\(family)")

            for name in UIFont.fontNames(forFamilyName: familyString as String) {

                let namel = name.lowercased()
                if 1 + 1 == 2 {
                    if familyl.contains("Bodoni Ornaments") ||
                        familyl.contains("American Typewriter") ||
                        familyl.contains("Avenir".lowercased()) ||
                        familyl.contains("Avenir Next".lowercased()) ||
                        familyl.contains("Bodoni 72 Oldstyle".lowercased()) ||
                        familyl.contains("Bodoni 72".lowercased()) ||
                        familyl.contains("Chalkboard SE".lowercased()) ||
                        familyl.contains("Cochin".lowercased()) ||
                        familyl.contains("Euphemia UCAS".lowercased()) ||
                        familyl.contains("Futura".lowercased()) ||
                        familyl.contains("Gill Sans".lowercased()) ||
                        familyl.contains("Gurmukhi MN".lowercased()) ||
                        familyl.contains("Noteworthy".lowercased()) ||
                        familyl.contains("Marker Felt".lowercased()) ||
                        familyl.contains("Noto Nastaliq Urdu".lowercased()) ||
                        familyl.contains("PingFang HK".lowercased()) ||
                        familyl.contains("PingFang SC".lowercased()) ||
                        familyl.contains("PingFang TC".lowercased()) ||
                        familyl.contains("Sinhala Sangam MN".lowercased()) ||
                        familyl.contains("Snell Roundhand".lowercased()) ||
                        familyl.contains("Thonburi".lowercased()) ||
                        familyl.contains("Savoye LET".lowercased()) ||
                        familyl.contains("Oriya Sangam MN".lowercased()) ||
                        familyl.contains("Khmer Sangam MN".lowercased()) ||
                        familyl.contains("Kohinoor".lowercased()) ||
                        familyl.contains("Gujarati".lowercased()) ||
                        familyl.contains("Damascus".lowercased()) ||
                        familyl.contains("symbol".lowercased()) ||
                        familyl.contains("courier".lowercased()) ||
                        familyl.contains("trebuchet".lowercased()) ||
                        familyl.contains("bradley".lowercased()) ||
                        familyl.contains("Zapfino".lowercased()) ||

                        /// Se confunde la a con la s
                        familyl.contains("american typewriter") ||
                        familyl.contains("baskerville") ||
                        familyl.contains("charter".lowercased()) ||
                        familyl.contains("devanagari".lowercased()) ||
                        familyl.contains("didot".lowercased()) ||
                        familyl.contains("farah".lowercased()) ||
                        familyl.contains("geeza pro".lowercased()) ||
           
                        familyl == "times new roman" ||
                        
                        // por nombres
                        namel.contains("Italic".lowercased()) ||
                        namel.contains("Oblique".lowercased()) ||
                        namel.contains("Academy".lowercased()) ||
                        namel.contains("Avenir".lowercased()) ||
                        namel.contains("BodoniSvtyTwoI".lowercased()) ||
                        namel.contains("BodoniOrnamen".lowercased()) ||
                        namel.contains("Chalkboar".lowercased()) ||
                        namel.contains("Chalkduster".lowercased()) ||
                        namel.contains("DINCondensed".lowercased()) ||
                        namel.contains("Futura-".lowercased()) ||
                        namel.contains("Georgia".lowercased()) ||
                        namel.contains("GillSans".lowercased()) ||
                        namel.contains("GurmukhiMN".lowercased()) ||
                        namel.contains("HoeflerText".lowercased()) ||
                        namel.contains("MarkerFelt".lowercased()) ||
                        namel.contains("Noteworthy".lowercased()) ||
                        namel.contains("NotoNastaliq".lowercased()) ||
                        namel.contains("artyLetPlain".lowercased()) ||
                        namel.contains("PingFang".lowercased()) ||
                        namel.contains("Rockwell".lowercased()) ||
                        namel.contains("SinhalaSangam".lowercased()) ||
                        namel.contains("SnellRoundhand".lowercased()) ||
                        namel.contains("Thonburi".lowercased()) ||
                        namel.contains("applecoloremoji".lowercased()) ||
                        namel.contains("applesdgothicneo".lowercased()) ||
                        namel.contains("symbol".lowercased()) ||
                        namel.contains("ms_trebuchetms".lowercased()) ||
                        namel.contains("menlo".lowercased()) ||
                        namel == ("helveticaneue") ||
                        namel == "arialhebrew" ||
                        namel == "arialhebrew-light" ||
                        namel.contains("Verdana") {
                        continue
                    }
                }


                print("\tName:\(name)")

                let font = UIFont(name: name, size: fontSize)!
                styles.append([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.black, NSParagraphStyleAttributeName: pstyle])

            }
        }

        print("styles \(styles.count)")

    }

    func createDirectory(named: String) {

        let fileManager = FileManager.default

        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)

        if !fileManager.fileExists(atPath: paths) {
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        } else {
            print("Already dictionary created.")
        }

    }

    func deleteDirectory(named: String) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)
        if fileManager.fileExists(atPath: paths) {
            try! fileManager.removeItem(atPath: paths)
        } else {
            print("Something wronge.")
        }

    }

    func save(image img: UIImage, inDirectory directory: String, named name: String) {

        var image = preventCGImageNil(image: img)
        
        let fileManager = FileManager.default

        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(directory)/\(name.lowercased()).jpg")
//        print(paths)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }


    func createImagen(from inputText: String) {
        for style in styles {

            let titleSize = CGSize(width: fontSize, height: fontSize + 10)
            guard let image = UIImage.image(from: inputText, attributes: style, size: titleSize) else {
                print("\tNo se pudo crea")
                continue
            }
            print("\t \(inputText)")
            let font = (style[NSFontAttributeName] as! UIFont)
            let name = "\(inputText)_\(font.familyName)_\(font.fontName)_\(Date().timeIntervalSince1970)"

//            if let img2 = resize(image: image, newSize: CGSize(width: 18, height: 18)) {
//                save(image: image, inDirectory: inputText.lowercased(), named: "\(name)-18*18")
//            }
            let category = inputText.lowercased()
            save(image: image, inDirectory: inputText.lowercased(), named: name)

            if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.5, height: titleSize.height * 0.5)) {
                save(image: resized, inDirectory: category, named: "\(name)-0.5x")
            }

            if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.25, height: titleSize.height * 0.25)) {
                save(image: resized, inDirectory: category, named: "\(name)-0.25x")
            }

            if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.20, height: titleSize.height * 0.20)) {
                save(image: resized, inDirectory: category, named: "\(name)-0.20x")
            }



            totalImages += 1
            DispatchQueue.main.async { [unowned self] in
                self.imageView.image = image
            }
        }
    }


    func createImageForAllFonts() {
        totalImages = 0
        for panagrama in panagrams {

            for style in styles {
                let titleSize = (panagrama as NSString).size(attributes: style)
                guard let image = UIImage.image(from: panagrama, attributes: style, size: titleSize) else {
                    continue
                }
                let font = (style[NSFontAttributeName] as! UIFont)
                let category = "\(font.familyName)-\(font.fontName)"
                createDirectory(named: category)

                if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.5, height: titleSize.height * 0.5)) {
                    save(image: resized, inDirectory: category, named: "\(Date().timeIntervalSince1970)-0.5x")
                }

                if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.25, height: titleSize.height * 0.25)) {
                    save(image: resized, inDirectory: category, named: "\(Date().timeIntervalSince1970)-0.25x")
                }

                if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.20, height: titleSize.height * 0.20)) {
                    save(image: resized, inDirectory: category, named: "\(Date().timeIntervalSince1970)-0.10x")
                }

                if let resized = resize(image: image, newSize: CGSize(width: titleSize.width * 0.15, height: titleSize.height * 0.15)) {
                    save(image: resized, inDirectory: category, named: "\(Date().timeIntervalSince1970)-0.15x")
                }


                save(image: image, inDirectory: category, named: "\(Date().timeIntervalSince1970)-1.0x")
            }
        }

        print("total \(panagrams.count * styles.count * 4)")
    }

    open func convertCIImageToCGImage(_ inputImage: CIImage) -> CGImage! {
        let context = CIContext(options: nil)
        return context.createCGImage(inputImage, from: inputImage.extent)

    }

    public func preventCGImageNil(image: UIImage) -> UIImage {
        if image.cgImage == nil {
            return UIImage(cgImage: convertCIImageToCGImage(image.ciImage!))
        }
        return image
    }

    func extractFromImage(croppedImage: UIImage, directory: String = "partes2") {

        createDirectory(named: directory)
        if #available(iOS 11.0, *) {
            let detectTextRequest = VNDetectTextRectanglesRequest()

            detectTextRequest.reportCharacterBoxes = true
            let image = croppedImage.ciImage ?? CIImage(image: croppedImage)!

            let handler = VNImageRequestHandler(ciImage: image, options: [:])
            try? handler.perform([detectTextRequest])

            guard let textObservations = detectTextRequest.results as? [VNTextObservation] else {
                print("Not found")
                return
            }

            let ciImage = image


            for textObservation in textObservations {
                guard let cs = textObservation.characterBoxes else { continue }


                let rectWord = textObservation.boundingBox.scaled(to: ciImage.extent.size)
                var wordImage = ciImage.cropping(to: CGRect(x: rectWord.origin.x - 3, y: rectWord.origin.y - 3, width: rectWord.width + 6, height: rectWord.height + 6))
                    .applying(CGAffineTransform(translationX: -rectWord.origin.x, y: -rectWord.origin.y))

                save(image: preventCGImageNil(image: UIImage(ciImage: wordImage)), inDirectory: directory, named: "word-\(Date().timeIntervalSince1970)")

                for c in cs {
                    if 1 == 1 {
                        continue
                    }

                    let rect = c.boundingBox.scaled(to: ciImage.extent.size)

                    var image = ciImage.cropping(to: CGRect(x: rect.origin.x - 3, y: rect.origin.y - 3, width: rect.width + 6, height: rect.height + 6))
                        .applying(CGAffineTransform(translationX: -rect.origin.x, y: -rect.origin.y))
                    //                        if let size = resize {
                    // 将文字切割出来 缩放到`size`
                    //                            image = image.resize(size)
                    //                        }
                    //                if adjustment {
                    //                    image = SmoothThresholdFilter(image, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage ?? image
                    //                    debugger?(image)
                    //                    image = AdaptiveThresholdFilter(image).outputImage ?? image
                    //                    debugger?(image)
                    //                }

                    // results.append(Value(image, rect))
//                    self.imageView.image = UIImage(ciImage: image)


//                    image = SmoothThresholdFilter(image, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage ?? image
//                    image = AdaptiveThresholdFilter(image).outputImage ?? image

                    save(image: preventCGImageNil(image: UIImage(ciImage: image)), inDirectory: directory, named: "char-\(Date().timeIntervalSince1970)")

                    //save(image: UIImage(ciImage: image), inDirectory: "prescriptions", named: "\(Date().timeIntervalSince1970)")

                    //UIImageWriteToSavedPhotosAlbum(UIImage(ciImage: image), self, #selector(saveSuccesfull(_:didFinishSavingWithError:contextInfo:)), nil);
                }
            }
        }

    }



    open func drawHighlightOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint,
                                            bottomLeft: CGPoint, bottomRight: CGPoint, color: CIColor = CIColor(red: 0.0, green: 0, blue: 1, alpha: 0.6)) -> CIImage {

        var overlay = CIImage(color: color)
        overlay = overlay.cropping(to: image.extent)
        overlay = overlay.applyingFilter("CIPerspectiveTransformWithExtent",
                                         withInputParameters: [
                                             "inputExtent": CIVector(cgRect: image.extent),
                                             "inputTopLeft": CIVector(cgPoint: topLeft),
                                             "inputTopRight": CIVector(cgPoint: topRight),
                                             "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                             "inputBottomRight": CIVector(cgPoint: bottomRight)
                                         ])

        return overlay.compositingOverImage(image)
    }
    
    open  func cropOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        
        var businessCard: CIImage
        businessCard = image.applyingFilter("CIPerspectiveTransformWithExtent",
                                            withInputParameters: [
                                                "inputExtent": CIVector(cgRect: image.extent),
                                                "inputTopLeft": CIVector(cgPoint: topLeft),
                                                "inputTopRight": CIVector(cgPoint: topRight),
                                                "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                                "inputBottomRight": CIVector(cgPoint: bottomRight)
            ])
        businessCard = image.cropping(to: businessCard.extent)
        
        return perspectiveCorrection(businessCard, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
    }
    
    open  func perspectiveCorrection(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        
        return image.applyingFilter("CIPerspectiveCorrection", withInputParameters: [
            
            "inputTopLeft": CIVector(cgPoint: topLeft),
            "inputTopRight": CIVector(cgPoint: topRight),
            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
            "inputBottomRight": CIVector(cgPoint: bottomRight)
            
            ])
        
    }

    func appliFiltersToImage(string: String) {
        let image = UIImage(named: string)!

        let ciImage = CIImage(cgImage: image.cgImage!)
        createDirectory(named: "filters")
        save(image: image, inDirectory: "filters", named: string)

        if let imageS = SmoothThresholdFilter(ciImage, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage {
            let i = preventCGImageNil(image: UIImage(ciImage: imageS))

            save(image: i, inDirectory: "filters", named: "\(string)-SmoothThresholdFilter")
//            extractFromImage(croppedImage: i)
        }



        
        if #available(iOS 11.0, *) {

            /**
             El que mejor a dado resultados hasta ahora
             */
            let imgBlueCI = SmoothThresholdFilter(CIImage(image: UIImage(named: string)!)!, inputEdgeO: 0.15, inputEdge1: 0.9)
                .outputImage!.applyingFilter("CIGloom")
                .applyingFilter("CIUnsharpMask")
                .applyingFilter("CIMedianFilter")
                //.applyingFilter("CIBoxBlur", withInputParameters: [kCIInputRadiusKey : NSNumber(integerLiteral: 2) ])
            
            
            
            if let resized = resize(image: image, newSize: CGSize(width: 439, height: 283 )) {
                save(image: resized, inDirectory: "filters", named: "\(1 == 1)-0.5x")
            }
            
        
            let uiImage = UIImage(ciImage: imgBlueCI)
//             extractFromImage(croppedImage: uiImage,directory: "palabras-caracteres")
            
            
            
            
            testtesseract(image: preventCGImageNil(image: uiImage) )
            
            save(image: uiImage, inDirectory: "filters", named: "CIGloom+CIUnsharpMask+CIMedianFilter-ok")
            
      
            
           
            
            
            
            
            

        }




//        if let a = SmoothThresholdFilter(ciImage, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage, let b = SmoothThresholdFilter(a, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage  {
//            let i = preventCGImageNil(image: UIImage(ciImage: b))
//            save(image: i, inDirectory: "filters", named: "\(string)-SmoothThresholdFilter2Veces")
//        }
//

//        if let imageS = AdaptiveThresholdFilter(ciImage).outputImage {
//            let i = preventCGImageNil(image: UIImage(ciImage: imageS))
//
//            save(image: i, inDirectory: "filters", named: "\(string)-AdaptiveThresholdFilter")
//        }
//
//        if let a = SmoothThresholdFilter(ciImage, inputEdgeO: 0.15, inputEdge1: 0.9).outputImage, let b = AdaptiveThresholdFilter(a).outputImage {
//            let i = preventCGImageNil(image: UIImage(ciImage: b))
//            save(image: i, inDirectory: "filters", named: "\(string)-SmoothThresholdFilter+AdaptiveThresholdFilter")
//
//        }
    }
    
    
    
    
    
    
    fileprivate let textDetector = CIDetector(
        ofType: CIDetectorTypeText,
        context: nil,
        options: [
            CIDetectorAccuracy: CIDetectorAccuracyHigh,
            CIDetectorAspectRatio: 1.0,
            CIDetectorReturnSubFeatures: true
        ]
    )

    open func performDetection(_ image: CIImage, detector: CIDetector?, expand: CGFloat = 0) -> UIImage? {
        
        var overlayImage: CIImage = image
        
        if let detector = detector {
           let features = detector.features(in: image)
            
            print("features count \(features.count)")
            
            // rectangleFeature = (features as? [CIRectangleFeature]) ?? []
            
            for feature in (features as? [CITextFeature] ?? []) {
                
                
                // Se resaltan los puntos donde se encontro la caracteristica
                overlayImage = drawHighlightOverlayForPoints(overlayImage, topLeft: feature.topLeft.plusx(-expand).plusy(expand), topRight: feature.topRight.plusx(expand).plusy(expand),
                                                                         bottomLeft: feature.bottomLeft.plusx(-expand).plusy(-expand), bottomRight: feature.bottomRight.plusx(expand).plusy(-expand))
                
            }
        }
        
        print("overlayImage\(overlayImage)")

        
        return UIImage(ciImage: overlayImage)
 
    }



    private func applyFilterBlue(originalCI: CIImage) -> CIImage {

        // return image

        /// Se le aplica una capa de azul encima de la imagen  ya que esto resalta el color negro en escala blanco y negro
        let color: CIColor = CIColor(red: 0.0, green: 0, blue: 1, alpha: 0.2)
        let originFixed = UIImage(ciImage: originalCI)

        let originOverlay = drawHighlightOverlayForPoints(
            originalCI,
            topLeft: CGPoint(x: 0, y: originFixed.size.height),
            topRight: CGPoint(x: originFixed.size.width * (1), y: originFixed.size.height),
            bottomLeft: CGPoint(x: 0, y: 0),
            bottomRight: CGPoint(x: originFixed.size.width * (1), y: 0),
            color: color)

        // return UIImage(CGImage: ImageStudio.convertCIImageToCGImage(originOverlay)).g8_blackAndWhite()

        // let bri = GPUImageBrightnessFilter()
        //  bri.brightness = -0.2
        //
        //  return bri.imageByFilteringImage(image).g8_blackAndWhite()
        return originOverlay
        // .g8_blackAndWhite()

        // return image
    }





    @IBAction func saveAction(_ sender: Any) {

        if true {
            
            testtesseract(image: UIImage(named: "IMG_1549")!)
//                createImageForAllFonts()
            
             appliFiltersToImage(string: "tessinput_2")
//            appliFiltersToImage(string: "tessinput_2")
//            appliFiltersToImage(string: "IMG_7724")
//            extractFromImage(croppedImage: UIImage(named: "IMG_1549")!,directory: "palabras-caracteres")
                return
        }

        let alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
//        let alpha = "asS";
        totalImages = 0

        for char in alpha {
            let name = String(char)
            //deleteDirectory(named: name.lowercased())
            createDirectory(named: name.lowercased())
            print("Image from \(char)")
            createImagen(from: name)
            print("totalImages \(totalImages)")


        }

        print("totalImages \(totalImages)")


    }





    // CIHeightFieldFromMask
    open func applyNoir(_ image: CIImage, filter: String = "CIPhotoEffectNoir") -> CIImage {

        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: filter)
        currentFilter!.setValue(image, forKey: kCIInputImageKey)
        let output = currentFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        return CIImage(cgImage: cgimg!)

    }

    func resize(image: UIImage, newSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    @IBAction func showPrescriptionCapture(_ sender: Any) {

        let ctrl = PrescriptionCaptureV2ViewController()
        show(ctrl, sender: self)
        
    }
    
}




