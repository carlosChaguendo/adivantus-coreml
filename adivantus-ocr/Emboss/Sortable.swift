//
//  Sortable.swift
//  AdivantusCore
//
//  Created by Carlos Chaguendo on 20/12/17.
//  Copyright © 2017 Mayorgafirm. All rights reserved.
//

import UIKit

/// Necesario para  usop del oerador **is**
///
///      //  error: protocol 'Sortable' can only be used
///      //  as a generic constraint because it has Self or associated type requirements
///      if Resident.self is Sortable.Type {
///
///      }
///
///      if Resident.self is AnySortable.Type {
///
///      }
public protocol AnySortable {

}

///
/// **Importante** Es necesario tener centralizada la propiedad con la cual esta ordenada cada entidad para las busquedas locales,
/// ya que se presentaria le error de mostra en diferente orden cuando se consulta del servidor y cuando se consulta de la base de datos
///
public protocol Sortable: AnySortable {
    
    associatedtype Sort: Comparable

    var sortedValue: Sort { get }

}

public extension Sequence where Iterator.Element: Sortable {
    
    /// valores segun `sortedValue`
    public var sorted: [Iterator.Element] {
        return self.sorted(by: { $0.sortedValue < $1.sortedValue })
    }
    
    /// valores segun `-sortedValue`
    public var sortedReversed: [Iterator.Element] {
        return self.sorted(by: { $0.sortedValue > $1.sortedValue })
    }
}


public extension Array where Element: Sortable {
    
    /// ordena los valores segun `sortedValue`
    public mutating func sort() {
        self = self.sorted
    }
    
    /// ordena los valores segun `-sortedValue`
    public mutating func sortReversed() {
        self = self.sortedReversed
    }

    
}
