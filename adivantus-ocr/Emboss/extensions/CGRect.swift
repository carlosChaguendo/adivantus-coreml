//
//  CGRect.swift
//  Emboss
//
//  Created by Carlos Chaguendo on 11/14/18.
//  Copyright © 2018 Jayway. All rights reserved.
//

import UIKit

extension CGRect {

    func scale(_ from: CGSize, to: CGSize) -> CGRect {

        ///
        /// Re-maps a number from one range to another. That is, a value of fromLow would get mapped to toLow,
        /// a value of fromHigh to toHigh, values in-between to values in-between, etc.
        ///
        func mapValue(_ x: CGFloat, in_min: CGFloat = 0, in_max: CGFloat, out_min: CGFloat = 0, out_max: CGFloat) -> CGFloat {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }

        let x = mapValue(self.origin.x, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let y = mapValue(self.origin.y, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)
        let w = mapValue(self.width, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let h = mapValue(self.height, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)

        return CGRect(x: x, y: y, width: w, height: h)

    }
    
    /// A bounding box is defined by four values (x, y, width, height) where (0, 0) is the top left corner. The center of the box is located at (x, y):
    var coordinates: [String: CGFloat] {
        return [
            "height": self.height ,
            "width": self.width ,
            "x": self.midX,
            "y": self.midY
        ]
    }
}
