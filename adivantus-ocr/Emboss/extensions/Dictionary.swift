//
//  Dictionary.swift
//  Emboss
//
//  Created by Carlos Chaguendo on 11/14/18.
//  Copyright © 2018 Jayway. All rights reserved.
//

import UIKit

extension Sequence {
    
    public var JSON: NSString {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions(rawValue: 0))
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            return json!
        } catch {
            return "Invalid JSON"
        }
    }
    
}
