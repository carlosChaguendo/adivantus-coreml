//
//  ObjectDetectionExporterViewController.swift
//  Emboss
//
//  Created by Carlos Chaguendo on 11/14/18.
//  Copyright © 2018 Jayway. All rights reserved.
//

import UIKit
import PromiseKit

public class ObjectDetectionExporterViewController: UIViewController {


    public let folder = "ocr-images"
    public var domain = FileManager.SearchPathDirectory.documentDirectory
    public var images: [UIImage] = []

    private var pathURL: URL!

    private let sizeForRose = CGSize(width: 641, height: 390)
    private let rectForRose = CGRect(x: 0, y: 0, width: 641, height: 100)

    public override func viewDidLoad() {
        super.viewDidLoad()


        let location = (NSSearchPathForDirectoriesInDomains(self.domain, .userDomainMask, true)[0] as NSString) as String
        self.pathURL = URL(fileURLWithPath: location).appendingPathComponent(folder)
        
        
        if let pythonURL = Bundle.main.resourceURL?.appendingPathComponent("python") {
            print(pythonURL.path)
        }
     
        

    }

    @IBAction func generateImages(_ sender: Any) {

        loadImagesIn(pathURL).then { (images) -> Void in

            var annotations: [[String: Any]] = []
            var imagesURLS: [String] = []

            CustomPhotoAlbum.createDirectory(named: "modificated")
            
            print("")
            
            for image in images {

                let rect = self.rectForRose.scale(self.sizeForRose, to: image.size)
                annotations.append([
                    "image-size": "\(image.size.width)*\(image.size.height)",
                    "image": image.accessibilityIdentifier ?? "n/a",
                    "label": "rose-pharmacy",
                    "coordinates": rect.coordinates
                ])
                
                let imgName = (image.accessibilityIdentifier ?? "n/a").lowercased().replacingOccurrences(of: ".jpg", with: "")
             
                if  let orient = CustomPhotoAlbum.fixImageOrientation(image), let mod = UIImageJPEGRepresentation(orient, 0.25) {
                    CustomPhotoAlbum.save(data: mod, inDirectory: "modificated", named: imgName)
                }
                
                
                print("tc.Image('images/\(imgName).jpg'),")
                //imagesURLS.append("tc.Image('../images/\(image.accessibilityHint!.lowercased()).jpg')" )

            }
            
            
            print(images.last?.accessibilityHint)
            annotations.JSON
            

        }
        
        
        
        
        
        
    }




    fileprivate func loadImagesIn(_ url: URL) -> Promise<[UIImage]> {
        return Promise<[UIImage]> { (resolve, reject) -> Void in
            do {
                images.removeAll()
                let urls = try FileManager.default.contentsOfDirectory(at: pathURL, includingPropertiesForKeys: [.isDirectoryKey, .isRegularFileKey], options: .skipsHiddenFiles)
                for url in urls {
                    if let image = UIImage(contentsOfFile: url.path), let fixed = CustomPhotoAlbum.fixImageOrientation(image) {
                        fixed.accessibilityIdentifier = url.lastPathComponent
                        fixed.accessibilityHint = url.path
                        images.append(fixed)
                    }
                }
                resolve(images)
            } catch {
                reject(error)
            }
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
