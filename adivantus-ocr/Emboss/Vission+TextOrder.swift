//
//  Vission+TextOrder.swift
//  AdivantusIphone
//
//  Created by Carlos Chaguendo on 10/2/18.
//  Copyright © 2018 Mayorgafirm. All rights reserved.
//

import UIKit
import Firebase


protocol SortableByFrame: Sortable {
    var frame: CGRect { get }
}

extension SortableByFrame {
    
    public var sortedValue: CGFloat {
        return frame.origin.y
    }
    
}

extension VisionTextBlock: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionTextLine: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionTextElement: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionText {
    
    /// EL texto de la imagen en orden de arriba hacia abajo
    public var textInOrder: String {
        let result = NSMutableString()
        for box in blocks.sorted {
                  print("\t \(box.text)")
            for line in box.lines.sorted {
                result.append(line.text)
                result.append("\n")
                print("\t\t \(line.text)")
                for a in line.elements {
                    print("\t\t\t \(a.text)")
                }
            }
        }
        print("+++++++++++++++")
        return result.description
    }
    
    public var bloksInOrder: String {
        let result = NSMutableString()
        for box in blocks.sorted {
            print("\t \(box.text)")
            
            result.append(box.text)
            result.append("\n")
            
 
        }
        print("+++++++++++++++")
        return result.description
    }
    
}


