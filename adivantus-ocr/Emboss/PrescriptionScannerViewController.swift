//
//  PrescriptionScannerViewController.swift
//  Emboss
//
//  Created by Carlos Chaguendo on 17/07/18.
//  Copyright © 2018 Jayway. All rights reserved.
//

import UIKit
import Vision
import CoreImage
import AdivantusOCR
import TesseractOCR
import Firebase


class PrescriptionScannerViewController: UIViewController {

    lazy var recognizer: VisionTextRecognizer =  Vision.vision().onDeviceTextRecognizer()

    fileprivate var textos = [String: String]()

    fileprivate let textDetector = CIDetector(ofType: CIDetectorTypeText, context: nil, options: [
        CIDetectorAccuracy: CIDetectorAccuracyHigh,
        CIDetectorAspectRatio: 1.0,
        CIDetectorReturnSubFeatures: true
    ]
    )


    /**
     Instancia de teseract con la configuracion mas optima
     
     * [Disabling the dictionaries](https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality#dictionaries-word-lists-and-patterns)
     * [Page Segmentation Mode](https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality#page-segmentation-method)
     
     */
    lazy var tesseract: G8Tesseract = {
        guard let tesseract = G8Tesseract(
            language: "eng",
            configDictionary: [:],
            configFileNames: ["user-words", "characters"],
            cachesRelatedDataPath: "adivantus-ocr/tmp-cache",
            // Opcion de escaneo mas precisa pero lenta
            engineMode: .tesseractCubeCombined
        ) else {
            preconditionFailure()
        }
        tesseract.pageSegmentationMode = .singleBlock
        tesseract.maximumRecognitionTime = 60.0
        tesseract.delegate = self
        return tesseract
    }()




    private let album = CustomPhotoAlbum()
    fileprivate var directory = "current-scaner"


    override func viewDidLoad() {
        super.viewDidLoad()

        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)

        print(path)
    }


    @IBAction func openCameraAction(_ sender: UIBarButtonItem) {
        
        
        let image = UIImage(named: "B")!



        executeTesseract(for: image, name: "B")



    }
    
    
    func executeTesseract(for image: UIImage, name: String) -> String {
        let ciImage = CIImage(image: image)!
        
        CustomPhotoAlbum.save(image: image, inDirectory: directory, named: "original")
        
        //        CustomPhotoAlbum.deleteDirectory(named: directory)
        //        CustomPhotoAlbum.createDirectory(named: directory)
        
        let img = rotateImageToHorizontalLines(ciImage: ciImage)
        CustomPhotoAlbum.save(image: img, inDirectory: directory, named: "deskew")
        
        let textOnly = removeBorders(ciImage: CIImage(image: img)!)
        //
        CustomPhotoAlbum.save(image: textOnly, inDirectory: directory, named: "textOnly")
        
        
        let text = extractText(of: textOnly, name: name)
        //
        CustomPhotoAlbum.save(image: img, inDirectory: directory, named: "ciImageWithFilters+rotated")
        CustomPhotoAlbum.save(image: UIImage(ciImage: ciImage), inDirectory: directory, named: "ciImageWithFilters")
        //
        //
        //
        //
        print("text\n ##########################\n \(text) \n ##########################\n")
        
        return text
    }

    @IBAction func allFolderAction(_ sender: Any) {
        //processAllfolder()
        cropTextForemAllImages()
    }
    
    @IBAction func openCameraCapture(_ sender: Any) {
        let ctrl =  PrescriptionCaptureV2ViewController()
        show(ctrl, sender: self)
    }
    
    func cropTextForemAllImages() {
        var minions = [String]()
        let fm = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
        
        let items = try! fm.contentsOfDirectory(atPath: "\(path)/rx")
        minions = items.filter { $0.hasSuffix("jpg") || $0.hasSuffix("JPG") }
        print(minions)
        
        directory = "croped"
        CustomPhotoAlbum.createDirectory(named: directory)
        

        for url in minions {
  
            let u = URL(fileURLWithPath: url)
            
            let name  = u.deletingPathExtension().lastPathComponent
   
            
            let imgUrl = "\(path)/rx/\(url)"
            if let image = UIImage(contentsOfFile: imgUrl), let fixed = CustomPhotoAlbum.fixImageOrientation(image){
                
                let rpotated = rotateImageToHorizontalLines(ciImage: CIImage(image: fixed)!)
                
                let croped = removeBorders(ciImage: CIImage(image: rpotated)!)
                
                directory = ""
                CustomPhotoAlbum.save(image: croped, inDirectory: directory, named: name)

                
                
                
            }else {
                print("No existe \(url)")
            }
            
        }

    }

    func processAllfolder() {

        var minions = [String]()
        let fm = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)


        let items = try! fm.contentsOfDirectory(atPath: "\(path)/rx")
        minions = items.filter { $0.hasSuffix("jpg") || $0.hasSuffix("JPG") }
        print(minions)
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: textos, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            CustomPhotoAlbum.save(data: jsonData, inDirectory: "rx", named: "values")
            
            
        } catch {
            print(error.localizedDescription)
        }
        
  

        for url in minions {

            let u = URL(fileURLWithPath: url)

            directory = u.deletingPathExtension().lastPathComponent
            CustomPhotoAlbum.createDirectory(named: directory)
            
            let imgUrl = "\(path)/rx/\(url)"
            if let image = UIImage(contentsOfFile: imgUrl), let fixed = CustomPhotoAlbum.fixImageOrientation(image){

                let text = executeTesseract(for: fixed, name: directory)
                
                
        
                
                
                
            }else {
                print("No existe \(url)")
            }

        }
        
        


    }


    /**
     El que mejor a dado resultados hasta ahora
     */
    func appliFilters(to ciImage: CIImage) -> CIImage {
        if #available(iOS 11.0, *) {
            let i = SmoothThresholdFilter(ciImage).outputImage ?? ciImage
                .applyingFilter("CIMedianFilter")
                .applyingFilter("CIBoxBlur", withInputParameters: [kCIInputRadiusKey: NSNumber(integerLiteral: 2)])
                .applyingFilter("CIGloom")
                .applyingFilter("CIMedianFilter")
                .applyingFilter("CIPhotoEffectMono")
            return i


        } else {
            preconditionFailure()
        }
    }


    /**
     Una imagen sesgada es cuando una página ha sido escaneada cuando no está recta. La calidad de la segmentación de línea de Tesseract
     se reduce significativamente si una página está demasiado sesgada, lo que tiene un impacto severo en la calidad del OCR.
     Para ello, gire la imagen de la página de modo que las líneas de texto sean horizontales.
     */
    private func rotateImageToHorizontalLines(ciImage: CIImage) -> UIImage {

        /// Se calcula el angulo mas commun en los textos de la imagen
        /// para rotar la imagen
        let feactures = textDetector!.features(in: ciImage, type: CITextFeature.self)
        let deskewAngles = feactures.map {
            // Calculo del angulo entre 2 puntos del plano
            // las cordendas de CoreImage son iguales al plano cartesiano
            // e inversas al sitema de cordenadas de UIkit
            atan2($0.topRight.y - $0.topLeft.y, $0.topRight.x - $0.topLeft.x).rounded(toPlaces: 5)
        }

        let contBysort = deskewAngles
            .filter({ $0 != 0.0 })
            .countBy { $0 }
        // se combierte a tupla para garantizar el orden de recorridos
        .map({ (angle: $0, count: $1) })
        // Se ordena el conteo de mayor a menor para el caso que existan varios angulos con el mismo peso
        // se toma el mayor angulo importante el abs() para angulos negativos el maxor angulo es el menor
        .sorted(by: { (a, b) -> Bool in
            return abs(a.angle) > abs(b.angle)
        })


        let cgimage = CustomPhotoAlbum.preventCGImageNil(image: UIImage(ciImage: ciImage));
        if contBysort.count < 5 {
            print("Insuficientes caracteristicas  para girar feactures: \(feactures.count) \ndeskewAngles:\(deskewAngles) \n\(contBysort)")
            return cgimage
        }

        /// se busca el angulo mas comun - con mayor  numero de conteo
        let maxAlfa: (CGFloat, Int) = contBysort.reduce((0, 0)) { $1.count > $0.1 ? $1: $0 }

        if maxAlfa.0 == 0.0 {
            return cgimage
        }

        print("Angulo de rotacion -\(maxAlfa.0)")
        return cgimage.image(withRotation: -maxAlfa.0)
    }

    /**
     Las páginas escaneadas a menudo tienen bordes oscuros alrededor de ellas.
     Estos pueden ser tomados erróneamente como caracteres extra, especialmente si varían en forma y gradación.
     */
    private func removeBorders(ciImage: CIImage) -> UIImage {
        let feactures = textDetector!.features(in: ciImage, type: CITextFeature.self)

        var topLeft = CGPoint.zero
        var bottomRight = CGPoint.zero

        let borderExpand: CGFloat = 10

        topLeft.x = feactures.map { $0.topLeft.x }.min()! - borderExpand
        topLeft.y = feactures.map { $0.topLeft.y }.max()! + borderExpand

        bottomRight.x = feactures.map { $0.bottomRight.x }.max()! + borderExpand
        bottomRight.y = feactures.map { $0.bottomRight.y }.min()! - borderExpand


        let topRight = CGPoint(x: bottomRight.x, y: topLeft.y)
        let bottomLeft = CGPoint(x: topLeft.x, y: bottomRight.y)

        // let overlay = drawHighlightOverlayForPoints(ciImage, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight, color: CIColor(red:1, green: 0, blue: 0, alpha: 0.4))
        let croped = cropOverlayForPoints(ciImage, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
        let cgimage = CustomPhotoAlbum.preventCGImageNil(image: UIImage(ciImage: croped));
        return cgimage
    }



    open func drawHighlightOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint,
                                            bottomLeft: CGPoint, bottomRight: CGPoint, color: CIColor = CIColor(red: 0.0, green: 0, blue: 1, alpha: 0.6)) -> CIImage {

        var overlay = CIImage(color: color)
        overlay = overlay.cropping(to: image.extent)
        overlay = overlay.applyingFilter("CIPerspectiveTransformWithExtent",
                                         withInputParameters: [
                                             "inputExtent": CIVector(cgRect: image.extent),
                                             "inputTopLeft": CIVector(cgPoint: topLeft),
                                             "inputTopRight": CIVector(cgPoint: topRight),
                                             "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                             "inputBottomRight": CIVector(cgPoint: bottomRight)
                                         ])

        return overlay.compositingOverImage(image)
    }


    open func cropOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {

        var businessCard: CIImage
        businessCard = image.applyingFilter("CIPerspectiveTransformWithExtent",
                                            withInputParameters: [
                                                "inputExtent": CIVector(cgRect: image.extent),
                                                "inputTopLeft": CIVector(cgPoint: topLeft),
                                                "inputTopRight": CIVector(cgPoint: topRight),
                                                "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                                "inputBottomRight": CIVector(cgPoint: bottomRight)
                                            ])
        businessCard = image.cropping(to: businessCard.extent)

        return perspectiveCorrection(businessCard, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
    }

    open func perspectiveCorrection(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {

        return image.applyingFilter("CIPerspectiveCorrection", withInputParameters: [

            "inputTopLeft": CIVector(cgPoint: topLeft),
            "inputTopRight": CIVector(cgPoint: topRight),
            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
            "inputBottomRight": CIVector(cgPoint: bottomRight)

        ])

    }

    private func extractText(of image: UIImage, name: String, withMinimunConfidence: CGFloat = 40) -> String {
        
        
        let vissionImage = VisionImage(image: image)
        
        recognizer.process(vissionImage) { (result, error) in
            
            
            if let text = result?.textInOrder {
                self.textos[name] = text.replacingOccurrences(of: "\n", with: " ")
                
                print("\(name):::::  \(text)")
            }
    
        
            
        }

        return "Larr"
    }



}





extension PrescriptionScannerViewController: G8TesseractDelegate {

    /**
     En las pruebas realizadas e mejor no realizar filtros
     */
    func preprocessedImage(for tesseract: G8Tesseract!, sourceImage: UIImage!) -> UIImage! {
        print("preprocessedImage")
        
        return sourceImage
        
        let ciImage = CIImage(image: sourceImage)!
        
        if true {
            
            /// funciona mejor para imagenes claras
            let out = ciImage.applyingFilter("CIColorMonochrome", withInputParameters: [kCIInputColorKey: CIColor(red: 0.0, green: 0.0, blue: 0.75, alpha: 0.6)])
            let withFilters = Configuration.default.process(inputImage2: out)
            let img = CustomPhotoAlbum.preventCGImageNil(image: UIImage(ciImage: withFilters))
            CustomPhotoAlbum.save(image: img, inDirectory: directory, named: "with filters")
            // Just be sure source image is valid
            print("preprocessedImage ok")
            return img
            
        }else {

            let withFilters = appliFilters(to: ciImage)
            let img = CustomPhotoAlbum.preventCGImageNil(image: UIImage(ciImage: withFilters))
              CustomPhotoAlbum.save(image: img, inDirectory: directory, named: "with filters")
            // Just be sure source image is valid
            print("preprocessedImage ok")
            return img
        }

      


  
    }

}


