//
//  RxInfoScene.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/30/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit



extension SKScene  {
    
    public func childNode<T: SKNode>(withName: String) -> T? {
        return childNode(withName: withName) as? T
    }
    
    
}

public class RxInfoScene: SKScene {
    
    
    public private(set) var nameLabel: SKLabelNode!
    
    public private(set) var image: SKSpriteNode!
    
    
    
    private func load() {
        nameLabel = childNode(withName: "residentName")
        image = childNode(withName: "photo")
    }
    
    
     func roundSquareImage(imageName: String) -> UIImage! {
        let originalPicture = UIImage(named: imageName)
        // create the image with rounded corners
        UIGraphicsBeginImageContextWithOptions(originalPicture!.size, false, 0)
        let rect = CGRect(x:0, y:0, width: originalPicture!.size.width, height: originalPicture!.size.height)
        let rectPath : UIBezierPath = UIBezierPath(roundedRect: rect, cornerRadius: originalPicture!.size.width / 2)
        rectPath.addClip()
        originalPicture!.draw(in:rect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return scaledImage ?? originalPicture
        
        
//        let texture = SKTexture(image: scaledImage!)
//        let roundedImage = SKSpriteNode(texture: texture, size: CGSize(width: originalPicture!.size.width, height: originalPicture!.size.height))
//        roundedImage.name = imageName
//        return roundedImage
    }
    
    
    public func setImage(named: String) {
        image.texture = SKTexture(image: roundSquareImage(imageName: named))
    }
    
    
    
    public class func instance() -> RxInfoScene {
        let result = RxInfoScene(fileNamed: "ProductInfo")!
        result.load()
        return result
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Enddd")
        
    }


}
