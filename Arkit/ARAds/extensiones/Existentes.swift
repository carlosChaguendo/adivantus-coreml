//
//  Existentes.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/27/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Vision
import VideoToolbox
import Firebase
import CoreImage
import AVFoundation


public protocol AnySortable {
    
}

public protocol Sortable: AnySortable {
    
    associatedtype Sort: Comparable
    
    var sortedValue: Sort { get }
    
}


public extension Sequence where Iterator.Element: Sortable {
    
    /// valores segun `sortedValue`
    public var sorted: [Iterator.Element] {
        return self.sorted(by: { $0.sortedValue < $1.sortedValue })
    }
    
    /// valores segun `-sortedValue`
    public var sortedReversed: [Iterator.Element] {
        return self.sorted(by: { $0.sortedValue > $1.sortedValue })
    }
}


public extension Array where Element: Sortable {
    
    /// ordena los valores segun `sortedValue`
    public mutating func sort() {
        self = self.sorted
    }
    
    /// ordena los valores segun `-sortedValue`
    public mutating func sortReversed() {
        self = self.sortedReversed
    }
    
    
}



protocol SortableByFrame: Sortable {
    var frame: CGRect { get }
}

extension SortableByFrame {
    
    public var sortedValue: CGFloat {
        return frame.origin.y
    }
    
}

extension VisionTextBlock: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionTextLine: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionTextElement: SortableByFrame {
    public typealias Sort = CGFloat
}

extension VisionText {
    
    /// EL texto de la imagen en orden de arriba hacia abajo
    public var textInOrder: String {
        let result = NSMutableString()
        for box in blocks.sorted {
            //                  print("\t \(box.text)")
            for line in box.lines.sorted {
                result.append(line.text)
                result.append("\n")
                //                print("\t\t \(line.text)")
                //                for a in line.elements {
                ////                    print("\t\t\t \(a.text)")
                //                }
            }
        }
        //        print("+++++++++++++++")
        return result.description
    }
    
}



public extension SCNVector3 {
    public func distance(from vector: SCNVector3) -> CGFloat {
        let deltaX = self.x - vector.x
        let deltaY = self.y - vector.y
        let deltaZ = self.z - vector.z
        
        return CGFloat(sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ))
    }
    
    static func of(transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }
    
    
  
}

public extension CGRect {
    
    public func scale(_ from: CGSize, to: CGSize) -> CGRect {
        
        ///
        /// Re-maps a number from one range to another. That is, a value of fromLow would get mapped to toLow,
        /// a value of fromHigh to toHigh, values in-between to values in-between, etc.
        ///
        func mapValue(_ x: CGFloat, in_min: CGFloat = 0, in_max: CGFloat, out_min: CGFloat = 0, out_max: CGFloat) -> CGFloat {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
        
        let x = mapValue(self.origin.x, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let y = mapValue(self.origin.y, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)
        let w = mapValue(self.width, in_min: 0, in_max: from.width, out_min: 0, out_max: to.width)
        let h = mapValue(self.height, in_min: 0, in_max: from.height, out_min: 0, out_max: to.height)
        
        return CGRect(x: x, y: y, width: w, height: h)
        
    }
    
}


public extension String {
    
    public func trim() -> String {
        return trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}

public extension Array {
    
    public var nonEmpty:Bool{
        return !isEmpty
    }
    
    public subscript (safe index: Int) -> Element? {
        return index >= 0 && index < count ? self[index] : nil
    }
    
    /// Removes all elements from an array that the callback returns true.
    ///
    /// - Returns: Array with removed elements.
    @discardableResult
    public mutating func remove( callback: (Iterator.Element) -> Bool) -> [Iterator.Element] {
        
        var index = 0
        var removed: [Iterator.Element] = []
        
        for el in self {
            
            if callback(el) == true {
                let rm = self[index]
                removed.append(rm)
                self.remove(at: index)
                index -= 1
            }
            index += 1
        }
        
        
        
        
        return removed
    }
}


public extension CGPoint {
    
    
    public func mapValue(_ x: CGFloat, in_min: CGFloat = 0, in_max: CGFloat, out_min: CGFloat = 0, out_max: CGFloat) -> CGFloat {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
    
    
   public func scale(_ from: CGSize, to: CGSize) -> CGPoint {
        let x = mapValue(self.x, in_max: from.width, out_max: to.width)
        let y = mapValue(self.y, in_max: from.height, out_max: to.height)
        return CGPoint(x: x, y: y)
        
    }
    
   public func coreImagesSpace(height: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: height - y)
    }
    
}


public extension UIImage {
    /**
     Creates a new UIImage from a CVPixelBuffer.
     NOTE: This only works for RGB pixel buffers, not for grayscale.
     */
    public convenience init?(pixelBuffer: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(pixelBuffer, nil, &cgImage)
        
        if let cgImage = cgImage {
            self.init(cgImage: cgImage, scale: 1, orientation: .up)
        } else {
            return nil
        }
    }
}



public extension UICollectionView {
    
    public func registerWithClass(_ cell: UICollectionViewCell.Type) {
        let className = String(describing: cell)
        register(cell, forCellWithReuseIdentifier: className)
    }
    
    public func registerNibWithClass(_ cell: UICollectionViewCell.Type) {
        let className = String(describing: cell)
        register(UINib(nibName: className, bundle: nil), forCellWithReuseIdentifier: className)
    }
    
    public func dequeueReusableCellWithClass<T>(_ className: String, forIndexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: className, for: forIndexPath) as? T
    }
    
    public func dequeueReusableCellWithClass2<T: UICollectionViewCell>(_ cell: T.Type, forIndexPath: IndexPath) -> T? {
        let className = String(describing: cell)
        return dequeueReusableCell(withReuseIdentifier: className, for: forIndexPath) as? T
    }
    
}

public extension UICollectionViewController {
    
    public func dequeue<T: UICollectionViewCell>( cell: T.Type, forIndexPath: IndexPath) -> T? {
        return collectionView?.dequeueReusableCellWithClass2(cell, forIndexPath: forIndexPath)
    }
    
}

