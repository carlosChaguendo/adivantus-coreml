//
//  RxField.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/30/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit

struct  RxField {

    let name: String!
    let frame: CGRect!
    
    
    func scale(from: CGSize, to: CGSize) -> RxField {
        
        return RxField(name: name, frame: frame.scale(from, to: to))
    }
 

}


struct RxRose {
    
    static let fields: [RxField] = [
        RxField(name: "Resident Name", frame:  CGRect(x: 0, y: 75, width: 220, height: 35)),
        RxField(name: "Medication Name", frame: CGRect(x: 0, y: 90, width: 318, height: 40)),
        RxField(name: "Name Generic", frame:  CGRect(x: 65, y: 110, width: 130, height: 30)),
        RxField(name: "Dosage", frame: CGRect(x: 0, y: 125, width: 320, height: 70)),
        RxField(name: "Ph Name", frame: CGRect(x: 145, y: 50, width: 175, height: 40)),
        RxField(name: "RX Number", frame: CGRect(x: 30, y: 50, width: 80, height: 40)),
        RxField(name: "Fill Date", frame: CGRect(x: 200, y: 70, width: 95, height: 40)),
        RxField(name: "Refill", frame: CGRect(x: 0, y: 195, width: 100, height: 45)),
        RxField(name: "Exp Date", frame: CGRect(x: 230, y: 195, width: 90, height: 45)),
        RxField(name: "Qty", frame: CGRect(x: 180, y: 108, width: 140, height: 45))
    ]
    
    static let baseSize = CGSize(width: 320, height: 240)
    
    static func scale(to: CGSize) -> [RxField] {
        return RxRose.fields.map{ $0.scale(from: RxRose.baseSize, to: to) }
    }
    
    
}
