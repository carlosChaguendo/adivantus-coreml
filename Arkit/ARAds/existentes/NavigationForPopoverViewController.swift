//
//  NavigationForPopoverViewController.swift
//  AdivantusIphone
//
//  Created by Carlos Chaguendo on 12/03/18.
//  Copyright © 2018 Mayorgafirm. All rights reserved.
//

import UIKit

class NavigationForPopoverViewController: UINavigationController, UIPopoverPresentationControllerDelegate {
    
    var iphonePressentationStyle = UIModalPresentationStyle.formSheet
    var ipadPressentationStyle = UIModalPresentationStyle.none
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        initialize()
    }
    
    
    convenience init(rootViewController: UIViewController, sourceView: UIView?, sourceRect: CGRect?) {
        self.init(rootViewController: rootViewController)
        
        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = sourceRect ?? CGRect.zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initialize()
    }
    
    
    private func initialize(){
        hidesBarsOnSwipe = false
        
        modalPresentationStyle = UIModalPresentationStyle.popover
        popoverPresentationController?.backgroundColor = .white
        popoverPresentationController?.permittedArrowDirections = .any
        popoverPresentationController?.delegate = self
        
        /// iPhone Xs Max
        if UIScreen.main.nativeBounds.height == 2688 {
            preferredContentSize = UIScreen.main.bounds.size
            //popoverPresentationController?.popoverBackgroundViewClass = PopoverBackgroundWithoutArrowView.self
        }
    }
    
    /**
     Forza el modo popopver en el iphonee
     */
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        if traitCollection.userInterfaceIdiom == .phone {
            
            /// iPhone Xs Max
            if UIScreen.main.nativeBounds.height == 2688 {
                return ipadPressentationStyle
            }
            
            return iphonePressentationStyle
        }
        return ipadPressentationStyle
    }
    
}

