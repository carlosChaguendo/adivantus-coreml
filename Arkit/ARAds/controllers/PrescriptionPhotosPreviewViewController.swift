//
//  PrescriptionPhotosPreviewViewController.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/28/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import NYTPhotoViewer
import Then

class PrescriptionPhotosPreviewViewController: PhotosViewController {
    
    
    
    
    lazy var toolbar = UIToolbar().then {
        let size = UIScreen.main.bounds.size
        let y = size.height - 45
        $0.tintColor = UIColor.white
        $0.barTintColor = UIColor.black.withAlphaComponent(0.5)
        $0.frame = CGRect(x: 0  , y: y, width: size.width, height: 45)
        
        let item = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteCurrentItem))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let edit = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(editCurrentItem))
        
        $0.setItems([item, space, edit], animated: true)
        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        overlayView?.addSubview(toolbar)
        
  
    }
    
    override func photosViewController(_ photosViewController: NYTPhotosViewController, didNavigateTo photo: NYTPhoto, at photoIndex: UInt) {
        super.photosViewController(photosViewController, didNavigateTo: photo, at: photoIndex)
        
        toolbar.removeFromSuperview()
        overlayView?.addSubview(toolbar)
    }
    
    
    @objc func deleteCurrentItem() {
        print("Eliminar")
    }
    
    @objc func editCurrentItem(_ sender: UIBarButtonItem) {
      print("edit current item")
        
        let vc = UIViewController()
        let nv = NavigationForPopoverViewController(rootViewController: vc)
        nv.popoverPresentationController?.barButtonItem = sender
        self.present(nv, animated: true, completion: nil)
        
    }

}
