//
//  CapturePhotosCollectionView.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/28/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import NYTPhotoViewer


public class CapturePhotosCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {



    public var vc: UIViewController?
    
    private var photos:[ImageData] = []
    
    
    init() {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let size = UIScreen.main.bounds.size
        let origin = CGPoint(x: 0, y: size.height - 70)
        let fr = CGRect(origin: origin, size: CGSize(width: size.width, height: 70))
        
        super.init(frame: fr, collectionViewLayout: flowLayout)
        
        
        registerNibWithClass(CapturePhotosCollectionViewCell.self)
        backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.2038079946)
        dataSource = self
        delegate = self
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    public func append(_ image: UIImage) {
        
        let data = ImageData(image: image)
        
        photos.append(data)
        self.insertItems(at: [IndexPath(row: photos.count - 1, section: 0)])
    
    }
    
    
    public func removeAll() {
        photos.removeAll()
        
        self.reloadData()
    }
    
    
    public func setText(text:String) {
        
        guard let photo = photos.last else {return}
        
        let titleAtributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         photo.attributedCaptionTitle = NSAttributedString(string: "\(text)\n\n", attributes: titleAtributes)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        guard  let cell = collectionView.dequeueReusableCellWithClass2(CapturePhotosCollectionViewCell.self, forIndexPath: indexPath)  else {
            preconditionFailure()
        }
        
        
        
        cell.imageView.image = photos[indexPath.row].image
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("aaaa")
        
        let ctrl = PrescriptionPhotosPreviewViewController(allPhotos: photos, initialIndex: indexPath.row)
  
        
        vc?.present(ctrl, animated: true, completion: nil)
    }
    
    public func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        print("xxxx")
        return true
    }
    
    
    
    
    


}
