//
//  ViewController.swift
//  ARAds
//
//  Created by Mohammad Azam on 3/26/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Vision
import VideoToolbox
import Firebase
import CoreImage
import AVFoundation

extension ARSCNView {
    
    
    open func node<Node: SCNNode>(for anchor: ARAnchor, as: Node.Type ) -> Node? {
        return self.node(for: anchor) as? Node
        
    }
    
}

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet fileprivate var sceneView: ARSCNView!
    fileprivate var photosCollectionView: CapturePhotosCollectionView!

    /// Todos las anchors agregados correspondientes a prescripciones captuaradas
    fileprivate var anchors: [PrescriptionAnchor] = []
    
    /// todos los anchors aggreados correspondientes a imagenes de rastreo
    fileprivate var imageAnchors: [ARImageAnchor] = []


    fileprivate var nodeForReference: PrescriptionReferenceNode?

    /// distancia minima en la que se permite capturar las prescripciones, esto evita la captura de la misma imagen varias veces
    /// tambien evita el traslape de la imagen de referencia y las prescriociones capturadas
    fileprivate var minimumDistanceBetweenCaptures: CGFloat = 0.05 // centimetros

    fileprivate let cutter = SceneCutter()

    private let IImil: ARReferenceImage = {
        let width: CGFloat = 0.1275
        let image = UIImage(named: "2mil")!.cgImage!
        return ARReferenceImage.init(image, orientation: .up, physicalWidth: width)
    }()

    private let rose: ARReferenceImage = {
        let width: CGFloat = 0.076499999999999999
        let image = UIImage(named: "Rose1")!.cgImage!
        return ARReferenceImage.init(image, orientation: .up, physicalWidth: width)
    }()

    private let imageTrackingConfiguration = ARImageTrackingConfiguration()

    private let worldTrackingConfiguration = ARWorldTrackingConfiguration()

    /// A serial queue for thread safety when modifying the SceneKit node graph.
    let updateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! +
        ".serialSceneKitQueue")
    
    
    var nextNewNode: RescriptionPlaneNode!

    //MARK: - Live
    
    override func loadView() {
        super.loadView()
    
        nextNewNode = RescriptionPlaneNode(width: 0.0765, height: 0.051)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        sceneView.delegate = self
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        sceneView.scene = SCNScene()


        rose.name = "rose-reference-image"
        IImil.name = "IImil-reference-image"

        imageTrackingConfiguration.trackingImages = [IImil, rose]

        worldTrackingConfiguration.detectionImages = [IImil, rose]
        worldTrackingConfiguration.planeDetection = []


        if photosCollectionView == nil {
            photosCollectionView = CapturePhotosCollectionView()
            photosCollectionView.vc = self
            view.addSubview(photosCollectionView)
        }


        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sceneViewDidTapAction(_:)))
        // Set the delegate to ensure this gesture is only used when there are no virtual objects in the scene.
        //        tapGesture.delegate = self
        sceneView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Run the view's session
        sceneView.session.run(worldTrackingConfiguration)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Pause the view's session
        sceneView.session.pause()
    }





    @IBAction func switchAction(_ sender: Any) {
        if sceneView.session.configuration == imageTrackingConfiguration {
            sceneView.session.run(worldTrackingConfiguration)
        } else {
            sceneView.session.run(imageTrackingConfiguration)
        }
    }


    @IBAction func refreshSceneAction(_ sender: Any) {
        removeAllAnchors(sender)
        sceneView.session.pause()
        sceneView.session.run(worldTrackingConfiguration)
    }


    //MARK: -  Actions
    @IBAction func removeAllAnchors(_ sender: Any) {

        while ((sceneView.layer.sublayers?.count ?? 0) > 1) {
            sceneView.layer.sublayers?.last?.removeFromSuperlayer()
        }
        updateQueue.async {
            self.anchors.forEach { self.sceneView.session.remove(anchor: $0) }
            self.imageAnchors.forEach { self.sceneView.session.remove(anchor: $0) }

            self.imageAnchors.removeAll()
            self.anchors.removeAll()
        }
        
        photosCollectionView.removeAll()
    }


    @objc func sceneViewDidTapAction(_ gesture: UITapGestureRecognizer) {
        if gesture.location(in: photosCollectionView).y > 0 {
            return
        }
        
        let location = gesture.location(in: sceneView)
        
        
        //SCNHitTestOption
       let hints = sceneView.hitTest(location, options: nil)
        
       
        if let tappedNode = hints.first?.node {
            print("tappedNode",tappedNode)
            // si se colocao sobre un SCNODe
        }
        
        self.takeImageAction(gesture)
    }






    @IBAction func takeImageAction(_ sender: Any) {

        guard let nodeForReference = nodeForReference,
            let node = nodeForReference.parent,
            let (cropedImage, highlightImage) = cutter.square(of: nodeForReference, in: sceneView) else {
                return
        }


        if let a = anchors.first(where: { $0.position.distance(from: node.position) < minimumDistanceBetweenCaptures }) {
            print("❌la imagen esta muy cerca a \(a.name ?? "-")")
            return
        }


        photosCollectionView.append(highlightImage)


        let newAnchor = PrescriptionAnchor(name: "rx-\(self.anchors.count)", transform: node.simdTransform)

        updateQueue.async {
            nodeForReference.isHidden = true
            //anchor2.image = cropedImage
            self.sceneView.session.add(anchor: newAnchor)
        }
        
        
//        if true  {
//            return
//        }


        let fields = RxRose.scale(to: cropedImage.size)


        let visionImage = VisionImage(image: cropedImage)
        visionImage.metadata = VisionImageMetadata()
        visionImage.metadata?.orientation = .topLeft



        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.1) {

            Vision.vision().onDeviceTextRecognizer().process(visionImage) { (result, error) in
                self.photosCollectionView.setText(text: result?.textInOrder ?? "N/A")
                print(result?.textInOrder ?? "nil")


                guard let recognizedBlock = result?.blocks.sorted(by: { $0.frame.origin.y < $1.frame.origin.y }) else {
                    return
                }




                var elements: [VisionTextElement] = []

                for box in recognizedBlock {
                    for line in box.lines {
                        elements.append(contentsOf: line.elements)
                    }
                }

                let stringB = NSMutableString()

                for field in fields {

                    stringB.append(field.name)
                    stringB.append(":")

                    elements.remove(callback: { (element) -> Bool in
                        if field.frame.contains(element.frame) {
                            stringB.append(element.text)
                            stringB.append(" ")
                            return true
                        }
                        return false
                    })

                    stringB.append("\n")




                }



                let text = stringB.uppercased.trim()
                self.photosCollectionView.setText(text: "\(text)\n\n\n")

                newAnchor.text = text


                

                if let nodeForPreviewAnchor = self.sceneView.node(for: newAnchor),
                    let informationNode = nodeForPreviewAnchor.childNode(withName: RescriptionPlaneNode.nodeName, recursively: false) as? RescriptionPlaneNode {


                    self.updateQueue.async {

                        if let name = text.components(separatedBy: "\n").first?.components(separatedBy: ":").last {
                            informationNode.informationScene.nameLabel.text = name
                        } else {
                            informationNode.informationScene.nameLabel.text = "Loading..."
                        }
                        
                        
                        informationNode.informationScene.setImage(named: "ab4")

                        informationNode.runInformationAction()



                    }

                }


            }
        }








    }

    //MARK: - session Delegate
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .notAvailable: title = ("notAvailable")
        case .normal: title = ("Normal")
        case .limited(let reason):

            switch reason {
            case .relocalizing: title = ("relocalizing")
            case .insufficientFeatures: title = ("insufficientFeatures")
            case .excessiveMotion: title = ("excessiveMotion")
            case .initializing: title = ("initializing")
            }

        }
    }


    //MARK: - Render Delegate
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {

        /// evita que se muetre un nodo de corte cerca a  un existente
        if let _ = anchors.first(where: { $0.position.distance(from: node.position) < minimumDistanceBetweenCaptures }) {
            nodeForReference?.isHidden = true
        } else {
            nodeForReference?.isHidden = false
        }


    }


    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        return node
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        /*
         Nodo para una prescripcion Capturada
         */
        if let prescriptionImage = anchor as? PrescriptionAnchor {
            anchors.append(prescriptionImage)
            updateQueue.async {
                // Create a plane to visualize the initial position of the detected image.
                let nodeForAnchor = self.nextNewNode!
                nodeForAnchor.position.z += 0.012
                nodeForAnchor.position.y += 0.001
                nodeForAnchor.runAction()
                node.addChildNode(nodeForAnchor)
            }
            updateQueue.async {
                self.nextNewNode = RescriptionPlaneNode(width: 0.0765, height: 0.051)
            }
            return
        }

        guard let imageAnchor = anchor as? ARImageAnchor else {
            return
        }

        /*
         Nodo de referencia para la imagen rastreada
         */
        updateQueue.async {
            let reference = RoseReferenceNode(image: imageAnchor.referenceImage)
            node.addChildNode(reference)
            self.nodeForReference = reference
            self.imageAnchors.append(imageAnchor)
        }
    }


    let rangeForMove: Float = 0.05 // 5 centimetros

}
