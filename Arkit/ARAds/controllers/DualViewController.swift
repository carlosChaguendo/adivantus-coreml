//
//  DualViewController.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit


class DualViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet weak var sceneView: ARSCNView!
    
    private let IImil: ARReferenceImage = {
        let width: CGFloat = 0.1275
        let image = UIImage(named: "2mil")!.cgImage!
        return ARReferenceImage.init(image, orientation: .up, physicalWidth: width)
    }()
    
    private let rose: ARReferenceImage = {
        let width: CGFloat = 0.076499999999999999
        let image = UIImage(named: "Rose1")!.cgImage!
        return ARReferenceImage.init(image, orientation: .up, physicalWidth: width)
    }()
    
    
    private let imageTracingConfiguration = ARImageTrackingConfiguration()
    
    private let worldTracingConfiguration = ARWorldTrackingConfiguration()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         sceneView.delegate = self

        
        let scene = SCNScene()

        
        // Set the scene to the view
        sceneView.scene = scene
        
        
        imageTracingConfiguration.trackingImages = [IImil, rose]
        imageTracingConfiguration.maximumNumberOfTrackedImages = 2
        
        worldTracingConfiguration.detectionImages = [IImil, rose]
        worldTracingConfiguration.planeDetection = []

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sceneView.session.run(imageTracingConfiguration)

    }
    
    @IBAction func reverseAction(_ sender: Any) {
        if sceneView.session.configuration == imageTracingConfiguration {
            sceneView.session.run(worldTracingConfiguration)
        }else {
            sceneView.session.run(imageTracingConfiguration)
        }
        
    }
    
    
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        

        
        guard let imageAnchor = anchor as? ARImageAnchor else { return }
        let referenceImage = imageAnchor.referenceImage
        
        
        // Create a plane to visualize the initial position of the detected image.
        let plane = SCNPlane(width: referenceImage.physicalSize.width,
                             height: referenceImage.physicalSize.height)
        let planeNode = SCNNode(geometry: plane)
        planeNode.opacity = 0.8
        
        /*
         `SCNPlane` is vertically oriented in its local coordinate space, but
         `ARImageAnchor` assumes the image is horizontal in its local space, so
         rotate the plane to match.
         */
        planeNode.eulerAngles.x = -.pi / 2
        
        /*
         Image anchors are not tracked after initial detection, so create an
         animation that limits the duration for which the plane visualization appears.
         */
//        planeNode.runAction(self.imageHighlightAction)
        
        // Add the plane visualization to the scene.
        node.addChildNode(planeNode)
        
        
        
    
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
