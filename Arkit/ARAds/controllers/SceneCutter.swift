//
//  SceneCutter.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit

class SceneCutter {

    fileprivate let ciContext = CIContext()


    fileprivate let kCIPerspectiveCorrection = "CIPerspectiveCorrection"
    fileprivate let kCIAffineTransform = "CIAffineTransform"


    func square(of refence: PrescriptionReferenceNode, in scene: ARSCNView) -> (UIImage, UIImage)? {

        let (topLeft, topRight, bottomLeft, bottomRight) = refence.projectSquare(to: scene)

        let ciimage = CIImage(cvPixelBuffer: scene.session.currentFrame!.capturedImage).oriented(.right)

        let screenSize = UIScreen.main.bounds.size
        let imageSize = ciimage.extent.size

        /// se esala de la dispositivo a la imagen
        let tl = topLeft.scale(screenSize, to: imageSize).coreImagesSpace(height: imageSize.height)
        let tr = topRight.scale(screenSize, to: imageSize).coreImagesSpace(height: imageSize.height)
        let bl = bottomLeft.scale(screenSize, to: imageSize).coreImagesSpace(height: imageSize.height)
        let br = bottomRight.scale(screenSize, to: imageSize).coreImagesSpace(height: imageSize.height)


        guard let highlight = self.drawHighlightOverlayForPoints(ciimage, points: (tl: tl, tr: tr, br: br, bl: bl)),
            let croped = self.perspectiveCorrect(ciimage, points: (tl: tl, tr: tr, br: br, bl: bl))else {
                return nil
        }




        guard let cgImageCroped = ciContext.createCGImage(croped, from: croped.extent),
            let cgImageHighlight = ciContext.createCGImage(highlight, from: highlight.extent) else {
                return nil
        }

        let cropedImage = UIImage(cgImage: cgImageCroped, scale: 1, orientation: .up)
        let highlightImage = UIImage(cgImage: cgImageHighlight, scale: 1, orientation: .up)

        return (cropedImage, highlightImage)

    }


    /// Recorta una porcion de la imagen y la rota si se requiere
    func perspectiveCorrect(_ image: CIImage, points: (tl: CGPoint, tr: CGPoint, br: CGPoint, bl: CGPoint), autoRotate: Bool = false) -> CIImage? {

        let perspectiveFilter = CIFilter(name: kCIPerspectiveCorrection)!
        perspectiveFilter.setValue(image, forKey: kCIInputImageKey)

        let corners = [
            (points.tl, "inputTopLeft"),
            (points.tr, "inputTopRight"),
            (points.br, "inputBottomRight"),
            (points.bl, "inputBottomLeft"),
        ]

        for (point, key) in corners {
            let vector = CIVector(cgPoint: point)
            perspectiveFilter.setValue(vector, forKey: key)
        }

        if !autoRotate {
            return perspectiveFilter.outputImage
        }

        guard let correctedImage = perspectiveFilter.outputImage else { return nil }
        let extent = correctedImage.extent

        let transformFilter = CIFilter(name: kCIAffineTransform)!
        /*----*
         |    |
         *----*/

        var transform = CGAffineTransform(translationX: extent.midX, y: -extent.midY)
        transform = transform.rotated(by: -.pi / 2)
        transform = transform.translatedBy(x: -extent.midX, y: extent.midY)
        transformFilter.setValue(correctedImage, forKey: kCIInputImageKey)
        transformFilter.setValue(transform, forKey: kCIInputTransformKey)

        return transformFilter.outputImage
    }


    func drawHighlightOverlayForPoints(_ image: CIImage, points: (tl: CGPoint, tr: CGPoint, br: CGPoint, bl: CGPoint), color: CIColor = CIColor(red: 1.0, green: 0, blue: 0, alpha: 0.6)) -> CIImage? {

        var overlay = CIImage(color: color)
        overlay = overlay.cropped(to: image.extent)
        overlay = overlay.applyingFilter("CIPerspectiveTransformWithExtent",
                                         parameters: [
                                             "inputExtent": CIVector(cgRect: image.extent),
                                             "inputTopLeft": CIVector(cgPoint: points.tl),
                                             "inputTopRight": CIVector(cgPoint: points.tr),
                                             "inputBottomLeft": CIVector(cgPoint: points.bl),
                                             "inputBottomRight": CIVector(cgPoint: points.br)
                                         ])

        return overlay.composited(over: image)
    }



}
