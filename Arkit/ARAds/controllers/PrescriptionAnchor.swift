//
//  PrescriptionAnchor.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/28/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit

class PrescriptionAnchor: ARAnchor {
    
    var image: UIImage?
    var text: String?
    
    var position: SCNVector3!
    
    
    override init(name: String, transform: simd_float4x4) {
        super.init(name: name, transform: transform)
        position = SCNVector3.of(transform: transform)
    }
    
    override init(transform: simd_float4x4) {
        super.init(transform: transform)
        position = SCNVector3.of(transform: transform)
    }
    
    required init(anchor: ARAnchor) {
        super.init(anchor: anchor)
        self.position = SCNVector3.of(transform: anchor.transform)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
