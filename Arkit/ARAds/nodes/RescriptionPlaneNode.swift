//
//  RescriptionPlaneNode.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/30/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

fileprivate extension SCNPlane {

    convenience init(spriteKit size: CGSize) {
        self.init(width: size.width / 10000, height: size.height / 10000)
    }

}

/// Nodo para todas las capturas de pantalla correspndientes a una prescripcion
/// mustra toda la informacion de la captura al usuario
class RescriptionPlaneNode: SCNNode {
    
    public static let nodeName = "pescription-plane-node"

    public static let informationNodeName = "pescription-plane-information-node"

    public private(set) var informationScene: RxInfoScene!
    
    

    public private(set) var size: CGSize!


    /// Nodo temporal el cual creara el efecto de flash
    private var highlightNode: SCNNode!
    
    /// Nodo que contenedor de la informacion escaneada
    public private(set) var informationNode: SCNNode!

    init(width: CGFloat = 0.0765, height: CGFloat = 0.051) {
        super.init()

        size = CGSize(width: width, height: height)
        informationScene = RxInfoScene.instance()
      
        geometry = createPlaneGeometry()
        setupTransform()

        highlightNode = createHighlightNode()
        informationNode = createInformationNode()

        addChildNode(highlightNode)
        addChildNode(informationNode)

        name = RescriptionPlaneNode.nodeName
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    public func createHighlightNode() -> SCNNode {

        let plane = SCNPlane(width: size.width, height: size.height)
        plane.firstMaterial?.diffuse.contents = UIColor.white

        let result = SCNNode(geometry: plane)
        result.opacity = 0.5
        return result
    }


    public func createInformationNode() -> SCNNode {
        let material = SCNMaterial() .then { material in
            material.isDoubleSided = true
            material.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
            material.diffuse.contents = informationScene
        }

        let plane = SCNPlane(spriteKit: informationScene.size).then { plane in
            plane.cornerRadius = 0.002
            plane.materials = [material]
        }

        let informationNode = SCNNode(geometry: plane).then { informationNode in
            informationNode.name = RescriptionPlaneNode.informationNodeName

            /// se ubica la scena de informacion segun corresponda
            /// la pocicion x se anima en run actions al igual que la opacidad
            informationNode.position.y = position.z
            informationNode.position.y -= Float(size.height / 2) - Float( plane.height / 2) //+ 0.001
            informationNode.position.z += 0.00011
            informationNode.opacity = 0
        }

        addSCNNodes(for: [informationScene.image], over: informationNode)
        return informationNode
    }


    /// Agrega SCNNodes justo encima de un SKNode ya que estos no permite eventos touch
    ///
    /// - Parameters:
    ///   - nodes: SKNode sobre los cuales se requiere agregar una accion
    ///   - over: SCNNode el cual contendra los nodos nuevos
    private func addSCNNodes(for nodes: [SKNode], over: SCNNode) {


        for sknode in nodes {

            let buttonsize = sknode.frame.size

            let plane = SCNPlane(spriteKit: buttonsize).then { plane in
                plane.firstMaterial?.diffuse.contents = UIColor.clear
            }

            let buttonNode = SCNNode(geometry: plane).then { node in
                node.name = sknode.name
                /// para que este al frente al momento de clikear
                /// por defecto la posicion (x,y) es el centro del nodo padre
                node.position.z += 0.00011
                /// toma la pocicion del nodo respecto al padre
                node.position.y = Float(sknode.position.y / 10000)
                node.position.x = Float(sknode.position.x / 10000)
            }
            over.addChildNode(buttonNode)
        }
    }

    public func createPlaneGeometry() -> SCNPlane {
        let plane = SCNPlane(width: size.width, height: size.height)
        plane.firstMaterial?.diffuse.contents = UIColor.clear
        return plane
    }



    public func setupTransform() {
        //opacity = 0.5
        eulerAngles.x = -.pi / 2
    }




    var imageHighlightAction: SCNAction {
        return .sequence([
                .wait(duration: 0.25),
                .fadeOpacity(to: 0.85, duration: 0.25),
                .fadeOpacity(to: 0.15, duration: 0.50),
                .fadeOpacity(to: 0.85, duration: 0.25),
                .fadeOpacity(to: 0.25, duration: 0.25),
                .fadeOut(duration: 0.5),
                .removeFromParentNode()
        ])
    }


    public func runAction() {
        highlightNode.runAction(self.imageHighlightAction)
    }

    public func runInformationAction() {
        let x = CGFloat(size.width / 2) + CGFloat( informationScene.size.width / 10000 / 2) + 0.005
        informationNode.runAction(.sequence([.fadeOpacity(to: 1, duration: 0.1), .moveBy(x: x, y: 0, z: 0, duration: 0.25)]))
    }





}
