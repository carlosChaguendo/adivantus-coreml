//
//  PrescriptionReferenceNode.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/28/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

/// Nodo de referencia para que se ubicara sobre una imagen de rastreo.
/// contiene los puntos de corte de las cuatro esquinas
class PrescriptionReferenceNode: SCNNode {
    
    lazy public private(set) var topLeftNode = SCNNode(geometry: SCNPlane(width: 0.01, height: 0.01))
    lazy public private(set) var topRightNode = SCNNode(geometry: SCNPlane(width: 0.01, height: 0.01))
    lazy public private(set) var bottomLeftNode = SCNNode(geometry: SCNPlane(width: 0.01, height: 0.01))
    lazy public private(set) var bottomRightNode = SCNNode(geometry: SCNPlane(width: 0.01, height: 0.01))
    
    public private(set) var referenceImage: ARReferenceImage!
    
    
    init(image referenceImage: ARReferenceImage) {
        super.init()

        self.referenceImage = referenceImage
        self.geometry = createPlaneGeometry()
        
        setupTransform()
        createNodesForSquare()
        
        addChildNode(topRightNode)
        addChildNode(topLeftNode)
        addChildNode(bottomLeftNode)
        addChildNode(bottomRightNode)
    }
    
    
    public func createPlaneGeometry() -> SCNPlane {
        let material = SCNMaterial()
        //material.diffuse.contents = UIColor.yellow
        
        let plane = SCNPlane(width: referenceImage.physicalSize.width, height: referenceImage.physicalSize.height )
        
        // Create the layer
        let layer = CALayer()
        layer.frame = CGRect(x: 0, y: 0, width: 1000, height: 1000)
        layer.borderColor = UIColor.red.cgColor
        layer.borderWidth = 10
        
        material.lightingModel = SCNMaterial.LightingModel.constant
         material.diffuse.contents = layer
        
        plane.materials = [material]
        return plane
    }
    
    
    public func setupTransform() {
        opacity = 0.5
        eulerAngles.x = -.pi / 2
    }
    
    
    public func createNodesForSquare() {
        
        guard let plane = self.geometry as? SCNPlane else {
            return
        }
        
        let width = Float(plane.width / 2)
        let height = Float(plane.height / 2)
        
        
        self.topLeftNode = self.createNodeReference(color: .red)
        self.topLeftNode.position.x -= width
        self.topLeftNode.position.y += height
        
        self.topRightNode = self.createNodeReference(color: .green)
        self.topRightNode.position.x += width
        self.topRightNode.position.y += height
        
        self.bottomLeftNode = self.createNodeReference(color: .blue)
        self.bottomLeftNode.position.x -= width
        self.bottomLeftNode.position.y -= height
        
        self.bottomRightNode = self.createNodeReference(color: .white)
        self.bottomRightNode.position.x += width
        self.bottomRightNode.position.y -= height

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    public func createNodeReference( color: UIColor? = nil) -> SCNNode {
        
        let result = SCNNode(geometry: SCNPlane(width: 0.005, height: 0.005))
        
        if let color = color {
            let material = SCNMaterial()
            material.diffuse.contents = color
            result.geometry?.materials = [material]
        }
        result.opacity = 0
        result.position.z += 0.0001
        return result
    }
    
    
    /// Projecta las posiciones de las cuatro esquinas en la escena
    ///
    /// - Parameter sceneView: ecena del usuario
    /// - Returns: Los puntos corres[ondientes a la 4 esquina del nodo, dependen de su pocicion y tamanio
    public func projectSquare(to sceneView: ARSCNView) -> (tl: CGPoint, tr: CGPoint, bl: CGPoint, br: CGPoint) {
        
        let tl = projectPosition(of: topLeftNode, to: sceneView)
        let tr = projectPosition(of: topRightNode, to: sceneView)
        let bl = projectPosition(of: bottomLeftNode, to: sceneView)
        let br = projectPosition(of: bottomRightNode, to: sceneView)
        
        return (tl, tr, bl, br)
        
    }
    
    
    /// Projecta una pocicion relativa al nodo en la ecena del usuario
    ///
    /// - Parameters:
    ///   - node: nodo
    ///   - sceneView: escena del uusario
    /// - Returns: la reprecentacion de la pocicion del nodo en cordenadas de la pantalla
    public func projectPosition(of node: SCNNode, to sceneView: ARSCNView) -> CGPoint {
        
        guard var nodeForAnchor = node.parent else {
            preconditionFailure()
        }
        
        var position = node.position
        
        /// hasta que llege al root node que ya no tiene padre
        while let rootNode = nodeForAnchor.parent {
            position = nodeForAnchor.convertPosition(position, to: rootNode)
            nodeForAnchor = rootNode
        }
        
        let projection = sceneView.projectPoint(position)
        return CGPoint(x: CGFloat(projection.x), y: CGFloat( projection.y))
    }
    
    
}
