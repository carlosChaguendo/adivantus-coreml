//
//  RoseReferenceNode.swift
//  ARAds
//
//  Created by Carlos Chaguendo on 11/28/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit
import ARKit

class RoseReferenceNode: PrescriptionReferenceNode {
    
    override func createPlaneGeometry() -> SCNPlane {
        
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.yellow
        
        let plane = SCNPlane(width: referenceImage.physicalSize.width, height: referenceImage.physicalSize.height * 2 )
        plane.materials = [material]
        return plane
    }
    
    override func setupTransform() {
        super.setupTransform()
        position.z += 0.012
    }
    
    override func createNodesForSquare() {
        super.createNodesForSquare()
        
        //topLeftNode.position.y += 0.012
       // topRightNode.position.y += 0.012
        //bottomLeftNode.position.y += 0.012
        //bottomRightNode.position.x += 0.012
    }
    
}
