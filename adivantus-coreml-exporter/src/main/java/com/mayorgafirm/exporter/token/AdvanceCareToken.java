package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdvanceCareToken extends TokenBuilder {

	public AdvanceCareToken(Random random) {
		super(random);
	}
	
	List<String> pnames = Arrays.asList("ADVANCED CARE PHARMACY");
	
	List<String> INS = Arrays.asList("IND", "LF", "CG", "JF", "NR", "IND", "NU", "MS", "D2");
	
	@Override
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		tokens = new ArrayList<>();
		labels = new ArrayList<>();

		String rx = randomizer.getRandomRXNumberAdvance();
		String resident = randomizer.getRandomResidentname();
		String med = randomizer.getRandonMedicationName();

		
		add(TokenName.pharmacy_name, getValueOrRandom(pnames));
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.phone, "(951) 256-8800");
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		add(TokenName.hour, randomizer.getRandoDate(hour));
		add(TokenName.rx_number, rx);
		add(TokenName.resident_name, resident);

		add(TokenName.medication_name, med);
		add(TokenName.qty, randomizer.getRandomQuantity());
		add(TokenName.generic, getValueOrRandom("GEN EQ : ")+randomizer.getRandonMedicationName());
		
		add(TokenName.dosage, randomizer.getRandomDosage());
	
		add(TokenName.ins, getValueOrRandom(INS));
		
		add(TokenName.fill_date, "FILLED: " + randomizer.getRandoDate(M_dd_yy));
	
	
		add(TokenName.ins, getValueOrRandom(INS));
		add(TokenName.refill, randomizer.getRandomRefill());


		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
	}

}
