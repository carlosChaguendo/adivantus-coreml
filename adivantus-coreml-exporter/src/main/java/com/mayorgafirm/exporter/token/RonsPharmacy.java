package com.mayorgafirm.exporter.token;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RonsPharmacy   extends TokenBuilder{

	public RonsPharmacy(Random random) {
		super(random);
	}
	
	public static List<String> qtyPrefixVariations = Arrays.asList("#");
	
	private List<String> refillPrefixVariations =  Arrays.asList("REM : ", "REFILL : ", "RIS REM: ", "RIS RAM: ", "RILE REM: ", "RFLS REM: ", "RILS REM: ");
	
	private List<String> phonePrefixVariations = Arrays.asList("P:", "F:");
	
	private List<String> cd8Subfix =  Arrays.asList("UH/PR", "CM", "AE", "MC","KR", "AB","ME");
	
	/**
	LEFKOWITZ, JOSEPHINE
	GABAPENTIN 10OMG CAP #30
	GENERLC FOR: NEURONTIN 100MG CAP
	RX# 5262749/0
	
	FAC: 716I 8T:
	JOYFUL HOMES #7
	CD8: 5AB
	NDC: 67877-0222-10
	MFG: ASCEND LABÖRATORIES
	
	TAKE 1 CAPSULE BY MOUTH AT
	BEDTIME
	(BAB)
	DR.FROLOV,STEFAN
	D8/24/2018
	RX8/KLN
	RX# 5252749/0
	05/24/2018
	KL
	
	GABAPENTIN 100MG CAP #30
	LEFKOWITZ, JOSEPHINE
	RONS PHARMACY 6ERVICES RFLS REM: 3
	10140 BARNES CANYON ROAD
	8AN DIEQ0, CA 92121
	
	P:858.652.6900 F:858.652.6999
	IMP: 216/
	DES: WHITE, OBLONG
	EXP: 03/20/2019 1 OF 1
	
	/// ph name initial only
	 Class             Precision(%)   Recall(%)      
	_trash_           100.00         99.74          
	address           100.00         99.85          
	caution           93.89          100.00         
	date              99.96          100.00         
	dosage            99.75          99.75          
	encino_number     100.00         100.00         
	expedition_date   100.00         100.00         
	fill_date         100.00         97.77          
	generic           99.94          100.00         
	hour              100.00         100.00         
	ins               100.00         100.00         
	medication_name   99.83          99.92          
	ndc               100.00         100.00         
	original_date     100.00         100.00         
	pagination        100.00         100.00         
	pharmacy_name     100.00         100.00         
	phone             100.00         100.00         
	physician_name    100.00         99.95          
	qty               100.00         99.97          
	refill            100.00         100.00         
	resident_name     99.97          99.02          
	rons_cd8          100.00         100.00         
	rons_fact         100.00         100.00         
	rons_manufacturer 100.00         100.00         
	rx_number         100.00         100.00         
	url               100.00         100.00  
	
	
	----------------------------------
	Class             Precision(%)   Recall(%)      
	_trash_           100.00         99.74          
	address           100.00         99.85          
	caution           93.89          100.00         
	date              99.96          100.00         
	dosage            99.75          99.75          
	encino_number     100.00         100.00         
	expedition_date   100.00         100.00         
	fill_date         100.00         97.77          
	generic           99.94          100.00         
	hour              100.00         100.00         
	ins               100.00         100.00         
	medication_name   99.83          99.92          
	ndc               100.00         100.00         
	original_date     100.00         100.00         
	pagination        100.00         100.00         
	pharmacy_name     100.00         100.00         
	phone             100.00         100.00         
	physician_name    100.00         99.95          
	qty               100.00         99.97          
	refill            100.00         100.00         
	resident_name     99.97          99.02          
	rons_cd8          100.00         100.00         
	rons_fact         100.00         100.00         
	rons_manufacturer 100.00         100.00         
	rx_number         100.00         100.00         
	url               100.00         100.00  
	
	
	/// agreg date + CG
	 _trash_           100.00         99.91          
	address           100.00         100.00         
	caution           91.58          100.00         
	date              100.00         100.00         
	dosage            99.96          99.78          
	encino_number     100.00         100.00         
	expedition_date   100.00         100.00         
	fill_date         100.00         96.68          
	generic           100.00         100.00         
	hour              100.00         100.00         
	ins               100.00         100.00         
	medication_name   99.86          99.94          
	ndc               100.00         100.00         
	original_date     100.00         100.00         
	pagination        100.00         100.00         
	pharmacy_name     100.00         100.00         
	phone             100.00         100.00         
	physician_name    100.00         99.95          
	qty               100.00         100.00         
	refill            100.00         100.00         
	resident_name     99.97          98.62          
	rons_cd8          100.00         100.00         
	rons_fact         100.00         100.00         
	rons_manufacturer 99.76          100.00         
	rx_number         100.00         100.00         
	url               100.00         100.00
	
	
	Class             Precision(%)   Recall(%)      
	_trash_           100.00         99.74          
	address           100.00         99.75          
	caution           92.64          100.00         
	date              99.96          100.00         
	dosage            99.78          99.82          
	encino_number     100.00         100.00         
	expedition_date   100.00         100.00         
	fill_date         99.93          97.25          
	generic           99.95          100.00         
	hour              100.00         100.00         
	ins               100.00         100.00         
	medication_name   99.92          99.94          
	ndc               100.00         100.00         
	original_date     100.00         100.00         
	pagination        100.00         100.00         
	pharmacy_name     100.00         100.00         
	phone             99.96          100.00         
	physician_name    100.00         99.90          
	qty               100.00         100.00         
	refill            100.00         100.00         
	resident_name     99.94          98.79          
	rons_cd8          100.00         100.00         
	rons_fact         100.00         100.00         
	rons_manufacturer 99.76          100.00         
	rx_number         100.00         100.00         
	url               100.00         100.00  
	

	
	 

	 */
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		
		tokens = new ArrayList<>();
		labels = new ArrayList<>();
		
		String residentName = randomizer.getRandomResidentname(); 
		String rx =  randomizer.getRandomRXNumberRons();
		String date = randomizer.getRandomContinueDate(MMddyyyy);
		String med = randomizer.getRandonMedicationName();
		String refill =  getValueOrRandom(refillPrefixVariations)+randomizer.random.nextInt(9);
		String qty =  randomizer.getRandomQty(qtyPrefixVariations);
		
		add(TokenName.resident_name, residentName);
		add(TokenName.medication_name, med );
		add(TokenName.qty, qty);
		add(TokenName.generic, randomizer.getRandonNameGeneric());
		add(TokenName.rx_number, randomizer.getRandomRXNumberRons());
		
		add(TokenName.rons_fact, "FAC: "+getFactNumber()+ getValueOrRandom( " / TS:", " I ST:", "I 8T", "I 8T:", "/ 8T:", "/8T"));
		add(TokenName.rons_cd8, getValueOrRandom("CD8: ", "CDS: ")+(randomizer.random.nextInt(8)+1)+getValueOrRandom(cd8Subfix));
		add(TokenName.ndc, "NDC: "+getNdc());
		add(TokenName.rons_manufacturer, getManufaturer());
		
		add(TokenName.dosage, randomizer.getRandomDosage());
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		add(TokenName.date, date);
		add(TokenName.ins, getValueOrRandom(roseUR));
		add(TokenName.rx_number,rx);
		add(TokenName.date, date);
		
		add(TokenName.ins, "KL");
		add(TokenName.medication_name, med );
		add(TokenName.qty, qty);
		add(TokenName.resident_name, residentName);
		add(TokenName.pharmacy_name,  getValueOrRandom("RONB PHARMACY 6ERVICES","RONS PHARMACY 6ERVICES", "RONB PHARMACY 8ERVICES", "RONG PHAAMACY BERVICES", "AONS PHARMACY 6EAVICES"));
		add(TokenName.refill,refill);
		
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.phone, getValueOrRandom(phonePrefixVariations)+getRandomPhoneNumber());

		add(TokenName.expedition_date, "EXP:" + randomizer.getRandoDate(MMddyyyy));
		add(TokenName._trash_, getValueOrRandom(roseTrash));
		add(TokenName.pagination, getRandomPage());
		
		
		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
		
	}
	
	public String getRandomPage() {
		return randomizer.getRandomPage();
	}
	
	public String getManufaturer() {
		String company = randomizer.faker.company().name();
		return getValueOrRandom("G MFG: ", "MFG: ", "MIG: ", "MFA: ", "MTG: ", "MFG :")+company;
	}
	
	public String getNdc() {
		java.util.Random rand = this.randomizer.random;
		int num1 = (rand.nextInt(99) + 1) * 100 + (rand.nextInt(88) * 10) + rand.nextInt(8)+ (rand.nextInt(8)*10 + (rand.nextInt(8)));
		int num2 = rand.nextInt(10000);
		int num3 = rand.nextInt(100);
		DecimalFormat df2 = new DecimalFormat("00"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros
		DecimalFormat df5 = new DecimalFormat("00000"); // 5 zeros
		String ndc = "" + df5.format(num1) + "-" + df4.format(num2) + "-" + df2.format(num3);
		return ndc;
	}
	
	public String getFactNumber() {
		java.util.Random rand = this.randomizer.random;
		int num1 = (rand.nextInt(9) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros\
		
		return df3.format(num1);
	}
	
	public String getRandomPhoneNumber() {
		java.util.Random rand = this.randomizer.random;
		int num1 = (rand.nextInt(9) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);
		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros
		String phoneNumber = "" + df3.format(num1) + "." + df3.format(num2) + "." + df4.format(num3);
		return phoneNumber;

	}

}
