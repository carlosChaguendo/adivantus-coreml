package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.joda.time.format.DateTimeFormat;

import com.github.javafaker.Name;

public class TokenBuilder {

	public List<TokenName> tokens = new ArrayList<>();
	public List<String> labels = new ArrayList<>();

	public Random randomizer;
	
	
	public org.joda.time.format.DateTimeFormatter MMddyyyy = DateTimeFormat.forPattern("MM/dd/yyyy");
	public org.joda.time.format.DateTimeFormatter MMddyy = DateTimeFormat.forPattern("MM/dd/yy");
	public org.joda.time.format.DateTimeFormatter MM_dd_yy = DateTimeFormat.forPattern("MM-dd-yy");
	public org.joda.time.format.DateTimeFormatter M_dd_yy = DateTimeFormat.forPattern("M-dd-yy");

	public org.joda.time.format.DateTimeFormatter hour = DateTimeFormat.forPattern("HH:mm");
	
	
	
	


	public static List<String> pharmacyTextVariations = Arrays.asList("PNARMACY", "PHARMACY", "PHA RMAC", "RPHARMACY",
			"PHARMAC");
	public static List<String> pharmacyNameVariations = Arrays.asList("R", "ROSE", "ROSE **");
	
	List<String> caution = Arrays.asList(
			 "FEDER:L LAW POHIBTS TRANSFER OFTHIS DRUG IO ANY PERSON OTHER JHAN PATETJI FCN irt* .",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OFTHIS DRUG IO ANY PERSON OTHER THAN PAIENT FO WHOM PTESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOMPRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THS DRUG TO ANY PERSON OTHER THAN PATIEÑT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG IO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBED",
			 "CAUTON: FEDERAL LAW PROHIBITS TRANSFER OFTHIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OFTHIS DRUG TO ANY PERSON OTHER THAN PATIENT FORWHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG IO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIEED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OFTHIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBEDO",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OFTHIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL AW PROHI8ITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR WHCOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FON HOM PRESCTNBED",
			 "CAUTION: FEDERAL LAW PROHIBITS RANSFER OF THIS DG 1O ANY PERSON OTHER IHAN PANEŃT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FORWHOM PRESCRIBED",
			 "CAUTON: FEDENAL LAW PROHIBITS TRANSFER OF THIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR VHOM PRESCRIBED",
			 "CAUTION. FEDERAL LAW PROHIBUTS TRANSFER OF THIS DRUG IO ANY PERSON OTHER HAN PATENT FOR WHOM PRESCRIBED",
			 "CAUTION. FEDERAL LAW PROHIBITS TRANSFER OF THIS DRUG IO ANY PERSON OIHER THAN PATIENT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHIBIIS TRANSFER OFTHIS DRUG IO ANY PERSON OTHER THAN PATIENT FOR WHOM PRESCRIBED",
			 "CAUTION: FEDERAL LAW PROHITS IRANFER OFTS DRUG 10 AY PERSON OHER THAN BATIOIT POR WHOM PRCSCR",
			 "CAUION FEDERAL LAW PROHBS 1AFER CRTHS DRUG 1OAWPERSON OHCR THAN BAEIT FOR WHOMPRESCRECO",
			 "CAUION FEDERAL LAW PROHIBTS TRANSFER ORTHS ORUG 10 ANY PERSON OHR THAN PARCIFOR WOM PRESCSED",
			 "CAUIOI; FEDERAL LAW PROH3TS TRANSFER CFTHIS DRUG TO ANY PERSON OTHER THAN PATIENT FOR H NC",
			 "CAUTION:FEDERAL LAW PROHIBITS THE LRANSTER OF THIS DUG TO ANY, PERSON OTHER THAN THE PATIENT FOR WHOM A WAS PRESCRIBED. DO NOTUSE IUT",
			 "CAUIKJ, FEDE RAL LAW PIRORLBIUS TRAVSIRLOF IHIS MUK. IO ASNY PLRSON OIHEI THA");
	
	
	
	List<String> roseTrash =  Arrays.asList(
			"OVAL:APO;ATV40;WHITE:WHITE",
			"OVAL.F.5 / 4 WHITE TO OFF-WHLTE;WHITE TO OFF-WHITE",
			"OVAL STO 200;BROWNISH-RED; BROWNISH-RED",
			"OVAL;STO 200;BROWNISH-RED BROWNISH-RED",
			"ROUND:APO:QUE/25,PEACH;PEACH",
			"OVALSTO 200;BROWNISH-RED;BROWNISH-RED",
			"CAPSULE-SHAPED:GG 336:112",
			"ROUND MYLAN 183;20:LIGHT-BLUE;LIGHT-BLUE",
			"OVAL:STO 200;BROWNISH-RED;BROWNISH-RED",
			"ROUND:E 64:LIGHT-BLUE;LIGHT-BIUE",
			"OVAL;STO 200;BROWNISH-RED;BROWNISH-RED",
			"ROUND TCL080;BROWN BROVWN",
			"OVAL;796;PINK:PINK",
			"ROUND A102;WHITE TO OFF-WHITE WHITE TO OFF-WHITE",
			"ROUND;A102;WHITE TO OFF-WHITE;WHITE TO OFF-WHITE",
			"ROUND A102.WHITE TO OFF-VIHITE WHITE TO OFF-VHIT",
			"OVAL STO 200;BROWNISH-RED; BROWNISH-RED",
			"CAPSULE-SHAPED GG 336;112",
			"OVAL:STO 200:BROV/NISH-RED;BIOVMISH-RED",
			"OVAL,STO 200;BROWNISH-RED BROWNISH-RED",
			"ROUND MYLAN 183:20;LIGHT-BLUE:LIGHT-BLUE",
			"ROUND MYLAN 183:20;LIGHT-BLUE: LIGHT-BLUE",
			"CAPSULE-SHAPED;H5/325;R/P;WHITE TO OFF-WIL LF:",
			"CAPSULE;LUPIN;500;LIGHT-GREEN; DARK-GREEN",
			"ROUND;E 64;LIGHT-BLUE;LIGHT-BLUE");
	
	
	
	List<String> roseUR = Arrays.asList("R U","R C","N C","N U");
	List<String> roseINS = Arrays.asList("CG","ES","HJ", "JH", "CC", "VN","AM", "LM", "AN");
	
	List<String> roseExpeditionDatePrefizVariations = Arrays.asList("EXP DATE: ", "EXP DATE ", "EXP DATE: ");

	public TokenBuilder( Random random) {
		super();
		
		this.randomizer = random;

		
	}



	

	public void add(TokenName token, String label) {
		tokens.add(token);
		labels.add(label);
	}
	
	public <T> T getValueOrRandom(List<T> list) {
		return randomizer.get(list);
	}
	
	public  <T> T getValueOrRandom(T... list) {
		return randomizer.get(Arrays.asList(list));
	}

	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		tokens = new ArrayList<>();
		labels = new ArrayList<>();

		add(TokenName.pharmacy_name, getValueOrRandom(pharmacyNameVariations) + " " + getValueOrRandom(pharmacyTextVariations));
		add(TokenName.phone, "(714) 662-0548");
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.caution, getValueOrRandom(caution));
		add(TokenName.rx_number, randomizer.getRandomRXNumber());
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		add(TokenName.ins, getValueOrRandom(roseINS));
		add(TokenName.resident_name, randomizer.getRandomResidentname());
		add(TokenName.ins, getValueOrRandom(roseUR));
		add(TokenName.medication_name, randomizer.getRandonMedicationName());
		
		

		add(TokenName.dosage, randomizer.getRandomDosage());
		add(TokenName.fill_date, randomizer.getRandoDate(MMddyyyy)+ getRandonFillInitials());
		add(TokenName.qty, randomizer.getRandomQuantity());
		add(TokenName.generic, randomizer.getRandonNameGeneric());
		add(TokenName.refill, randomizer.getRandomRefill());
		add(TokenName._trash_, getValueOrRandom(roseTrash));
		add(TokenName.last_fill_date, getValueOrRandom("LF:", "LF: ") + randomizer.getRandoDate(MMddyy));
		add(TokenName.expedition_date, getValueOrRandom(roseExpeditionDatePrefizVariations) + randomizer.getRandoDate(MMddyyyy));

		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
	}
	
	

	public String getRandonFillInitials() {
		int num = randomizer.random.nextInt(2);
		switch (num) {
		case 0:
		case 1:
			return getValueOrRandom(" CG"," ES"," HJ", " JH", " CC", " VN"," AM", " LM", " AN", " CO", " ES", " CE", " TH"," JH");
		default:
			
			Name name = randomizer.faker.name();
			return " "+name.lastName().substring(0, 1).toUpperCase() + "" + name.firstName().substring(0, 1).toUpperCase();
	
	
		}
	}

}
