package com.mayorgafirm.exporter.token;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.mayorgafirm.exporter.model.Medication;
import com.mayorgafirm.exporter.model.Physician;
import com.mayorgafirm.exporter.model.Resident;

public class Random {

	public java.util.Random random = new java.util.Random();

	private int globalindex = 0;

	private List<Medication> medication;
	private List<Resident> residents;
	private List<Physician> phiciciasn;
	private List<String> address;
	private List<String> caMedications;
	private List<String> dosages;
	private List<String> webUrl;

	public Faker faker = new Faker(Locale.ENGLISH);

	private List<Integer> qtyValues = Arrays.asList(28, 30, 31, 60, 90, 84, 56, 31, 56, 11, 14, 42);

	private List<String> genericPrefixVariations = Arrays.asList(
			"GENERIC FOR: ",
			"GENERIC FOR ",
			"IENERIC FOR :",
			"GENERLC FOR: ",
			"ENERIC FOR :",
			"GENERIC FOR. ",
			"ENERIC FOR :",
			"GENEREC FOR ",
			"GENENCFOR ",
			"GENERNC ",
			"BENERIC FOR.",
			"EGENERIC FOR: ",
			"CENERIC FOR ",
			"GENERIC FOR ");

	private List<String> qtyPrefix = Arrays.asList("QTY","QTY#", "CQY#", "C!TY#", "QY: ", "#", "CTY#T ", "CGTY# ", "QTY#*",
			"QY:");

	private List<String> medicationPresentations = Arrays.asList("TAB(MAJOR)", "TAB", "CAP", "TER", "CAP(TRUN)", "CAP", "TAE",
			"CAP(TEVE)", "TAB(RUGBY)", "CER(SAND)", "TAB(SOLCO)", "CAPTRUN)", "APRUN)", "TAB(SOLC", "PAB(SOLC", "TABLET");

	private List<String> strenggtlabels = Arrays.asList("MG", "%", "MCG", "G", "MG/ML", "GM", "OZ", "KB", "TBS", "IU",
			"UI","TABS");
	

	


	public Random(int globalindex, List<Medication> medication, List<Resident> residents, List<Physician> phiciciasn, List<String> address, List<String> caMedications, List<String> dosages, List<String> webUrl) {
		super();
		this.globalindex = globalindex;
		this.medication = medication;
		this.residents = residents;
		this.phiciciasn = phiciciasn;
		this.address = address;
		this.caMedications = caMedications;
		this.dosages = dosages;
		this.webUrl = webUrl;

	}
	
	public <T> T get(List<T> list) {
		return getValueOrRandom(list);
	}
	
	public void willCreate() {
		globalindex++;
	}

	private <T> T getValueOrRandom(List<T> list) {

		if (globalindex > list.size() - 1) {
			int i = random.nextInt(list.size());

			T obj = list.get(i);

			if (Objects.isNull(obj)) {
				return list.get(0);// throw new IllegalStateException("lalista1 "+i+" es mayora "+list.size());
			}

			return obj;
		}

		T obj = list.get(globalindex);

		if (Objects.isNull(obj)) {
			return list.get(0);// throw new IllegalStateException("lalista "+globalindex+" es mayora
								// "+list.size()+ " "+list);
		}
		return obj;

	}

	public String getRandonPharmacyAddres() {
		int num = random.nextInt(3);
		switch (num) {
		case 0:
			return getValueOrRandom(address).replaceAll(":", " ").toUpperCase(); // 1220 W Hemlock Way # 110:Aliso
		case 1:
			return getValueOrRandom(address).split(":")[1].toUpperCase(); // Aliso Viejo, CA 92707
		case 2:
			return getValueOrRandom(address).replaceAll(":", ", ").toUpperCase(); //9240 GARDEN GROVE BLD. SUITE #20, GARDEN GROVE, CA 928
		default:
			return getValueOrRandom(address).split(":")[0].toUpperCase(); // 1220 W Hemlock Way # 110
		}
	}
	
	
	public String getRandomDosage() {
		return getValueOrRandom(dosages).toUpperCase();
	}

	/**
	 * @return (123) 456-789 
	 */
	public String getRandomPhoneNumber() {
		java.util.Random rand = this.random;
		int num1 = (rand.nextInt(9) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);
		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros
		String phoneNumber = "(" + df3.format(num1) + ") " + df3.format(num2) + "-" + df4.format(num3);
		return phoneNumber;

	}

	/**
	 * @return 12345-678-90
	 */
	public String getRandomEncinoNumber() {
		java.util.Random rand = this.random;
		int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);
		DecimalFormat df2 = new DecimalFormat("00"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df5 = new DecimalFormat("00000"); // 4 zeros
		String phoneNumber = "" + df5.format(num1) + "-" + df4.format(num2) + "-" + df2.format(num3);
		return phoneNumber;

	}

	/**
	 * @return 123-456-7898
	 */
	public String getRandomPhoneNumber2() {
		java.util.Random rand = this.random;
		int num1 = (rand.nextInt(9) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);
		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros
		String phoneNumber = "" + df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
		return phoneNumber;
	}

	/**
	 * @return RX# 123456
	 */
	public String getRandomRXNumber() {
		int num1 = random.nextInt(9);
		int num2 = random.nextInt(9);
		int num3 = random.nextInt(9);
		int num4 = random.nextInt(9);
		int num5 = random.nextInt(9);
		int num6 = random.nextInt(9);
		return "RX# " + num1 + "" + num2 + "" + num3 + "" + num4 + "" + num5 + "" + num6;
	}
	
	
	/**
	 * @return RX# 123456789
	 */
	public String getRandomRXNumberOptuns() {
		int num1 = random.nextInt(9);
		int num2 = random.nextInt(9);
		int num3 = random.nextInt(9);
		int num4 = random.nextInt(9);
		int num5 = random.nextInt(9);
		int num6 = random.nextInt(9);
		int num7 = random.nextInt(9);
		int num8 = random.nextInt(9);
		int num9 = random.nextInt(9);
		return "RX: " + num1 + "" + num2 + "" + num3 + "" + num4 + "" + num5 + "" + num6+ "" + num7+ "" + num8+ "" + num9;
	}
	
	/**
	 * @return RX# 2991606/0
	 */
	public String getRandomRXNumberRons() {
		int num1 = random.nextInt(9);
		int num2 = random.nextInt(9);
		int num3 = random.nextInt(9);
		int num4 = random.nextInt(9);
		int num5 = random.nextInt(9);
		int num6 = random.nextInt(9);
		int num7 = random.nextInt(9);
		int num8 = random.nextInt(9);
		return "RX# " + num1 + "" + num2 + "" + num3 + "" + num4 + "" + num5 + "" + num6+"" + num7+"/"+num8;
	}
	
	/**
	 * @return RX 004-299160
	 */
	public String getRandomRXNumberAdvance() {
		int num1 = random.nextInt(9);
		int num2 = random.nextInt(9);
		int num3 = random.nextInt(9);
		int num4 = random.nextInt(9);
		int num5 = random.nextInt(9);
		int num6 = random.nextInt(9);
		return "RX " + num1 + "" + num2 + "" + num3 + "-" + num4 + "" + num5 + "" + num6+""+num2+""+num4+""+num1;
	}
	
	
	

	public String getRandomPhicicianName() {

		int num = random.nextInt(5);
		Physician ph = getValueOrRandom(phiciciasn);

		switch (num) {
		case 0:
			return "DR." + ph.getLastName().toUpperCase() + ", " + ph.getFirstName().split(" ")[0].toUpperCase();
		case 1:
			return "DR." + ph.getLastName().toUpperCase() + "." + ph.getFirstName().split(" ")[0].toUpperCase();
		case 2:
			return "DR." + ph.getLastName().toUpperCase() + ", " + ph.getFirstName().split(" ")[0].toUpperCase();
		case 3:
			return "DR." + ph.getLastName().toUpperCase() + " " + ph.getFirstName().split(" ")[0].toUpperCase();
		case 4:
			return "DR." + ph.getLastName().toUpperCase() + " " + ph.getFirstName().split(" ")[0].toUpperCase();
		default:
			return "DR." + getRandomNamePoint().toUpperCase();
		}
	}

	public String getRandomPhicicianNameInitialOnly() {

		int num = random.nextInt(2);
		Physician ph = getValueOrRandom(phiciciasn);

		String lastName = ph.getLastName().substring(0, 1).toUpperCase();
		String firstname = ph.getFirstName().split(" ")[0].toUpperCase();

		switch (num) {
		case 0:
			return  lastName + ". " + firstname;
		case 1:
			return  lastName + ". " + firstname;
		default:
			Name name = faker.name();
			return name.lastName().substring(0, 1).toUpperCase() + ". " + name.firstName().toUpperCase();
			
		}
	}
	
	public String getRandomWeb() {
		return getValueOrRandom(webUrl);
	}

	public String getRandomQuantity() {
		int qty = getValueOrRandom(qtyValues);
		String prefix = getValueOrRandom(qtyPrefix);
		return prefix + qty;

	}
	
	public String getRandomQty(List<String> prefixez) {
		int qty = getValueOrRandom(qtyValues);
		String prefix = getValueOrRandom(prefixez);
		return prefix + qty;
	}

	

	
	public String getRandomRefill() {
		int num = random.nextInt(9);
		switch (num) {
		case 0:
			return "REFILLS" + random.nextInt(9);
		case 1:
			return "REFILLS:" + random.nextInt(9);
		case 2:
			return "REFILLS: " + random.nextInt(9);
		case 3:
			return "REWILLS " + random.nextInt(9);
		case 4:
			return "REFILLS. NONE";
		case 5:
			String n1 =  new DecimalFormat("00").format( random.nextInt(15)+1);
			return "REF-" + n1;
		case 6:
			return "NO REFILLS";
		case 7:
			String n =  new DecimalFormat("00").format( random.nextInt(15)+1);
			return  "("+n+") REFILLS REMAINING";
		case 8:
			return "RFLS REM:" + random.nextInt(9);
		default:
			return "REFILLS. NONE";
		}
	}

	public String getRandomResidentname() {
		int num = random.nextInt(2);
		Resident res = getValueOrRandom(residents);
		switch (num) {
		case 0:
			return res.getLastName().toUpperCase() + ", " + res.getFirstName().split(" ")[0].toUpperCase();
		case 1:
			return res.getLastName().toUpperCase() + " " + res.getFirstName().split(" ")[0].toUpperCase();
		default:
			return getRandomName().toUpperCase();
		}
	}

	public String getRandonNameGeneric() {
		String name = getRandonMedicationNameWithOutStrength();
		return getValueOrRandom(genericPrefixVariations) + name;
	}

	public String getRandonStrenght() {
		int num = random.nextInt(6);

		int num1 = random.nextInt(9);
		int num2 = random.nextInt(9);
		int num3 = random.nextInt(9);

		String name = getValueOrRandom(strenggtlabels);
		switch (num) {
		case 0:
			return num1 + "" + num2 + name;
		case 1:
			return num1 + "." + num2 + name;
		case 2:
			return num1 + "" + num2 + "" + num3 + name;
		case 5:
			return num3 + "" + num1 + "" + num2 + "-" + num1 + "" + num2 + "" + num3 + name; // 400-500MG
		case 6:
			return num3 + "" + num1 + "" + num2 + "-" + num1 + "" + num2 + "" + num3 + name + "-"; // 400-500MG-
		default:
			return num1 * num2 + "/" + num2 + " " + name;
		}

	}

	public String getRandomName() {
		Name name = faker.name();
		return name.lastName() + "," + name.firstName();
	}

	public String getRandomNamePoint() {
		Name name = faker.name();
		return name.lastName() + "." + name.firstName();
	}

	public String getRandonMedicationNameWithOutStrength() {
		int num = random.nextInt(2);

		switch (num) {
		case 0:
			Medication med = getValueOrRandom(medication);
			if (med.getName().length() > 5) {
				return med.getName().toUpperCase();
			}

			return getValueOrRandom(caMedications).toUpperCase();

		default:
			return getValueOrRandom(caMedications).toUpperCase();
		}
	}

	public String getRandonMedicationName() {
		int num = random.nextInt(2);

		switch (num) {
		case 0:
			Medication med = getValueOrRandom(medication);
			if (med.getName().length() > 5) {

				// if(med.getStrength() == null || "-".equals(med.getStrength()) ||
				// "0".equals(med.getStrength()) || "1".equals(med.getStrength()) ||
				// "N/A".equals(med.getStrength().toUpperCase()) ||
				// "UNK".equals(med.getStrength().toUpperCase())) {
				// return med.getName() +" "+med.getStrength().toUpperCase();
				// }

				return med.getName().toUpperCase() + " " + getRandonStrenght();
			}

			return getValueOrRandom(caMedications).toUpperCase() + " " + getRandonStrenght() + " "
					+ getValueOrRandom(medicationPresentations).toUpperCase();

		default:
			return getValueOrRandom(caMedications).toUpperCase() + " " + getRandonStrenght() + " "
					+ getValueOrRandom(medicationPresentations).toUpperCase();
		}
	}

	DateTime d1 = new DateTime("2015-01-01");
	DateTime d2 = new DateTime("2030-01-01");

	public String getRandoDate(org.joda.time.format.DateTimeFormatter frmt) {
		int days = Days.daysBetween(d1, d2).getDays();
		DateTime randomDate = d1.plusDays(ThreadLocalRandom.current().nextInt(days + 1))
				.plusHours(ThreadLocalRandom.current().nextInt(24))
				.plusSeconds(ThreadLocalRandom.current().nextInt(random.nextInt(256) + 1));
		return frmt.print(randomDate);
	}
	
	
	
	public String getRandomPage() {
		int page = this.random.nextInt(3)+1;
		int page2 = this.random.nextInt(3)+1;
		
		int a = Math.min(page, page2);
		int b = Math.max(page, page2);
		
		return a+" OF "+b;
	}
	
	
	DateTime d1continuew = new DateTime("2015-01-01");

	public String getRandomContinueDate(org.joda.time.format.DateTimeFormatter frmt) {
		
		d1continuew = d1continuew.plusDays(1);

		return frmt.print(d1continuew);
	}

}
