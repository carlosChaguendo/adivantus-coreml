package com.mayorgafirm.exporter.token;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class GilbertToken extends TokenBuilder {

	public GilbertToken(Random random) {
		super(random);
	}
	
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		tokens = new ArrayList<>();
		labels = new ArrayList<>();
		add(TokenName.pharmacy_name, getValueOrRandom( "GILBERT DRUGS", "GILBER7 DRUGS", "GILBERT DRUGSS", "CRGILBERT DRUGS", "GILBERT CDRUGS", "GILBERT DRUGSS", "GILBERT DRUGSS","GILBERRT DRUGS"));
		add(TokenName.phone, "(714) 638-8230");
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.rx_number, randomizer.getRandomRXNumber());
		add(TokenName.ins, getValueOrRandom(roseUR));

		add(TokenName.physician_name_initialis, randomizer.getRandomPhicicianNameInitialOnly());
		add(TokenName.resident_name, randomizer.getRandomResidentname());
		add(TokenName.date, randomizer.getRandomContinueDate(MMddyy));

		add(TokenName.medication_name, randomizer.getRandonMedicationName()+ getValueOrRandom(" TEVAU", ""));

		add(TokenName.dosage, randomizer.getRandomDosage());
		
		String name = randomizer.getRandonMedicationNameWithOutStrength();
		add(TokenName.generic, "SUBSTITUTED FOR " + name.toUpperCase());

		add(TokenName.qty, randomizer.getRandomQuantity());

		add(TokenName.expedition_date, "EXP-" + randomizer.getRandoDate(MMddyy));
		add(TokenName.refill, new DecimalFormat("00").format(randomizer.random.nextInt(20)+1) + " REFILLS");

		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
	}

}
