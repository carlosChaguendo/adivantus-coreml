package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AsapToken extends TokenBuilder {

	public AsapToken(Random random) {
		super(random);
		// TODO Auto-generated constructor stub
	}
	

	List<String> asapTrash = Arrays.asList("THIS MEDICINE IS A BROWN, ROUND SHAPED,",
			"TABLET IMPRINTED WITH S4 ON ONE SIDE.", "A BROWN, ROUND SHAPED,", "ORANGE", "ROUND-SHAPED");
	List<String> asapQty = Arrays.asList("TAB", "CAP", "GM", "ML", "SUP");
	
	List<String> originalPrefix = Arrays.asList("ORG-","ORIG-");
	
	List<String> ins = Arrays.asList("BY:RN","ACTAV", "ACT", "NDC");
 
	/*
	 	ASAP PHARMACY
		340EWILSON AWE.
		ENDALE, CA 91206
		WWW.ASAPPHARMACY.COM
		818-543-1800
		7860224
		BAILEY, JAYNE
		SENEXON-S TABLET
		GENERIC FOR: SENNA S TABLET
		E TAKE 1 TABLET BY MOUTH
		FILL DATE: 09/13/18
		31 TAB
		E EVERY DAY FOR
		CONSTIPATION. HOLD FOR
		E LOOSE STOOLS
		THIS MEDICINE IS AN ORANGE, ROUND- SHAPED
		B0536-4086-01 EXP: 01/2022 TABLET IMPRINTED WITH TCL 081 ON ONE SIDE
		DR.N. JAUREGUI1
		ORG-09/13/18 REF-12
		BY: CH RUGBY
	 */
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {

		tokens = new ArrayList<>();
		labels = new ArrayList<>();

		add(TokenName.pharmacy_name, "ASAP " + getValueOrRandom(pharmacyTextVariations));
		add(TokenName.phone, "818-543-1800");
		add(TokenName.url, randomizer.getRandomWeb());
		add(TokenName.address, "1340 E. WILSON AVE. GLENDALE, CA 91206");
		add(TokenName.resident_name, randomizer.getRandomResidentname());
		add(TokenName.medication_name, randomizer.getRandonMedicationNameWithOutStrength());
		add(TokenName.generic, randomizer.getRandonNameGeneric());
		add(TokenName.qty, randomizer.getRandomQuantity());
		add(TokenName.fill_date, "FILL: " + randomizer.getRandoDate(MMddyy));
		add(TokenName.dosage, randomizer.getRandomDosage());
		add(TokenName._trash_, getValueOrRandom(asapTrash));
		add(TokenName.ins, getValueOrRandom(ins));
		add(TokenName.encino_number, randomizer.getRandomEncinoNumber());
		add(TokenName.caution,
				"CAUTION: FEDERAL LAW PHOHIBITS TRANSFER THIS DRUG TO ANY PERSON OTHER THAN THE PATIENTS FOR WHOM PRESCRIBED");
		add(TokenName.original_date, getValueOrRandom(originalPrefix) + randomizer.getRandoDate(MMddyy));
		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
	}

}
