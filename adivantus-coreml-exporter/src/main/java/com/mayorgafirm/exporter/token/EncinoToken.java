package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EncinoToken extends TokenBuilder {

	public EncinoToken(Random random) {
		super(random);
	}
	
	List<String> encinoCaution = Arrays.asList("BBP+NARC SHT", "SOLCO HEALTHCAR", "THIS MEDICINE IS A LIGHT YELLOW",
			"ROUND SHAPED", "TABLET IMPRINTED WITH", "HH210 ON ONE SIDE.", "TABLET IMPRINTED WITH",
			"SOLCO HEALTHCAR THIS MEDICINE IS A LIGHT YELLOW", "BBP+NARC SHT");
	
	List<String> encinoINS = Arrays.asList("IND", "LF", "CG", "JF", "NR", "IND", "NU");
	
	List<String> encinoPahramayNames = Arrays.asList("ENCINO CARE PHA RMAC", "COENCINO CARET PHA RMAC",
			"ENCINO CARE PHARMACY", "ENCINO PNARMACY");
	
	List<org.joda.time.format.DateTimeFormatter> encinoOriginalDateFormat = Arrays.asList(MM_dd_yy, MMddyy);
	
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		tokens = new ArrayList<>();
		labels = new ArrayList<>();

		String rx = randomizer.getRandomRXNumber();
		String resident = randomizer.getRandomResidentname();
		String med = randomizer.getRandonMedicationName();

		add(TokenName.phone, "(818) 789-9200");
		add(TokenName.address, "16001 VENTURA BLVD. #135 ENCINO, CA 91436");
		add(TokenName.pharmacy_name, getValueOrRandom(encinoPahramayNames));
		add(TokenName.original_date, "ORIG: " + randomizer.getRandoDate(getValueOrRandom(encinoOriginalDateFormat)));
		// add("original_date", "ORIG: "+ getRandoDate(dtf2));
		add(TokenName.fill_date, "FILL: " + randomizer.getRandoDate(getValueOrRandom(encinoOriginalDateFormat)));
		add(TokenName.rx_number, rx);
		add(TokenName.resident_name, resident);
		add(TokenName.medication_name, med);

		add(TokenName.ins, getValueOrRandom(encinoINS));
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		add(TokenName.dosage, randomizer.getRandomDosage());

		add(TokenName.resident_name, resident);
		add(TokenName.date, randomizer.getRandomContinueDate(MM_dd_yy));
		add(TokenName.rx_number, rx);
		add(TokenName.medication_name, med);

		add(TokenName.encino_number, randomizer.getRandomEncinoNumber());

		add(TokenName.qty, randomizer.getRandomQuantity());
		add(TokenName.refill, randomizer.getRandomRefill());
		add(TokenName.expedition_date, "EXP DT-" + randomizer.getRandoDate(MMddyyyy));
		add(TokenName.caution, getValueOrRandom(encinoCaution));

		// falta agregar dosveces el nombre y el dosage para encino
		// TAKE ONE TABLET BY MOUTH EVERY DAY CARTER , ALICE DONEPEZIL HCL 10 MG TABLE

		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
	}
	
}
