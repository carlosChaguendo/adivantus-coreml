package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptunRxToken extends TokenBuilder {

	public OptunRxToken(Random random) {
		super(random);
		// TODO Auto-generated constructor stub
	}
	
	
	/*
	 * 
	  	DR.NEYSSAN.TEBYANI
		DISPENSED DATE
		C6/19/2018
		OPTUNRX
		NO REFILLS, SUBMIT NEW PRE SCRIPTIO.
		RX: 200187042
		DISCARD AFTER:
		1.877.389.6358
		SEE PACKAAGE
		CANNOT FILL AFTER:
		03/27/2019
		2858 LOKERAVE EAST STE 100/ CADSBAD, CA 9 010
		ITEM: (3 OF 3)
		JAYNE BAILEY
		PREMARIN VAG CRE 0.625MG
		MFG: WYETH
		APPLY 1 INCH OF CREAM TO
		FINGER AND APPLY TO OUTER
		URETHRA AND VAGINAL AREA
		TWICE WEEKLY
		QTY:30
		RPH: Q NGUYENN
		REFILLS: 0
		CALL YOUR DOCTOR TOR MEDICAL ADVICE ABOUT SIDE EFFECTS. YOU MAY REPORT ELFECTS TO FOA AT 1-800-FDA-1088
		ILH
		CAUTION:FEDERAL LAW PROHIBITS THE LRANSTER OF THIS DUG TO ANY, PERSON OTHER THAN THE PATIENT FOR WHOM A WAS PRESCRIBED. DO NOTUSE IUT
		DISPENSED 8Y: NPI #1497704431 /0DEA # BP9744524/CATIC # PHY47482
		
		Antes de agregar este 
		
		
		Class                    Precision(%)   Recall(%)      
		_trash_                  100.00         99.14          
		address                  100.00         99.75          
		caution                  88.20          100.00         
		date                     100.00         100.00         
		dosage                   99.64          99.89          
		encino_number            100.00         100.00         
		expedition_date          100.00         100.00         
		fill_date                100.00         93.59          
		generic                  100.00         100.00         
		hour                     100.00         100.00         
		ins                      100.00         99.92          
		last_fill_date           100.00         100.00         
		medication_name          99.94          99.86          
		ndc                      100.00         100.00         
		original_date            100.00         100.00         
		pagination               100.00         100.00         
		pharmacy_name            100.00         100.00         
		phone                    100.00         100.00         
		physician_name           100.00         100.00         
		physician_name_initialis 99.25          100.00         
		qty                      100.00         100.00         
		refill                   100.00         100.00         
		resident_name            99.92          99.49          
		rons_cd8                 100.00         100.00         
		rons_fact                100.00         100.00         
		rons_manufacturer        100.00         100.00         
		rx_number                100.00         100.00         
		url                      100.00         100.00 
		
	 * (non-Javadoc)
	 * @see com.mayorgafirm.exporter.token.TokenBuilder#token()
	 */
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		tokens = new ArrayList<>();
		labels = new ArrayList<>();
		
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		add(TokenName.expedition_date, "DISPENSED DATE "+randomizer.getRandoDate(MMddyyyy));
		add(TokenName.pharmacy_name, "OPTUNRX");
		add(TokenName.rx_number, randomizer.getRandomRXNumberOptuns());
		add(TokenName.discart_date, "DISCARD AFTER:");
		add(TokenName._trash_, "SEE PACKAAGE");
		add(TokenName.next_fill_date ,"CANNOT FILL AFTER: "+randomizer.getRandoDate(MMddyyyy));
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.pagination, "ITEM: ("+randomizer.getRandomPage()+")");
		add(TokenName.resident_name, randomizer.getRandomResidentname());
		add(TokenName.medication_name, randomizer.getRandonMedicationName());
		add(TokenName.rons_manufacturer, getManufaturer());
		add(TokenName.dosage, randomizer.getRandomDosage());
		add(TokenName.qty, randomizer.getRandomQty(Arrays.asList("QTY:")));
		add(TokenName.refill, randomizer.getRandomRefill());
		add(TokenName.emergency, "CALL YOUR DOCTOR TOR MEDICAL ADVICE ABOUT SIDE EFFECTS. YOU MAY REPORT ELFECTS TO FOA AT 1-800-FDA-1088");
		add(TokenName.caution, getValueOrRandom(caution));
		
		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);
		
	}
	
	

	
	public String getManufaturer() {
		String company = randomizer.faker.company().name();
		return getValueOrRandom("MFG: ", "MFG: ", "MIG: ")+company;
	}

}
