package com.mayorgafirm.exporter.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KeiserToken extends TokenBuilder {

	public KeiserToken(Random random) {
		super(random);
	}
	
	List<String> keiserTrash = Arrays.asList(
			"PRINI NOT AVAILABL",
			"WHITE OVAL TABLET, FILM-COATED, SCORED, IMPRINT: 14 , A",
			"BEIGE, FILM-COATED, ROUND, TABLET, MX31,",
			"YELLOW, SCORED, ROUND, TABLET, W 40, ",
			"WHITE ROUND TABLET , IMPRINT: ",
			"PEACH AND LAVENDER, OBLONG, RDY,256",
			"WHITE ROUND TABLET , FILM-COATED, IMPRINT: WPI 858",
			"BRICK RED, FILM-COATED, OVAL, TABLET, S 4,",
			"WHITE AEROSOL",
			"WHITE, FILM-COATED, OVAL, IG, 257 ",
			"BLUE ROUND TABLET , FILM-COATED, IMPRINT: ABG, 15",
			"GREEN, OVAL , TABLET, E/46",
			"WHITE, FILM-COATED, ROUND, J, 10");
	
	List<String> keiserUsage = Arrays.asList(
			"USE AS DIRECTED TO TEST BLOOD SUGAR. FOR USE WITH DELICA LANCING DEVICE", 
			"USE AS DIRECT TO TEST BLOOD SUGAR",
			"USE AS DIRECT TO TEST BLOOD GLUCOSE METER",
			"USE 2 TIMES A DAYS AS DIRECT TO MEASSURE BLOOD");
	
	public java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token() {
		
		
		/*
		AUTION:FEDERAL LAW PROHIBITS TRANSTER OF 866206-2983/WWW.KP.ORG 09-30-2018
		CRP MAIL ORDER PHARMACY
		M PERMANENTEO
		86
		DOWNEY, CA 90242
		9521 DALEN STREET
		RX: 182005188756
		ANDREW PARK MD
		MF: NEPHR
		CORN, MAURY
		IPRATROPIUM BROMIDE 0.02 % SOLUTION FOR INHALATION
		GENERIC FOR: ATROVENT
		USE I VIAL VIA NEBULIZER EVERY 6 HOURS
		HD
		00487980125
		COLORLESS, CLEAR, SOLUTION,
		QTY:250
		CLAIM RX:6091488458
		DISCARD: 09/30/201
		REFILLS 6
		DOSE VIALS

		

		 */
		
		tokens = new ArrayList<>();
		labels = new ArrayList<>();
		add(TokenName.caution, getValueOrRandom(caution));
		add(TokenName.address, randomizer.getRandonPharmacyAddres());
		add(TokenName.phone, "(866) 353-5054");
		add(TokenName.pharmacy_name, "KAISER PERMANENTE");
		
		add(TokenName.rx_number, randomizer.getRandomRXNumberOptuns());
		add(TokenName.physician_name, randomizer.getRandomPhicicianName());
		
		add(TokenName.date, randomizer.getRandoDate(MMddyy));
		add(TokenName.resident_name, randomizer.getRandomResidentname());
		add(TokenName.rons_manufacturer, randomizer.getRandomResidentname());
		add(TokenName.medication_name, randomizer.getRandonMedicationName());
		add(TokenName.generic, randomizer.getRandonNameGeneric());
		
		add(TokenName.dosage, randomizer.getRandomDosage());
		add(TokenName.qty, randomizer.getRandomQty(Arrays.asList("QTY:")));

		add(TokenName._trash_, getValueOrRandom(keiserTrash));
		add(TokenName.rx_number, "CLAIM "+randomizer.getRandomRXNumberOptuns());
		add(TokenName.discart_date, "DISCARD: "+randomizer.getRandoDate(MMddyyyy));
		add(TokenName.refill, randomizer.getRandomRefill());
	
		add(TokenName.emergency, "PLEASE CALL 48 HOURS IN ADVANCE FOR REFILLS");
	
		
		return new java.util.AbstractMap.SimpleEntry<>(tokens, labels);

	}
	
	public String getManufaturer() {
		return getValueOrRandom("MFR ", "MFG ", "MIG ", "MF:")+getValueOrRandom("LIFESCA", "FOUGERA",
				"RISING P",
				"GLENMARK PHARMA",
				"ADAPT PH",
				"HI-TECH",
				"AMERISOU",
				"TEVA USA",
				"RHODES P",
				"ACTAVISI",
				"AUROBIND",
				"NATLVI",
				"GLAXOSMA",
				"WEST-WARD/HIKMA",
				"ACCORD HEALTHCA",
				"JUBILANT CADIST",
				"MYLAN",
				"NEPHR"
				);
	}

}
