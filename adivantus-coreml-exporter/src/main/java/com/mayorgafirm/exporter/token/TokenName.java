package com.mayorgafirm.exporter.token;

public enum TokenName {
	
	 pharmacy_name,
	 phone,
	 address,
	 caution,
	 emergency,//CALL YOUR DOCTOR TOR MEDICAL ADVICE ABOUT SIDE EFFECTS. YOU MAY REPORT ELFECTS TO FOA AT 1-800-FDA-1088
	 rx_number,
	 physician_name,
	 physician_name_initialis,
	 ins,
	 resident_name,
	 medication_name,
	 dosage,
	 date,
	 qty,
	 generic,
	 refill,
	 _trash_,
	 expedition_date,
	 ndc, // national drug code
	 
	 pagination, // 1 of 1
	 fill_date,
	 last_fill_date,
	 next_fill_date,
	 encino_number,
	 url,
	 original_date, // ASP origin
	 hour,
	 rons_cd8,
	 rons_fact,
	 rons_manufacturer,
	 discart_date,
	 
	 keiserUsage, //USE AS DIRECTED TO TEST BLOOD SUGAR. FOR USE WITH DELICA LANCING DEVICE

}
