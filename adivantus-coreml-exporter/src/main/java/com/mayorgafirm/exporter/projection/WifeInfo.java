package com.mayorgafirm.exporter.projection;

public interface WifeInfo extends BasicEntityProjection {
	String getFirstName();
//	String getLastName();
	int getAge();
}
