package com.mayorgafirm.exporter.projection;


@FunctionalInterface
public interface ConsumerKV {

	
    void accept(String... t);
	
}
