package com.mayorgafirm.exporter;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

import net.ricecode.similarity.LevenshteinDistanceStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;

public class MainTest {
	
	
	static int globalindex = 10;
	static Random random = new Random();

	public static void main(String[] args) {
		
		
        String text1 = "ROS\n" +
        "PHARM.\n" +
        "1220 HEMLOCK WA\n" +
        "CAUION FEDERAL LAW PROHBITS TRANSFER C\n" +
        "RX 961083 R\n" +
        "CHRISTIANO, JOS\n" +
        "MULTILEX T/\n" +
        "TAKE 1 TABLET E\n" +
        "SUPPLEMENT)\n" +
        "REFILLS:0 LF: 11/01/2\n" +
        "OVAL,ORANGE,ORANGE\n";
        
//        RX# 966154
//        GOUGH, PHILIR
//        SENNA LAXAT
//        R
//        GENENIC FOR SENOKOT
//        TAKE 1 TABLET B
//        REFILLS:2 LF: 1117/201
//        ROUND AZ 217,BROV/M.BRO
        
        String text2 = "ROSE\n" + 
        		"PHARMACY\n" + 
        		"1220 HEMLOCK WAY, SUITE 110\n" + 
        		"RAL LAW PROHIBITS TRANSFER OF THS DRUG 1O ANY\n" + 
        		"1083 R C DR.T\n" + 
        		"TIANO, JOSEPH\n" + 
        		"RILEX T/M TABC\n" + 
        		"TABLETBY MOU\n" + 
        		"LEMENT)\n" + 
        		":0 LF: 11/01/20 CTY# 3\n" + 
        		"NGE;ORANGE";
        
      
        Supplier<Stream<String>> leftLines = () -> Stream.of( text1.split("\n"));
        Supplier<Stream<String>> rightLines = () -> Stream.of( text2.split("\n"));
        
        
   	 SimilarityStrategy strategy = new LevenshteinDistanceStrategy();

	 StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
        
        
        leftLines.get().forEach( left -> {
        	
        		System.out.println(left);
        		
        		rightLines.get().sorted( (a,b) -> {
        			
        			double x = service.score(a, left);
        			double y = service.score(b, left);
        			
        			return Double.compare(y, x);
        		}).map( e -> "\t"+e)
        		.forEach(System.out::println);
        		
        	
        		
        	
        	
        });

		
	
		 
		 
		
		
		
//		com.mayorgafirm.exporter.token.Random r = new com.mayorgafirm.exporter.token.Random(0, null, null, null, null, null, null, null);
//		
//		
//		for (int i = 0; i < 20; i++) {
//			
//			System.out.println(r.getRandoDate(DateTimeFormat.forPattern("HH:mm")));
//			
//		}
//		
//
//		System.out.println(new Faker().company().name()); 
//		
		
	}
	
	private static <T> T getValueOrRandom(List<T> list) {
		
		if(globalindex > list.size() - 1) {
			int i = random.nextInt(list.size() );
			
			T obj = list.get(i);
			
			if(Objects.isNull(obj)) {
				return list.get(0);//throw new IllegalStateException("lalista1 "+i+" es mayora "+list.size());
			}
			
			return obj;
		}
		
		T obj = list.get(globalindex);
		
		if(Objects.isNull(obj)) {
			return list.get(0);//throw new IllegalStateException("lalista "+globalindex+" es mayora "+list.size()+ "  "+list);
		}
		return obj;
		
	}

}
