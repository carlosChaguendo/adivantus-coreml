package com.mayorgafirm.exporter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.mayorgafirm.exporter.model.Medication;
import com.mayorgafirm.exporter.model.Physician;
import com.mayorgafirm.exporter.model.Resident;
import com.mayorgafirm.exporter.repository.MedicationRepository;
import com.mayorgafirm.exporter.repository.PhysicianRepository;
import com.mayorgafirm.exporter.repository.ResidentRepository;
import com.mayorgafirm.exporter.token.AsapToken;
import com.mayorgafirm.exporter.token.EncinoToken;
import com.mayorgafirm.exporter.token.GilberUpToken;
import com.mayorgafirm.exporter.token.GilbertToken;
import com.mayorgafirm.exporter.token.KeiserToken;
import com.mayorgafirm.exporter.token.OptunRxToken;
import com.mayorgafirm.exporter.token.RonsPharmacy;
import com.mayorgafirm.exporter.token.TokenBuilder;
import com.mayorgafirm.exporter.token.TokenName;

@SpringBootApplication
public class SpringJpaProjectionApplication implements CommandLineRunner {

	//
	@Autowired
	com.mayorgafirm.exporter.repository.WifeRepository wifeRepository;

	@Autowired
	MedicationRepository medicationRepository;

	@Autowired
	ResidentRepository residentRepository;

	@Autowired
	PhysicianRepository physicianRepository;

	ObjectMapper mapper = new ObjectMapper();

	List<Medication> medication;
	List<Resident> residents;
	List<Physician> phiciciasn;
	List<String> pharamaciBames;
	List<String> address;
	List<String> webUrl;
	List<String> caMedications = new ArrayList<>();
	List<String> dosages;
	List<String> ABCD = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
			"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
	
	Map<String, Object> datainfo = new HashMap<>();

	int globalindex = 0;
	Random random = new Random();

	org.joda.time.format.DateTimeFormatter dtf1 = DateTimeFormat.forPattern("MM/dd/yyyy");
	org.joda.time.format.DateTimeFormatter dtf2 = DateTimeFormat.forPattern("MM/dd/yy");
	org.joda.time.format.DateTimeFormatter dtf3 = DateTimeFormat.forPattern("MM-dd-yy");

	org.joda.time.format.DateTimeFormatter hour = DateTimeFormat.forPattern("HH:mm");

	/// Valores para cada elemento del tagger
	List<String> tokens = new ArrayList<>();
	List<String> labels = new ArrayList<>();

	public static void main(String[] args) {

		Faker faker = new Faker(Locale.ENGLISH);

		Name name = faker.name();

		System.out.println(name.lastName() + "," + name.firstName());
		SpringApplication.run(SpringJpaProjectionApplication.class, args);
	}

	//
	// @Override
	public void run(String... arg0) throws Exception {
		// deleteData();
		// saveData();
		loadDataFormDataBase();
		createTattger();
	}


	private Map<String, Object> MapTokenToElement(
			java.util.AbstractMap.SimpleEntry<List<TokenName>, List<String>> token) {
		Map<String, Object> element = new HashMap<>(2);
		element.put("token", token.getValue());
		element.put("label", token.getKey());
		return element;
	}



	@Transactional
	private void loadDataFormDataBase() throws JsonParseException, JsonMappingException, IOException {
		medication = medicationRepository.findByResidents();
		residents = residentRepository.findByResidents();
		phiciciasn = physicianRepository.findByOwner();
		pharamaciBames = medicationRepository.findPharamaciNames();
//		address = physicianRepository.findPharmaciesAddress();
		
		dosages = medicationRepository.findDosages().stream().map( a -> { return a.replaceAll("\\r|\\n", "");}).collect(Collectors.toList());
		
		
		TypeReference<List<String>> r = new TypeReference<List<String>>() {
		};
		
		
		
		webUrl  = mapper.readValue(
				new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/ca-web-url.json"), r);
		
		address  = mapper.readValue(
				new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/ca-address.json"), r);
		
		caMedications = mapper.readValue(
				new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/ca-medications.json"), r);
		
		
		List<String> otherDosages = mapper.readValue(
				new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/ca-dosages.json"), r);
		dosages.addAll(otherDosages);
		
		datainfo.put("db_dosages", dosages.size());
		datainfo.put("ca_dosages", otherDosages.size());
		datainfo.put("db_residents", residents.size());
		datainfo.put("db_medications", medication.size());
		datainfo.put("ca_medications", caMedications.size());
		datainfo.put("db_pharmacies", pharamaciBames.size());
		datainfo.put("db_phicicias", phiciciasn.size());

	}



	private void createTattger() {
		List<Map<String, Object>> mlWordTaggerV2 = new ArrayList<>();



		com.mayorgafirm.exporter.token.Random random = new com.mayorgafirm.exporter.token.Random(globalindex,
				medication, residents, phiciciasn, address, caMedications, dosages, webUrl);
		
		TokenBuilder roseToken = new TokenBuilder(random);
		TokenBuilder asapToken = new AsapToken(random);
		TokenBuilder encinoToken = new EncinoToken(random);
		TokenBuilder gilberUp = new GilberUpToken(random);
		TokenBuilder gilberDown = new GilbertToken(random);
		TokenBuilder advanceToken = new com.mayorgafirm.exporter.token.AdvanceCareToken(random);
		TokenBuilder ronsToken = new RonsPharmacy(random);
		TokenBuilder optunrx = new OptunRxToken(random);
		TokenBuilder keiser = new KeiserToken(random);
		
		List<Class<?>> tokenBuilders = new ArrayList<>();
		tokenBuilders.add(TokenBuilder.class);
		tokenBuilders.add(AsapToken.class);
		tokenBuilders.add(EncinoToken.class);
		tokenBuilders.add(GilberUpToken.class);
		tokenBuilders.add(GilbertToken.class);
		tokenBuilders.add(com.mayorgafirm.exporter.token.AdvanceCareToken.class);
		tokenBuilders.add(RonsPharmacy.class);
		tokenBuilders.add(OptunRxToken.class);
		tokenBuilders.add(KeiserToken.class);
		
		String tokens = tokenBuilders.stream().map(c -> c.getSimpleName()).collect(Collectors.joining(":"));
		
		datainfo.put("tokens", tokens);
		

		globalindex = 0;
		for (int i = 0; i < 2000; i++) {
			
			
			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(roseToken.token()));

			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(encinoToken.token()));

			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(asapToken.token()));


			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(gilberUp.token()));


			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(gilberDown.token()));
			

			random.willCreate();
			/// falta mejorar los nombres de las pharmacias
			mlWordTaggerV2.add(MapTokenToElement(advanceToken.token()));
			
			
			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(ronsToken.token()));
			
			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(optunrx.token()));
			
		
			random.willCreate();
			mlWordTaggerV2.add(MapTokenToElement(keiser.token()));

			

		}
		


		globalindex = 0;


		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.writeValue(new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/taggerV3.0.json"),
					mlWordTaggerV2);
			
			mapper.writeValue(new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/dataInfoV3.0.json"),
					datainfo);
			
//			mapper.writeValue(
//					new File("../AdivantusTrainer/TrainerPlayground.playground/Resources/MedicationStrength.json"),
//					mlMedicationStrenghtTaggerV2);

			System.out.println("mlWordTagger");
			System.out.println(datainfo);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
