package com.mayorgafirm.exporter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mayorgafirm.exporter.model.Resident;

public interface ResidentRepository extends JpaRepository<Resident, Long>{

	@Query(value = " SELECT resident.* FROM residentfacility rf inner join resident on resident.id = rf.resident_id where rf.facility in (select f.id from facility f WHERE owner in (189,360,675,747,886,1179,1335,1372,2074) ) and rf.status = 'ACTIVE'",nativeQuery= true)
	List<Resident> findByResidents();

}
