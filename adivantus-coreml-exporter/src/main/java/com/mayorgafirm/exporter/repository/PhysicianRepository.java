package com.mayorgafirm.exporter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mayorgafirm.exporter.model.Physician;

public interface PhysicianRepository  extends JpaRepository<Physician, Long>{

	
	
	@Query(value="SELECT physician.* FROM ownerphysician inner join physician on  physician.id = ownerphysician.physician where owner in (189,360,675,747,886,1179,1335,1372,2074) and length(physician.first_name) > 3 and physician.first_name not like \"%*%\" and physician.first_name not like \"%(%\" and physician.last_name not like \"%(%\" and LENGTH(physician.last_name) > 3",nativeQuery = true)
	List<Physician> findByOwner();
	
	
	@Query(value="SELECT concat(pharmacy.address,\":\", city.description,\", \",state.code,\" \",pharmacy.zip_code) FROM pharmacy inner join city on city.id = pharmacy.city inner join state on state.id = pharmacy.state where pharmacy.state = 1 and address is not null and city is not null", nativeQuery= true)
	List<String> findPharmaciesAddress();
	
}
