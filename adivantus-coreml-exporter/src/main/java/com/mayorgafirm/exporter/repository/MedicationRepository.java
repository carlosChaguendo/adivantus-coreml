package com.mayorgafirm.exporter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mayorgafirm.exporter.model.Medication;
import com.mayorgafirm.exporter.model.MedicationSchedule;

public interface MedicationRepository extends JpaRepository<Medication, Long>{
	
//	@Query(name="Wife.queeee")
//	List<RemodellingWifeInfo> findByFirstName(String firstName);
	
	
	@Query(value = "SELECT * FROM medicament WHERE medicament.name not like '%(%' AND LENGTH(medicament.name) > 3 AND resident IN( SELECT `resident_id` FROM `residentfacility` WHERE `facility` IN( SELECT f.id FROM facility f WHERE OWNER IN( 189, 360, 675, 747, 886, 1179, 1335, 1372, 2074 ) ) ORDER BY `residentfacility`.`resident_id` )", nativeQuery = true)
	List<Medication> findByResidents();
	
	@Query("select ms  from MedicationSchedule ms  where ms.medicament_id = ?1")
	List<MedicationSchedule> findByMedicament(long medi);

	@Query(value="SELECT pharmacy.name FROM ownerpharmacy INNER JOIN pharmacy ON pharmacy.id = ownerpharmacy.pharmacy WHERE LENGTH(pharmacy.name) > 3 AND OWNER IN( 189, 360, 675, 747, 886, 1179, 1335, 1372, 2074 )", nativeQuery = true)
	List<String> findPharamaciNames();

	@Query(value = "SELECT medicament.dosage FROM medicament WHERE medicament.name not like '%(%' AND LENGTH(medicament.dosage) > 12 AND resident IN( SELECT `resident_id` FROM `residentfacility` WHERE `facility` IN( SELECT f.id FROM facility f WHERE OWNER IN( 189, 360, 675, 747, 886, 1179, 1335, 1372, 2074 ) ) ORDER BY `residentfacility`.`resident_id` ) group by medicament.dosage ", nativeQuery = true)
	List<String> findDosages();

}
