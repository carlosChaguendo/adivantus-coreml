package com.mayorgafirm.exporter.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;


@Entity
@Table(name="husband")
@NamedEntityGraph(name = "Graph.Husband.{photo}", attributeNodes = @NamedAttributeNode(value = "photo"))
public class Husband {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String firstName;
	private String lastName;
	private int age;
	
	@JoinColumn(name = "media")
	@ManyToOne( fetch = FetchType.LAZY)
	private Media photo;
	
//	@OneToOne(mappedBy = "husband")
//	private Wife wife;
	
//	public Husband(){}
	
//	public Husband(String firstName, String lastName, int age){
//		this.firstName = firstName;
//		this.lastName = lastName;
//		this.age = age;
//	}
//	
//	public Husband(int id, String firstName, String lastName, int age, Wife wife){
//		this.id = id;
//		this.firstName = firstName;
//		this.lastName = lastName;
//		this.age = age;
////		this.wife = wife;
//	}
	
	

	public Media getPhoto() {
		return photo;
	}

	public void setPhoto(Media photo) {
		this.photo = photo;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
	public int getAge(){
		return this.age;
	}
	
//	public void setWife(Wife wife){
//		this.wife = wife;
//	}
//	
//	public Wife getWife(){
//		return this.wife;
//	}
	
//	public String toString(){
//		String info = String.format("Husband:{'firstname':%s, 'lastname': %s, age: %d}", 
//				this.firstName, this.lastName, this.age);
//		return info;
//	}
}
