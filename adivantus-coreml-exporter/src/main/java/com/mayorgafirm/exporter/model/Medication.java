package com.mayorgafirm.exporter.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name="MEDICAMENT")
public class Medication implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
    private String name;
    private String nameGeneric;
    private String dosage;
    private org.joda.time.DateTime startDate;
    private String rxNumber;
    private String strength;
    private boolean prn;
    private List<MedicationSchedule> medicationSchedules;

    @Column(nullable = false, updatable = false)
    public boolean isPrn() {
        return prn;
    }

    public void setPrn(boolean prn) {
        this.prn = prn;
    }
 
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	@JsonProperty("medication_name_generic")
    @Column(name = "name_generic")
    public String getNameGeneric() {
        return nameGeneric;
    }

    public void setNameGeneric(String nameGeneric) {
        this.nameGeneric = nameGeneric;
    }
    

	@JsonProperty("medication_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    

	@JsonProperty("medication_name_dosage")
    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
    

//    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }
    
    @Column(name = "rx_number")
    public String getRxNumber() {
        return rxNumber;
    }

    public void setRxNumber(String rxNumber) {
        this.rxNumber = rxNumber;
    }
    
    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicament_id")
    public List<MedicationSchedule> getMedicationSchedules() {
        return medicationSchedules;
    }

    public void setMedicationSchedules(List<MedicationSchedule> medicationSchedules) {
        this.medicationSchedules = medicationSchedules;
    }

}
