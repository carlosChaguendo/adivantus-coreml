package com.mayorgafirm.exporter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "MEDICATION_SCHEDULE")
public class MedicationSchedule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long resident_id;
	private Long medicament_id;
	private String cron;
	private boolean active;
	
	public MedicationSchedule(){
		active=true;
	}
	
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	@Column(nullable = false)
	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

//	prevent infinite recurcion when deserialization
	@JoinColumn(name="medicament_id", nullable = false)
	public Long getMedicament_id() {
		return medicament_id;
	}

	public void setMedicament_id(Long medicament_id) {
		this.medicament_id = medicament_id;
	}

	@JoinColumn(name="resident_id", nullable = false)
	public Long getResident_id() {
		return resident_id;
	}

	public void setResident_id(Long resident_id) {
		this.resident_id = resident_id;
	}

}
