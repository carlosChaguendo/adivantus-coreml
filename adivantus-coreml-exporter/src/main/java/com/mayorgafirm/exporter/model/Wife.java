package com.mayorgafirm.exporter.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import org.hibernate.annotations.QueryHints;

@Entity
@Table(name = "wife")
// @NamedEntityGraph(name = "Graph.Wife.[husband]", attributeNodes =
// @NamedAttributeNode(value = "items", subgraph = "items"), subgraphs =
// @NamedSubgraph(name = "items", attributeNodes =
// @NamedAttributeNode("product")))

// Graph.Husband.[photo]


@NamedQuery(name= "Query.Wife.asdasda", query = "select w from Wife w where w.firstName = ?1 ", hints = @QueryHint(name = QueryHints.COMMENT, value = "a custom SQL comment"))

//@NamedEntityGraphs(value = {
//		@NamedEntityGraph(name = "Graph.Wife.{husban}", attributeNodes = @NamedAttributeNode(value = "husband")),
//		@NamedEntityGraph(name = "Graph.Wife.{husban{photo}}", attributeNodes = @NamedAttributeNode(value = "husband", subgraph = "Graph.Husband.{photo}"), subgraphs = @NamedSubgraph(name = "Graph.Husband.{photo}", attributeNodes = @NamedAttributeNode("photo"))) })

public class Wife implements com.mayorgafirm.exporter.projection.BasicEntityProjection {
	
	public interface BasicEntityProjection {
		
		int getId();

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String firstName;
	private String lastName;
	private int age;

//	@JoinColumn
//	@ManyToOne(fetch = FetchType.LAZY)
//	@Fetch(FetchMode.JOIN)
//	private Husband husband;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

//	public void setHusband(Husband husband) {
//		this.husband = husband;
//	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return this.age;
	}

//	public Husband getHusband() {
//		return this.husband;
//	}

	// public String toString() {
	// String info = String.format(
	// "Wife: {'firstname': %s, 'lastname': %s, age: %d, husband: {'firstname': %s,
	// 'lastname': %s, age: %d}}",
	// this.firstName, this.lastName, this.age, this.husband.getFirstName(),
	// this.husband.getLastName(),
	// this.husband.getAge());
	// return info;
	// }
}
