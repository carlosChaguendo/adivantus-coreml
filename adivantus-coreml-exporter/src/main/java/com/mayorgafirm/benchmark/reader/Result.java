package com.mayorgafirm.benchmark.reader;

import java.util.List;

import lombok.Data;

@Data
public class Result {
	
	private String id;
	private List<ResultImage> images;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<ResultImage> getImages() {
		return images;
	}
	public void setImages(List<ResultImage> images) {
		this.images = images;
	}

}
