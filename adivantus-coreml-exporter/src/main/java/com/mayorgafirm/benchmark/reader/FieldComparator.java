package com.mayorgafirm.benchmark.reader;

@FunctionalInterface
public interface FieldComparator {

	String compare(String a, String b, String label, String group);
	
}
