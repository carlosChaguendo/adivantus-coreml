package com.mayorgafirm.benchmark.reader;

import java.util.List;

import lombok.Data;

@Data
public class ResultImage {
	
	private String id;
	private PrescriptionInformation realInformation;
	private List<ImageAnlitics> analitics;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public PrescriptionInformation getRealInformation() {
		return realInformation;
	}
	public void setRealInformation(PrescriptionInformation realInformation) {
		this.realInformation = realInformation;
	}
	public List<ImageAnlitics> getAnalitics() {
		return analitics;
	}
	public void setAnalitics(List<ImageAnlitics> analitics) {
		this.analitics = analitics;
	}

	
	
}
