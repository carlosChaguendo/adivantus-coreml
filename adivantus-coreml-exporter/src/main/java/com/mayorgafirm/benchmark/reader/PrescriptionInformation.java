package com.mayorgafirm.benchmark.reader;

import java.util.Objects;

import lombok.Data;

@Data
public class PrescriptionInformation {
	
	
	private String residentName;
	private String medicationName;
	private String nameGeneric;
	private String dosage;
	private String numberRefill;
	private String expDate;
	private String fillDate;
	private String rxnumber;
	private String physicianName;
	private String pharmacyName;
	private String medicationStrenght;
	private String qty;
	
	
	public String getResidentName2() {
		return ReadVisionJson.clearName( residentName);
	}
	
	public String getPhysicianName2() {
		return ReadVisionJson.clearName(physicianName);
	}
	
	
	public String getDosage2() {
		
		if (Objects.isNull(dosage)){return null;}
		
		return dosage.replace(" ( ", " (").replace(" )", ")");
	}

	public String getResidentName() {
		return residentName;
	}

	public void setResidentName(String residentName) {
		this.residentName = residentName;
	}

	public String getMedicationName() {
		return medicationName;
	}

	public void setMedicationName(String medicationName) {
		this.medicationName = medicationName;
	}

	public String getNameGeneric() {
		return nameGeneric;
	}

	public void setNameGeneric(String nameGeneric) {
		this.nameGeneric = nameGeneric;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getNumberRefill() {
		return numberRefill;
	}

	public void setNumberRefill(String numberRefill) {
		this.numberRefill = numberRefill;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getFillDate() {
		return fillDate;
	}

	public void setFillDate(String fillDate) {
		this.fillDate = fillDate;
	}

	public String getRxnumber() {
		return rxnumber;
	}

	public void setRxnumber(String rxnumber) {
		this.rxnumber = rxnumber;
	}

	public String getPhysicianName() {
		return physicianName;
	}

	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}

	public String getPharmacyName() {
		return pharmacyName;
	}

	public void setPharmacyName(String pharmacyName) {
		this.pharmacyName = pharmacyName;
	}

	public String getMedicationStrenght() {
		return medicationStrenght;
	}

	public void setMedicationStrenght(String medicationStrenght) {
		this.medicationStrenght = medicationStrenght;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	
	
	
	
	

	
	
}
