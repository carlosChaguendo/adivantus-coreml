package com.mayorgafirm.benchmark.reader;

import java.util.List;

import lombok.Data;

@Data
public class ImageAnlitics {
	private String id;
	
	
	private List<Extraction> extrations;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public List<Extraction> getExtrations() {
		return extrations;
	}


	public void setExtrations(List<Extraction> extrations) {
		this.extrations = extrations;
	}
	
}
