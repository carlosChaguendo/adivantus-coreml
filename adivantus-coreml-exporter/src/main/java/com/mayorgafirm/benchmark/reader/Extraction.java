package com.mayorgafirm.benchmark.reader;

import lombok.Data;

@Data
public class Extraction {

	private String bruto;
	private PrescriptionInformation information;
	private String id;
	public String getBruto() {
		return bruto;
	}
	public void setBruto(String bruto) {
		this.bruto = bruto;
	}
	public PrescriptionInformation getInformation() {
		return information;
	}
	public void setInformation(PrescriptionInformation information) {
		this.information = information;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
