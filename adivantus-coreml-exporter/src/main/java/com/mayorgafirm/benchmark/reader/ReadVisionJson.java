package com.mayorgafirm.benchmark.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.ricecode.similarity.JaroStrategy;
import net.ricecode.similarity.JaroWinklerStrategy;
import net.ricecode.similarity.LevenshteinDistanceStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;

public class ReadVisionJson {
	
	static SimilarityStrategy strategy = new JaroWinklerStrategy();

	static StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
	
	
	static CellStyle totalStyle;
	
	static DecimalFormat df = new DecimalFormat("####0.00");
	
	
	public static String unaccent(String src) {

		
		return Normalizer
				.normalize(src, Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}]", "");
	}
	
	/*
	 
	 
	 Ultimo caso para rose
	 	Resident		0,98
		QTY			0,8
		RX			0,99
		Ph Name		0,98
		Medication	0,88
		Dosage		0,95
		Num-Refil	0,92
		Med-Streng	0,78
		Pharmacy		0,93
		Exp-Date		0,96
		Name Generic	1
		Fill-Date	0,85
	 
	 
	 
	 */
	




	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		
	
		

		TypeReference<List<Result>> typeReference = new TypeReference<List<Result>>() {
		};

		ObjectMapper mapper = new ObjectMapper();

		String fileNamed = "rose|gilber|runs|keyser|english.xls";
				                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
		String path = "/Users/carlos.chaguendo/Library/Developer/CoreSimulator/Devices/D578CE43-E15C-43CA-BA3C-0935805624C0/data/Containers/Data/Application/59986DD9-9C77-4155-8043-3F00E345201B/Documents/";

		String jsonPath = path+"ocr-debug/";

		List<Result> results = mapper.readValue(new File(jsonPath + "capija.json"), typeReference);
		List<String> ignorModels = Arrays.asList("rose-pharmacy", "othersv2");

		PrescriptionInformation labels = new PrescriptionInformation();
		labels.setResidentName("Resident");
		labels.setMedicationName("Medication");
		labels.setDosage("Dosage");
		labels.setNameGeneric("Name Generic");
		labels.setExpDate("Exp-Date");
		labels.setFillDate("Fill-Date");
		labels.setNumberRefill("Num-Refil");
		labels.setRxnumber("RX");
		labels.setQty("QTY");
		labels.setPhysicianName("Ph Name");
		labels.setPharmacyName("Pharmacy");
		labels.setMedicationStrenght("Med-Streng");

		HSSFWorkbook wb = new HSSFWorkbook();
		
		 CellStyle headerCellStyle = wb.createCellStyle();
		 headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		 short color = 5;
		 short color2 = 42;
		 headerCellStyle.setFillBackgroundColor(color);
		 headerCellStyle.setFillForegroundColor(color);
		 
		 
		 short cellSeparatorStylColor = 45;
		 CellStyle cellSeparatorStyle = wb.createCellStyle();
		 cellSeparatorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		 cellSeparatorStyle.setFillBackgroundColor(cellSeparatorStylColor);
		 cellSeparatorStyle.setFillForegroundColor(cellSeparatorStylColor);
		 
		 
		 
		 
		 totalStyle = wb.createCellStyle();
		 totalStyle.setFillBackgroundColor(color2);
		 totalStyle.setFillForegroundColor(color2);
		 totalStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		 
		 HSSFCellStyle fillTExt=  wb.createCellStyle();
		 
		
		 fillTExt.setAlignment(HorizontalAlignment.LEFT);
		 fillTExt.setVerticalAlignment(VerticalAlignment.CENTER);
		
		int needsRowPerImage = 16;
		
		
		List<String> headers = Arrays.asList("Image", "Label", "Real");


		for (Result result : results) {

			HSSFSheet sheet = wb.createSheet(result.getId());


			int rowPerImage = 0;
			
			labelsOk = new LinkedHashMap<>();
			int totalImagesAnlitics = 0;
			int numberOfModels = 0;

			for (ResultImage image : result.getImages()) {

				for (ImageAnlitics analitics : image.getAnalitics()) {
					totalImagesAnlitics ++;
					
					numberOfModels = analitics.getExtrations().size();
					
	

					/// Templante para cada imagen
					// 1 header
					// 12 field
					// 1 salto linea
					for (int r = 0; r < needsRowPerImage; r++) {
						HSSFRow row = sheet.createRow(r + rowPerImage);
						
//						if (r == 14) {
//							short h = 4000;
//							row.setHeight(h);
//						}
						
						// iterating c number of columns
						for (int c = 0; c < ( numberOfModels* 3) +  headers.size(); c++) {
							HSSFCell cell = row.createCell(c);
							if (r == 0 ) {
								cell.setCellStyle(headerCellStyle);
								
							}
						}
					}
					
					
						
					for (int i = 0; i < headers.size(); i++) {
						sheet.getRow(rowPerImage).getCell(0 + i).setCellValue(headers.get(i));

					}
					
					
					
					// labels
					setMedicalInformation(sheet, rowPerImage, 1, labels);

					/// fija los valores reales

					setMedicalInformation(sheet, rowPerImage, 2, image.getRealInformation());
					
					
					
	
					for (int i = 0; i < analitics.getExtrations().size(); i++) {
						Extraction extraction =  analitics.getExtrations().get(i);
						int colum =  headers.size()  + i;
						String label = "Real vs "+extraction.getId();
						
						/// Se fija la informacuion extraida por cada modelo
						sheet.getRow(rowPerImage).getCell(colum).setCellValue(extraction.getId());
						setMedicalInformation(sheet, rowPerImage, colum, extraction.getInformation());
		
						/// Se compara los valores contra el real
						colum = headers.size() +  analitics.getExtrations().size() + i;
						sheet.getRow(rowPerImage).getCell(colum).setCellValue(label);
						setCompare(label,sheet, rowPerImage, colum, image.getRealInformation(), extraction.getInformation() ,labels);
						
						colum = headers.size() + (analitics.getExtrations().size()*2) + i;
						sheet.getRow(rowPerImage).getCell(colum).setCellValue("Bruto "+extraction.getId());
						
						
//	
						
						HSSFCell cellA = sheet.getRow(rowPerImage+2).getCell(colum);
						HSSFCell cellB = sheet.getRow(rowPerImage+15).getCell(colum);
						
						CellRangeAddress region2 = CellRangeAddress.valueOf(cellA.getAddress().toString() + ":"+cellB.getAddress().toString());
						sheet.addMergedRegion(region2);
						
					
						sheet.getRow(rowPerImage + 2).getCell(colum).setCellValue(extraction.getBruto());
						sheet.getRow(rowPerImage + 2).getCell(colum).setCellStyle(fillTExt);
						sheet.setColumnWidth(colum, 8000);
						
						
						
						
//						"Configurado", "Generico", "Real vs Configurado", "Real vs Generico"
					}
					
					sheet.getRow(rowPerImage + 1).getCell(0).setCellValue(image.getId());
					
					
		
			
					
					
					CellRangeAddress region1 = CellRangeAddress.valueOf("A" + (rowPerImage+3) + ":A"+ (rowPerImage+14));
					sheet.addMergedRegion(region1);
					
					String img = path+"croped/"+result.getId()+"/"+image.getId();
					insertImage(wb, sheet, rowPerImage + 2, 0, img);
					
					/// para la saiguiente imagen
					rowPerImage += needsRowPerImage;

				} /// image analitics
				
				
				HSSFRow row = sheet.createRow( rowPerImage);
				for (int c = 0; c < ( numberOfModels* 3) +  headers.size(); c++) {
					HSSFCell cell = row.createCell(c);
					cell.setCellStyle(cellSeparatorStyle);
				}
				
				rowPerImage ++;
				
				
				
				

			}
		
			sheet.autoSizeColumn(0);
//			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
			sheet.autoSizeColumn(5);
			sheet.autoSizeColumn(6);
			sheet.autoSizeColumn(7);
			sheet.autoSizeColumn(8);
			
			sheet.setColumnWidth(0, 8000);
			
			
		
			HSSFRow row = sheet.createRow( rowPerImage);
			HSSFCell title = row.createCell(0);
			HSSFCell otherValue;
			HSSFCell otherValueFormala;
			title.setCellValue("Imagenes Analizada "+totalImagesAnlitics);
			rowPerImage++;

			
			for(Map.Entry<String, Map<String, Double>> labes: (labelsOk).entrySet()) {
				
				 row = sheet.createRow( rowPerImage);
				 title = row.createCell(0);
				HSSFCell value = row.createCell(1);
				
				title.setCellValue("Comparacion");
				value.setCellValue(labes.getKey());
				
				rowPerImage ++;
				
				for(Map.Entry<String, Double> entry: labes.getValue().entrySet()) {
				
					row = sheet.createRow( rowPerImage);
					title = row.createCell(0);
					value = row.createCell(1);
					otherValue = row.createCell(2);
					otherValueFormala = row.createCell(3);
					
					
					int n = row.getRowNum()+1;
					
					otherValueFormala.setCellFormula("SI(B"+n+"=C"+n+",\"\",   SI(B"+n+"<C"+n+",\"<\",  SI(B"+n+">C"+n+",\">\") ) )");
					
					Double p =  round(entry.getValue()/totalImagesAnlitics, 2);
				
					title.setCellValue(entry.getKey());
					value.setCellValue(p);
//					otherValue.setCellValue( );
					rowPerImage++;
				}
			}
			

			
			
		}
		
		
		
		
		HSSFSheet sheetColores = wb.createSheet("colores");
		for (int r = 0; r < 100; r++) {
			HSSFRow row = sheetColores.createRow(r);
			HSSFCell cell = row.createCell(0);
			cell.setCellValue(r);
			
			 CellStyle a1 = wb.createCellStyle();
			 a1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//			 HSSFColor myColor = HSSFColor.HSSFColorPredefined.BLUE.getColor();
			 short xs = Short.valueOf(r+"");
//			 short color2 = 3;
			 a1.setFillBackgroundColor(xs);
			 a1.setFillForegroundColor(xs);
			 
			 cell.setCellStyle(a1);
		}
		
		System.out.println(jsonPath);

		FileOutputStream fileOut = new FileOutputStream(jsonPath + fileNamed);

		// write this workbook to an Outputstream.
		wb.write(fileOut);
		fileOut.flush();
		fileOut.close();

	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	


	static void setMedicalInformation(HSSFSheet sheet, int rowPerImage, int column, PrescriptionInformation info) {
		
		int i = 1;
		
		for ( Map.Entry< Function<PrescriptionInformation, String>, FieldComparator> entry : filedOrder.entrySet()) {
			Function<PrescriptionInformation, String> fn = entry.getKey();
			String compartion  = fn.apply(info);
			
	
			
			sheet.getRow(rowPerImage + i).getCell(column).setCellValue(compartion);
			i ++;
		
		}
		

		
	}
	
	
	static int compareConter = 0;
	static int compareTotals = 0;
	static Double compareObjectSumatory = 0.0;
	
	static Map<String, Map<String, Double>> labelsOk = new LinkedHashMap<>();
	static Map<Function<PrescriptionInformation, String> , FieldComparator> filedOrder = new LinkedHashMap<>();
	static {
		filedOrder.put(PrescriptionInformation::getResidentName2, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getQty, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getRxnumber, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getPhysicianName2, ReadVisionJson::compareString);
		
		filedOrder.put(PrescriptionInformation::getMedicationName, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getDosage2, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getNumberRefill, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getMedicationStrenght, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getPharmacyName, ReadVisionJson::compareString);
		
		
		filedOrder.put(PrescriptionInformation::getExpDate, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getNameGeneric, ReadVisionJson::compareString);
		filedOrder.put(PrescriptionInformation::getFillDate, ReadVisionJson::compareString);
	}
	
	
	static List<SimilarityStrategy> similarityStrategies = new ArrayList<>();
	static {
		similarityStrategies.add(new JaroStrategy());
		similarityStrategies.add(new JaroWinklerStrategy());
		similarityStrategies.add(new LevenshteinDistanceStrategy());
	}
	
	static void setCompare(String group,HSSFSheet sheet, int rowPerImage, int column, PrescriptionInformation a, PrescriptionInformation b, PrescriptionInformation c) {
		
		
		compareConter = 0;
		compareTotals = 0;
		compareObjectSumatory = 0.0;
		
		
		int i = 1;
		
		for ( Map.Entry< Function<PrescriptionInformation, String>, FieldComparator> entry : filedOrder.entrySet()) {
			
			Function<PrescriptionInformation, String> fn = entry.getKey();
			FieldComparator compartor = entry.getValue();
			
			String _a = fn.apply(a);
			String _b = fn.apply(b);
			String _c = fn.apply(c);
			String compartion = compartor.compare(_a, _b, _c,group);
			
			sheet.getRow(rowPerImage + i).getCell(column).setCellValue(compartion);
			i ++;
			
		}
		
		sheet.getRow(rowPerImage + i ).getCell(column).setCellValue(compareConter);
		sheet.getRow(rowPerImage + i ).getCell(column).setCellStyle(totalStyle);
		
		sheet.getRow(rowPerImage + i+1 ).getCell(column).setCellValue(compareObjectSumatory/filedOrder.size());
		
		
	}
	
	static String compareString(String a, String b, String label, String group) {
		
		a =  Objects.isNull(a) ? "": a.trim();
		b =  Objects.isNull(b) ? "": b.trim();
		
//		boolean eq = Objects.equals(a, b);
		double percent = 0;
		
		if( a.isEmpty() && b.isEmpty()) { 
			percent = 1;
		} else  {
			percent = Math.abs(service.score(a.trim(), b.trim()));
		}
		
//		if (percent < 0.95) {
//			//percent = 0;
//		}
		
		boolean eq =  percent != 0;
		
		Double incremet = percent; //eq ? 1:0;
		compareConter += eq ? 1:0;
		compareTotals++;
		compareObjectSumatory +=  incremet;
		
		addPageStatics(group, label, (incremet));
		
		return (eq ? "=" : "<>")+percent;
	}
	
	
	static void addPageStatics(String group, String label, Double incremet) {
		if(labelsOk.containsKey(group)) {
			if  (labelsOk.get(group).containsKey(label)) {
				Double n =  labelsOk.get(group).get(label);
				labelsOk.get(group).put(label, n += incremet);
				
			}else {
				labelsOk.get(group).put(label, incremet);
			}
		} else {
			Map<String, Double> n =  new LinkedHashMap<>(); 
			n.put(label, incremet);
			labelsOk.put(group, n);
		}
	}
	
	static String clearName(String name) {
		if (name == null || name.isEmpty()){
			return name;
		}	
		
		
		String name_x =  name.replace(" , ", ",").replace(", ", ",").replace(". ",".");

		if (name_x.charAt(name_x.length()-1)=='.') {
			name_x = name_x.substring(0, name_x.length() - 1);
		}
		
		return name_x;
	}
	
	static void insertImage(HSSFWorkbook wb,HSSFSheet sheet, int row, int col, String img) throws IOException {
		//FileInputStream obtains input bytes from the image file

//		if (true) {
//			return;
//		}
		
		   InputStream inputStream = new FileInputStream(img);
		   //Get the contents of an InputStream as a byte[].
		   byte[] bytes = IOUtils.toByteArray(inputStream);
		   //Adds a picture to the workbook
		   int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		   //close the input stream
		   inputStream.close();

		   //Returns an object that handles instantiating concrete classes
		   CreationHelper helper = wb.getCreationHelper();

		   //Creates the top-level drawing patriarch.
		   Drawing drawing = sheet.createDrawingPatriarch();

		   //Create an anchor that is attached to the worksheet
		   ClientAnchor anchor = helper.createClientAnchor();
		   //set top-left corner for the image
		   anchor.setCol1(col);
		   anchor.setRow1(row);

		   //Creates a picture
		   Picture pict = drawing.createPicture(anchor, pictureIdx);
		   
		   pict.resize(1, 10);
		   //Reset the image to the original size
		
		
	}
	


}
